package web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dto.WEBjuchuDTO;
import dto.juchu;
import dto.kaiinDTO;
import dto.keiyakuDTO;
import dto.kyosanDTO;
import dto.payDTO;
import dto.sendDTO;
import dto.shohinDTO;

public class WebJucyuDAO {

	// **************************************************************************
	// カート内商品情報を取得（セッション）
	// **************************************************************************

	public static ArrayList<shohinDTO> getShohin(String gou, String pagee,
			String shohinno, String iro, String gara, String size) {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Statement stmt = null;
		Connection con = null;

		// 配列の宣言
		ArrayList<shohinDTO> data = new ArrayList<shohinDTO>();

		try {
			// MySQLとの接続を開始

			// JDBCドライバーのロード（p1-17）
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			Statement startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "SELECT * FROM `t_shohin` WHERE gou='" + gou + "'AND "
					+ "page = '" + pagee + "' AND " + "shohinno='" + shohinno
					+ "' AND " + "size='" + size + "' AND " + "gara='" + gara
					+ "' AND " + "iro='" + iro + "'";

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			while (result.next()) {

				shohinDTO record = new shohinDTO();

				record.setGou(result.getString("gou"));
				record.setPage(result.getString("pagee"));
				record.setShohinno(result.getString("shohinno"));
				record.setShohinname(result.getString("shohinname"));
				record.setSize(result.getString("size"));
				record.setGara(result.getString("gara"));
				record.setIro(result.getString("iro"));
				data.add(record);// 配列に追加
			}

			// MySQLとの接続を終了
			con.close();

			// エラー情報を取得

		} catch (ClassNotFoundException e) {
			System.out.println("ドライバのロードに失敗しました。");
		} catch (Exception e) {
			System.out.println("例外発生:" + e);

		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}// tryの終わり
		return null;

	}

	// **************************************************************************
	// 受注商品　インサート
	// **************************************************************************

	public static ArrayList<juchu> addJuchuSyohin(int juchuno, String gou,
			String pagee, String shohinno, String shohinname, String iro,
			String gara, String size, int tanka, String model, String unit) {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Statement stmt = null;
		Connection con = null;

		// 配列の宣言
		ArrayList<juchu> data = new ArrayList<juchu>();

		try {
			// MySQLとの接続を開始

			// JDBCドライバーのロード（p1-17）
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			con.setAutoCommit(false);
			stmt = con.createStatement();

			// //追加するSQL
			String sql = "INSERT INTO `t_juchushohin`(juchuno, `gou`,  `page`, `shohinno`, `iro`, gara, shohinsize, nontaxsum, rate, taxsum, tanka, shohinname, model, unit) VALUES "
					+ "('"
					+ juchuno
					+ "','"
					+ gou
					+ "','"
					+ pagee
					+ "','"
					+ shohinno
					+ "','"
					+ iro
					+ "','"
					+ gara
					+ "','"
					+ size
					+ "','"
					+ tanka
					+ "', 1.08, "
					+ Math.floor(tanka * 1.08)
					+ ",'"
					+ tanka
					+ "','" + shohinname + "','"+model+"','"+unit+"')";

			if (stmt.executeUpdate(sql) == 1) {
				System.out.println("成功しました。");
				con.commit();
			} else {
				System.out.println("失敗しました。");
				con.rollback();
			}

			// エラー情報を取得

		} catch (ClassNotFoundException e) {
			System.out.println("ドライバのロードに失敗しました。");
		} catch (Exception e) {
			System.out.println("例外発生:" + e);

		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}// tryの終わり
		return data;
	}

	// **************************************************************************
	// 受注　インサート
	// **************************************************************************

	public static ArrayList<juchu> addJuchu(String receptime, int kyosanno,
			String kaiinno, int sumgaku, double riritu) {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Statement stmt = null;
		Connection con = null;

		// 配列の宣言
		ArrayList<juchu> data = new ArrayList<juchu>();

		try {
			// MySQLとの接続を開始

			// JDBCドライバーのロード（p1-17）
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			con.setAutoCommit(false);
			stmt = con.createStatement();

			// //追加するSQL
			String sql = "INSERT INTO `t_juchu`(`receptime`,  `kyosanno`, `kaiinno`, `sumgaku`, sumtax, riritu, tantouid) VALUES ('"
					+ receptime	+ "','"	+ kyosanno	+ "','"	+ kaiinno	+ "','"	+ sumgaku	+ "',"	+ sumgaku + "*1.08,"+ riritu+ ",9999999999)";

			System.out.println(sql);

			if (stmt.executeUpdate(sql) == 1) {
				System.out.println("成功しました。");
				con.commit();
			} else {
				System.out.println("失敗しました。");
				con.rollback();
			}

			// エラー情報を取得

		} catch (ClassNotFoundException e) {
			System.out.println("ドライバのロードに失敗しました。");
		} catch (Exception e) {
			System.out.println("例外発生:" + e);

		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}// tryの終わり
		return data;
	}

	// **************************************************************************
	// 支払取得
	// **************************************************************************

	public static payDTO dispShiharai(String juchuno) {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Statement stmt = null;
		Connection con = null;
		// 配列の宣言
		payDTO pay = new payDTO();

		try {
			// MySQLとの接続を開始

			// JDBCドライバーのロード（p1-17）
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			Statement startment = con.createStatement();

			// 検索するSQL
			String sql = "SELECT * FROM t_pay WHERE juchuno = " + juchuno;

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 商品クラスのレコードをセット

			while (result.next()) {
				pay.setKaiinno(result.getString("kaiinno"));
				pay.setJuchuno(result.getInt(Integer.getInteger("juchuno")));
				pay.setPayway(result.getString("payway"));
				pay.setPaybank(result.getString("paybank"));
				pay.setPaysum(result.getInt(Integer.getInteger("paysum")));
				pay.setPaykaisu(result.getInt(Integer.getInteger("paykaisu")));
				pay.setPaygaku(result.getInt(Integer.getInteger("paygaku")));
				pay.setPaydate(result.getDate("paydata"));
			}

			// MySQLとの接続を終了
			con.close();

			// エラー情報を取得

		} catch (ClassNotFoundException e) {
			System.out.println("ドライバのロードに失敗しました。");
		} catch (Exception e) {
			System.out.println("例外発生:" + e);

		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}// tryの終わり
		return pay;
	}

	// **************************************************************************
	// 届先　インサート
	// **************************************************************************

	public static int addTodoke(String juchuno, String sendjusho,
			String sendname, String sendkubun, String rusu, String sendtelno,
			String sakierea) {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";
		int result1 = 0;
		Statement stmt = null;
		Connection con = null;
		// 配列の宣言
		sendDTO send = new sendDTO();

		try {
			// MySQLとの接続を開始

			// JDBCドライバーのロード（p1-17）
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			Statement startment = con.createStatement();

			// 検索するSQL
			String sql = "INSERT INTO `t_send`(`juchuno`, `sendjusho`, `sendname`, `sendkubun`, `rusu`, `sendtelno`, `sakiarea`) "
					+ "VALUES ('"
					+ juchuno
					+ "',"
					+ sendjusho
					+ ","
					+ sendname
					+ ","
					+ "'"
					+ sendkubun
					+ "',"
					+ "'"
					+ rusu
					+ "'"
					+ "'"
					+ sendtelno + "'," + "'" + null + "'," + ")";

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// //商品クラスのレコードをセット
			//
			// while (result.next()) {
			//
			// send.setJuchuno(result.getInt(Integer.getInteger("juchuno")));
			// send.setSendjusho(result.getString("sendjusho"));
			// send.setSendname(result.getString("sendname"));
			// send.setSendkubun(result.getString("sendkubun"));
			// send.setRusu(result.getString("rusu"));
			// send.setSendtelno(result.getString("sendtelno"));
			// send.setSakierea(result.getString("sakierea"));
			// }
			//

			// MySQLとの接続を終了
			con.close();

			// エラー情報を取得

		} catch (ClassNotFoundException e) {
			System.out.println("ドライバのロードに失敗しました。");
		} catch (Exception e) {
			System.out.println("例外発生:" + e);

		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}// tryの終わり
		return result1;
	}

	// **************************************************************************
	// 届先取得
	// **************************************************************************

	public static sendDTO dispTodoke(String juchuno) {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";
		int result1 = 0;
		Statement stmt = null;
		Connection con = null;
		// 配列の宣言
		sendDTO send = new sendDTO();

		try {
			// MySQLとの接続を開始

			// JDBCドライバーのロード（p1-17）
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			Statement startment = con.createStatement();

			// 検索するSQL
			String sql = "SELECT * FROM t_send WHERE juchuno = '" + juchuno
					+ "'";

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 商品クラスのレコードをセット

			while (result.next()) {

				send.setJuchuno(result.getInt(Integer.getInteger("juchuno")));
				send.setSendjusho(result.getString("sendjusho"));
				send.setSendname(result.getString("sendname"));
				send.setSendkubun(result.getString("sendkubun"));
				send.setRusu(result.getString("rusu"));
				send.setSendtelno(result.getString("sendtelno"));
				send.setSakierea(result.getString("sakierea"));
			}

			// MySQLとの接続を終了
			con.close();

			// エラー情報を取得

		} catch (ClassNotFoundException e) {
			System.out.println("ドライバのロードに失敗しました。");
		} catch (Exception e) {
			System.out.println("例外発生:" + e);

		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}// tryの終わり
		return send;
	}

	// 会員の協賛企業No取得
	public static kyosanDTO select(String name) {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Statement stmt = null;
		Connection con = null;

		// 配列の宣言

		kyosanDTO data = null;

		try {
			// MySQLとの接続を開始

			// JDBCドライバーのロード（p1-17）
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			Statement startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "SELECT * FROM t_kyosan WHERE kyosanname='" + name
					+ "'";
			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);
			// 変数resultの内容を順次出力

			while (result.next()) {
				data = new kyosanDTO();
				data.setKyosanno(result.getInt("kyosanno"));
			}

		} catch (ClassNotFoundException e) {
			System.out.println("ドライバのロードに失敗しました。");
		} catch (Exception e) {
			System.out.println("例外発生:" + e);

		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}// tryの終わり
		return data;

	}

	public static juchu juchunoGet(String receptime, int kyosanno,
			String kaiinnoo, int sumgaku) {

		// String keiyakuno;
		// String keiyakuname;
		// String keiyakujusho;
		// String motoarea;

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String pw = "";
		Connection con = null;
		Statement startment = null;

		// ArrayList<keiyakuDTO> data = new ArrayList<keiyakuDTO>();
		// //recordを入れる配列

		juchu record = new juchu();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, pw);
			startment = con.createStatement();
			System.out.println("接続成功");

			String sql2 = "SELECT * FROM `t_juchu` WHERE `receptime`=" + "'"
					+ receptime + "'" + "AND `kyosanno`=" + "'" + kyosanno
					+ "'" + "AND `kaiinno`=" + "'" + kaiinnoo + "'"
					+ "AND `sumgaku`='" + sumgaku + "'";

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql2);

			// 変数resultの内容を順次出力
			int x;
			for (x = 0; result.next(); x++) {
				record.setJuchuno(result.getInt("juchuno"));

			}

		} catch (ClassNotFoundException e) {
			System.out.println("JDBCドライバーのロードに失敗しました");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return record;
	}

	public static payDTO shiharai(String kaiinno, int juchuno, String payway,
			String paybank, double paysum, String paykaisu) {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Statement stmt = null;
		Connection con = null;
		// 配列の宣言
		payDTO data = new payDTO();

		int kaisu = Integer.parseInt(paykaisu);

		try {
			// MySQLとの接続を開始

			// JDBCドライバーのロード（p1-17）
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			con.setAutoCommit(false);
			stmt = con.createStatement();

			// 追加するSQL
			String sql = "INSERT INTO `t_pay`(`kaiinno`, `juchuno`, `payway`, `paybank`, `paysum`, `paykaisu`, paygaku) "
					+ "VALUES ('"
					+ kaiinno
					+ "','"
					+ juchuno
					+ "','"
					+ payway
					+ "','"
					+ paybank
					+ "','"
					+ Math.floor(paysum)
					+ "','"
					+ kaisu + "','" + Math.floor(paysum / kaisu) + "')";

			if (stmt.executeUpdate(sql) == 1) {
				System.out.println("成功しました。");
				con.commit();
			} else {
				System.out.println("失敗しました。");
				con.rollback();
			}

			// エラー情報を取得

		} catch (ClassNotFoundException e) {
			System.out.println("ドライバのロードに失敗しました。");
		} catch (Exception e) {
			System.out.println("例外発生:" + e);

		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}// tryの終わり
		return data;
	}

	public static payDTO shiharai2(String kaiinno, int juchuno, String payway,
			String paybank, double paysum, String paykaisu) {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Statement stmt = null;
		Connection con = null;
		// 配列の宣言
		payDTO data = new payDTO();

		int kaisu = Integer.parseInt(paykaisu);

		try {
			// MySQLとの接続を開始

			// JDBCドライバーのロード（p1-17）
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			con.setAutoCommit(false);
			stmt = con.createStatement();

			// 追加するSQL
			String sql = "INSERT INTO `t_pay`(`kaiinno`, `juchuno`, `payway`, `paybank`, `paysum`, `paykaisu`, paygaku) "
					+ "VALUES ('"
					+ kaiinno
					+ "','"
					+ juchuno
					+ "','"
					+ payway
					+ "','"
					+ paybank
					+ "','"
					+ Math.floor(paysum)
					+ "','"
					+ kaisu + "','" + Math.floor(paysum / kaisu) + "')";

			if (stmt.executeUpdate(sql) == 1) {
				System.out.println("成功しました。");
				con.commit();
			} else {
				System.out.println("失敗しました。");
				con.rollback();
			}

			// エラー情報を取得

		} catch (ClassNotFoundException e) {
			System.out.println("ドライバのロードに失敗しました。");
		} catch (Exception e) {
			System.out.println("例外発生:" + e);

		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}// tryの終わり
		return data;
	}

	public static ArrayList<WEBjuchuDTO> infoGet(int juchuno) {

		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Connection con = null;
		Statement startment = null;

		ArrayList<WEBjuchuDTO> data = new ArrayList<WEBjuchuDTO>();


		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "SELECT * FROM t_juchu ,t_send, t_pay, t_juchushohin, t_member WHERE t_juchu.juchuno='"
					+ juchuno
					+ "' and t_juchu.juchuno=t_send.juchuno and t_juchu.juchuno=t_pay.juchuno	and t_juchu.juchuno=t_juchushohin.juchuno and t_juchu.kaiinno=t_member.kaiinno and nowpaykaisu = 1";

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 変数resultの内容を順次出力
			while (result.next()) {
				// 受注
				WEBjuchuDTO record = new WEBjuchuDTO();
				record.setJuchuno(result.getInt("juchuno"));
				record.setReceptime(result.getString("receptime"));
				record.setKyosanno(result.getString("kyosanno"));
				record.setKaiinno(result.getString("kaiinno"));
				record.setSumgaku(result.getInt("sumgaku"));
				record.setCost(result.getInt("cost"));
				record.setSumtax(result.getInt("sumtax"));
				record.setRiritu(result.getDouble("riritu"));
				record.setTantouid(result.getString("tantouid"));
				record.setHachuumu(result.getString("hachuumu"));
				record.setSeikyuumu(result.getString("seikyuumu"));
				record.setNyukinumu(result.getString("nyukinumu"));
				// 受注商品
				record.setGou(result.getString("gou"));
				record.setPage(result.getString("page"));
				record.setShohinno(result.getString("shohinno"));
				record.setSuryo(result.getInt("suryo"));
				record.setHasoubi(result.getString("hassodate"));
				record.setNotaxtotal(result.getInt("nontaxsum"));
				record.setTotal(result.getInt("taxsum"));
				record.setIro(result.getString("iro"));
				record.setGara(result.getString("gara"));
				record.setSize(result.getString("shohinsize"));
				record.setHachuumu(result.getString("hachuumu"));
				record.setTax(result.getInt("rate"));
				record.setShohinname(result.getString("shohinname"));
				record.setModel(result.getString("model"));
				record.setUnit(result.getString("unit"));
				// 会員
				record.setKyosanname(result.getString("kyosanname"));
				record.setBusho(result.getString("busho"));
				record.setKaiinname(result.getString("kaiinname"));
				record.setKaiinkana(result.getString("kaiinkana"));
				record.setKaiinntelno(result.getString("kaiintelno"));
				record.setKaiinntelkubun(result.getString("kaiintelkubun"));
				record.setKaiinnjusho(result.getString("kaiinjusho"));
				record.setKaiinnyubin(result.getString("kaiinyubin"));
				// 届先
				record.setSendjusho(result.getString("sendjusho"));
				record.setSendname(result.getString("sendname"));
				record.setSendkubun(result.getString("sendkubun"));
				record.setSendtelno(result.getString("sendtelno"));
				// 支払
				record.setPayway(result.getString("payway"));
				record.setPaybank(result.getString("paybank"));
				record.setPaysum(result.getInt("paysum"));
				record.setPaykaisu(result.getInt("paykaisu"));
				record.setPaygaku(result.getInt("paygaku"));
				record.setKaiinntelkubun(result.getString("kaiintelkubun"));

				data.add(record);
			}

		} catch (ClassNotFoundException e) {
			System.err.println("JDBCのドライバのロードに失敗しました。");
			e.printStackTrace();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		// エラー情報を取得
		catch (Exception e) {
			System.out.println("例外発生:" + e);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}

	public static void newsendadd(int juchuno, String yubin, String sjusho,
			String sendname, String sendkubun, String sendtel) {

		// String keiyakuno;
		// String keiyakuname;
		// String keiyakujusho;
		// String motoarea;

		String jusho = "〒"+yubin+"　" + sjusho;

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String pw = "";
		Connection con = null;
		Statement startment = null;

		// ArrayList<keiyakuDTO> data = new ArrayList<keiyakuDTO>();
		// //recordを入れる配列

		keiyakuDTO record = new keiyakuDTO();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, pw);
			startment = con.createStatement();
			System.out.println("接続成功");
			// SQL文

			String sql = "INSERT INTO `t_send`(`juchuno`, `sendjusho`, `sendname`, `sendkubun`, `sendtelno`) VALUES"
					+ " ("
					+ "'"
					+ juchuno
					+ "'"
					+ ","
					+ "'"
					+ jusho
					+ "'"
					+ ","
					+ "'"
					+ sendname
					+ "'"
					+ ","
					+ "'"
					+ sendkubun
					+ "'"
					+ "," + "'" + sendtel + "'" + ")";

			//System.out.println(sql);
			// 定義したSQLを実行して、実行結果を変数resultに代入
			// ResultSet result = startment.executeUpdate(sql);

			if (startment.executeUpdate(sql) != 1) {
				System.err.println("追加失敗");
			} else {
				System.out.println("追加成功");
			}

			// 変数resultの内容を順次出力
			// int x;
			// for (x=0; result.next(); x++) {
			// record.setKeiyakuno(result.getString("keiyakuno"));
			// record.setKeiyakuname(result.getString("keiyakuname"));
			// record.setKeiyakujusho(result.getString("keiyakujusho"));
			// record.setMotoarea(result.getString("motoarea"));
			// // data.add(record); //配列に追加
			// }

		} catch (ClassNotFoundException e) {
			System.out.println("JDBCドライバーのロードに失敗しました");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		// return record;
	}

	public static void yosinUpdate(int yosin, int mibarai,String kaiinno) {


		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String pw = "";
		Connection con = null;
		Statement startment = null;

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, pw);
			startment = con.createStatement();
			System.out.println("接続成功");
			// SQL文

			String sql = "UPDATE `t_member` SET  total ="+mibarai+" WHERE kaiinno='"+kaiinno+"'";


			//System.out.println(sql);
			// 定義したSQLを実行して、実行結果を変数resultに代入
			// ResultSet result = startment.executeUpdate(sql);

			if (startment.executeUpdate(sql) != 1) {
				System.err.println("追加失敗");
			} else {
				System.out.println("追加成功");
			}


		} catch (ClassNotFoundException e) {
			System.out.println("JDBCドライバーのロードに失敗しました");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	public static kaiinDTO kaiinYosin(String kaiinno) {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String pw = "";
		Connection con = null;
		Statement startment = null;

		// ArrayList<keiyakuDTO> data = new ArrayList<keiyakuDTO>();
		// //recordを入れる配列

		kaiinDTO record = new kaiinDTO();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, pw);
			startment = con.createStatement();
			System.out.println("接続成功");

			String sql2 = "SELECT * FROM `t_member` WHERE `kaiinno`='"+ kaiinno + "'";

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql2);

			// 変数resultの内容を順次出力
			int x;
			for (x = 0; result.next(); x++) {
				record.setYosin(result.getInt("yosin"));

			}

		} catch (ClassNotFoundException e) {
			System.out.println("JDBCドライバーのロードに失敗しました");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return record;
	}

}
