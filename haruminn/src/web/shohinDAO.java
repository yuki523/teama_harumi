package web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dto.shohinDTO;


public class shohinDAO {

	public static ArrayList<shohinDTO> getSoldList(int sold) {

		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Connection con = null;
		Statement startment = null;

		ArrayList<shohinDTO> data = new ArrayList<shohinDTO>();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "SELECT DISTINCT gou,page,shohinno,gazo,shohinname FROM t_shohin WHERE sold ='"+sold+"' ORDER BY sold DESC";

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 変数resultの内容を順次出力
			while (result.next()) {
				shohinDTO record = new shohinDTO();
				record.setGazo(result.getString("gazo"));
				record.setShohinname(result.getString("shohinname"));
				record.setGou(result.getString("gou"));
				record.setPage(result.getString("page"));
				record.setShohinno(result.getString("shohinno"));
				data.add(record);
			}


		} catch (ClassNotFoundException e) {
			System.err.println("JDBCのドライバのロードに失敗しました。");
			e.printStackTrace();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		// エラー情報を取得
		catch (Exception e) {
			System.out.println("例外発生:" + e);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}

	public static ArrayList<shohinDTO> getCategory(String cateno) {

		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Connection con = null;
		Statement startment = null;

		ArrayList<shohinDTO> data = new ArrayList<shohinDTO>();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "SELECT DISTINCT gou,page,shohinno,gazo, shohinname FROM t_shohin WHERE cateno ='"+cateno+"' ORDER BY cateno DESC";

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 変数resultの内容を順次出力
			while (result.next()) {
				shohinDTO record = new shohinDTO();
				record.setGazo(result.getString("gazo"));
				record.setShohinname(result.getString("shohinname"));
				record.setGou(result.getString("gou"));
				record.setPage(result.getString("page"));
				record.setShohinno(result.getString("shohinno"));
				data.add(record);
			}


		} catch (ClassNotFoundException e) {
			System.err.println("JDBCのドライバのロードに失敗しました。");
			e.printStackTrace();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		// エラー情報を取得
		catch (Exception e) {
			System.out.println("例外発生:" + e);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}

	public static ArrayList<shohinDTO> getKensaku(String kensaku) {

		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Connection con = null;
		Statement startment = null;

		ArrayList<shohinDTO> data = new ArrayList<shohinDTO>();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "SELECT DISTINCT gou,page,shohinno,gazo,shohinname FROM t_shohin WHERE shohinname LIKE '%"+kensaku+"%' ORDER BY sold DESC";

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 変数resultの内容を順次出力
			while (result.next()) {
				shohinDTO record = new shohinDTO();
				record.setGazo(result.getString("gazo"));
				record.setShohinname(result.getString("shohinname"));
				record.setGou(result.getString("gou"));
				record.setPage(result.getString("page"));
				record.setShohinno(result.getString("shohinno"));
				data.add(record);
			}


		} catch (ClassNotFoundException e) {
			System.err.println("JDBCのドライバのロードに失敗しました。");
			e.printStackTrace();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		// エラー情報を取得
		catch (Exception e) {
			System.out.println("例外発生:" + e);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}

	public static shohinDTO getKobetsu(String gou, String page, String shohinno) {

		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Connection con = null;
		Statement startment = null;

		shohinDTO record = new shohinDTO();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "SELECT * FROM t_shohin WHERE gou ='"+gou+"' AND page ='"+page+"' AND shohinno ='"+shohinno+"'";

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 変数resultの内容を順次出力
			while (result.next()) {
				record.setGazo(result.getString("gazo"));
				record.setGou(result.getString("gou"));
				record.setPage(result.getString("page"));
				record.setShohinno(result.getString("shohinno"));
				record.setShohinname(result.getString("shohinname"));
				record.setModel(result.getString("model"));
				record.setIro(result.getString("iro"));
				record.setGara(result.getString("gara"));
				record.setSize(result.getString("size"));
				record.setTanka(result.getInt("tanka"));
				record.setUnit(result.getString("unit"));
				record.setKeiyakuno(result.getString("keiyakuno"));
				record.setSoryoumu(result.getBoolean("soryoumu"));
				record.setKigen(result.getString("kigen"));
				record.setSetumei(result.getString("setumei"));
				record.setCateno(result.getString("cateno"));
			}


		} catch (ClassNotFoundException e) {
			System.err.println("JDBCのドライバのロードに失敗しました。");
			e.printStackTrace();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		// エラー情報を取得
		catch (Exception e) {
			System.out.println("例外発生:" + e);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return record;
	}

	public static ArrayList<shohinDTO> getKobetsu2(String gou, String page, String shohinno) {

		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Connection con = null;
		Statement startment = null;

		ArrayList<shohinDTO> data = new ArrayList<shohinDTO>();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "SELECT * FROM t_shohin WHERE gou ='"+gou+"' AND page ='"+page+"' AND shohinno ='"+shohinno+"'";

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 変数resultの内容を順次出力
			while (result.next()) {
				shohinDTO record = new shohinDTO();
//				System.out.println(result.getString("shohinname"));
				record.setGazo(result.getString("gazo"));
				record.setGou(result.getString("gou"));
				record.setPage(result.getString("page"));
				record.setShohinno(result.getString("shohinno"));
				record.setShohinname(result.getString("shohinname"));
				record.setModel(result.getString("model"));
				record.setIro(result.getString("iro"));
				record.setGara(result.getString("gara"));
				record.setSize(result.getString("size"));
				record.setTanka(result.getInt("tanka"));
				record.setUnit(result.getString("unit"));
				record.setKeiyakuno(result.getString("keiyakuno"));
				record.setSoryoumu(result.getBoolean("soryoumu"));
				record.setKigen(result.getString("kigen"));
				record.setSetumei(result.getString("setumei"));
				record.setCateno(result.getString("cateno"));
				data.add(record);
			}


		} catch (ClassNotFoundException e) {
			System.err.println("JDBCのドライバのロードに失敗しました。");
			e.printStackTrace();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		// エラー情報を取得
		catch (Exception e) {
			System.out.println("例外発生:" + e);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}

	public static ArrayList<shohinDTO> getKobetsu3(String gou, String page, String shohinno, String iro, String size, String gara) {

		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Connection con = null;
		Statement startment = null;

		ArrayList<shohinDTO> data = new ArrayList<shohinDTO>();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "SELECT * FROM t_shohin WHERE gou ='"+gou+"' AND page ='"+page+"' AND shohinno ='"+shohinno+"' AND iro ='"+iro+"' AND size ='"+size+"' AND gara='"+gara+"'";

//			System.out.println(sql);

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 変数resultの内容を順次出力
			while (result.next()) {
				shohinDTO record = new shohinDTO();
//				System.out.println(result.getString("shohinname"));
				record.setGazo(result.getString("gazo"));
				record.setGou(result.getString("gou"));
				record.setPage(result.getString("page"));
				record.setShohinno(result.getString("shohinno"));
				record.setShohinname(result.getString("shohinname"));
				record.setModel(result.getString("model"));
				record.setIro(result.getString("iro"));
				record.setGara(result.getString("gara"));
				record.setSize(result.getString("size"));
				record.setTanka(result.getInt("tanka"));
				record.setUnit(result.getString("unit"));
				record.setKeiyakuno(result.getString("keiyakuno"));
				record.setSoryoumu(result.getBoolean("soryoumu"));
				record.setKigen(result.getString("kigen"));
				record.setSetumei(result.getString("setumei"));
				record.setCateno(result.getString("cateno"));
				data.add(record);
			}


		} catch (ClassNotFoundException e) {
			System.err.println("JDBCのドライバのロードに失敗しました。");
			e.printStackTrace();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		// エラー情報を取得
		catch (Exception e) {
			System.out.println("例外発生:" + e);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}

}
