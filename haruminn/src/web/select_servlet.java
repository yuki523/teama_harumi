package web;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.kaiinDTO;
import dto.shohinDTO;

/**
 * Servlet implementation class select_servlet
 */
@WebServlet("/select_servlet")
public class select_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public select_servlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		kaiinDTO member = (kaiinDTO) session.getAttribute("member");

		// ユーザーが見つかった場合
		if (request.getParameter("No").equals("1")) {
			if (member != null) {

				ArrayList<shohinDTO> moto = (ArrayList<shohinDTO>) session
						.getAttribute("selectShohin");

				if (moto == null) {
					moto = new ArrayList<shohinDTO>();
				}
				ArrayList<shohinDTO> sdto2 = shohinDAO.getKobetsu3(
						request.getParameter("gou"),
						request.getParameter("page"),
						request.getParameter("shohinno"),
						request.getParameter("iro"),
						request.getParameter("size"),
						request.getParameter("gara"));

				moto.addAll(sdto2);
				session.setAttribute("selectShohin", moto);

				RequestDispatcher rd = request.getRequestDispatcher("cart.jsp");
				rd.forward(request, response);

				// ユーザーが見つからなかった場合
			} else {
				RequestDispatcher rd = request
						.getRequestDispatcher("cartMiss.html");
				rd.forward(request, response);
			}

			// カートの商品を削除する
		} else if (request.getParameter("No").equals("2")) {
			if (member != null) {
			ArrayList<shohinDTO> select = (ArrayList<shohinDTO>) session
					.getAttribute("selectShohin");

			for (shohinDTO item : select) {
				if (request.getParameter("delno").equals(item.getUniqNo())
						&& request.getParameter("deliro").equals(item.getIro())
						&& request.getParameter("delgara").equals(
								item.getGara())
						&& request.getParameter("delsize").equals(
								item.getSize())) {
					select.remove(item);
					break;
				}
			}

			session.setAttribute("selectShohin", select);

			RequestDispatcher rd = request.getRequestDispatcher("cart.jsp");
			rd.forward(request, response);
			} else {
				RequestDispatcher rd = request
						.getRequestDispatcher("cartMiss.html");
				rd.forward(request, response);
			}
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");
		doGet(request, response);

	}

}
