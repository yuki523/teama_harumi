package web;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dto.kaiinDTO;


public class Login_DAO {

	public static ArrayList<kaiinDTO> login(String email,String pass) {

		// 接続情報を定義
				String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
				String user = "root";
				String password = "";

				Statement stmt = null;
				Connection con = null;

				// 配列の宣言

				ArrayList<kaiinDTO> data = new ArrayList<kaiinDTO>();

				try {
					// MySQLとの接続を開始

					// JDBCドライバーのロード（p1-17）
					Class.forName("org.gjt.mm.mysql.Driver");
					con = DriverManager.getConnection(url, user, password);
					Statement startment = con.createStatement();

					// 実行するSQLを定義
					String sql = "SELECT kaiinmail,pass FROM t_member WHERE kaiinmail='"+email+"'  and pass='"+pass+"'";
					// 定義したSQLを実行して、実行結果を変数resultに代入
					ResultSet result = startment.executeQuery(sql);
					// 変数resultの内容を順次出力

					while(result.next()) {
					kaiinDTO record =new kaiinDTO();
					record.setKaiinemail(result.getString("kaiinmail"));
					record.setPass(result.getString("pass"));
					data.add(record);// 配列に追加
					}

				} catch (ClassNotFoundException e) {
					System.out.println("ドライバのロードに失敗しました。");
				} catch (Exception e) {
					System.out.println("例外発生:" + e);

				} finally {
					if (stmt != null) {
						try {
							stmt.close();
						} catch (SQLException e) {

						}
					}
					if (con != null) {
						try {
							con.close();
						} catch (SQLException e) {
						}
					}

				}// tryの終わり
				return data;



	}

	public static kaiinDTO login2(String email,String pass) {

		// 接続情報を定義
				String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
				String user = "root";
				String password = "";

				PreparedStatement pstmt = null;
				Connection con = null;

				// 配列の宣言

				kaiinDTO data =null;

				try {
					// MySQLとの接続を開始

					// JDBCドライバーのロード（p1-17）
					Class.forName("org.gjt.mm.mysql.Driver");
					con = DriverManager.getConnection(url, user, password);
					pstmt = con.prepareStatement("SELECT * FROM t_member WHERE kaiinmail= ?  and pass= ?");

					pstmt.setString(1, email);
					pstmt.setString(2, pass);

					// 定義したSQLを実行して、実行結果を変数resultに代入
					ResultSet result = pstmt.executeQuery();
					// 変数resultの内容を順次出力

					while(result.next()) {
					data =new kaiinDTO();

					data.setKaiinemail(result.getString("kaiinmail"));
					data.setPass(result.getString("pass"));
					data.setKaiinname(result.getString("kaiinname"));
					data.setKaiinno(result.getString("kaiinno"));
					data.setKaiinkana(result.getString("kaiinkana"));
					data.setKaiinnyubin(result.getString("kaiinyubin"));
					data.setKaiinnjusho(result.getString("kaiinjusho"));
					data.setKaiinntelno(result.getString("kaiintelno"));
					data.setKyousanname(result.getString("kyosanname"));
					data.setKaiinntelkubun(result.getString("kaiintelkubun"));
					data.setYosin(result.getInt("yosin"));
					data.setTotal(result.getInt("total"));


					}

				} catch (ClassNotFoundException e) {
					System.out.println("ドライバのロードに失敗しました。");
				} catch (Exception e) {
					System.out.println("例外発生:" + e);

				} finally {
					if (pstmt != null) {
						try {
							pstmt.close();
						} catch (SQLException e) {
						}
					}
					if (con != null) {
						try {
							con.close();
						} catch (SQLException e) {
						}
					}

				}// tryの終わり
				return data;



	}
}
