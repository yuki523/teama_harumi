package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.kaiinDTO;

/**
 * Servlet implementation class WebShiharai
 */
@WebServlet("/WebShiharai")
public class WebShiharai extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public WebShiharai() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF8");

		HttpSession ss = request.getSession();

		kaiinDTO member = (kaiinDTO) ss.getAttribute("member");

		String kaiinname=request.getParameter("kaiinname");
		String kaiinyubin=request.getParameter("kaiinyubin");
		String kaiinjusho=request.getParameter("kaiinjusho");
		String kaiintelno=request.getParameter("kaiintelno");
		String todokesakikubun=request.getParameter("todokesakikubun");
		String rusunotoki=request.getParameter("rusunotoki");
		String kubun = request.getParameter("kubun");
		String shiharai=request.getParameter("shiharai");


		ss.setAttribute("kaiinname", kaiinname);
		ss.setAttribute("kaiinyubin",kaiinyubin);
		ss.setAttribute("kaiinjusho",kaiinjusho);
		ss.setAttribute("kaiintelno",kaiintelno);
		ss.setAttribute("todokesakikubun",todokesakikubun);
		ss.setAttribute("rusunotoki",rusunotoki);
		ss.setAttribute("kubun", kubun);


		//if(shiharai=="銀行振り込み"){
		if(shiharai.equals("銀行振り込み")){

			String ginkouname = request.getParameter("ginkouname");
			ss.setAttribute("shiharai", shiharai);
			ss.setAttribute("ginkouname", ginkouname);
			response.sendRedirect("WebSaisyukakutei.jsp");

		}else if(shiharai.equals("給与控除1回")){

			ss.setAttribute("shiharai", shiharai);
			response.sendRedirect("WebSaisyukakutei.jsp");

		}else if(shiharai.equals("給与控除分割")){

			String kaisu = request.getParameter("kaisu");
			ss.setAttribute("shiharai", shiharai);
			ss.setAttribute("kaisu", kaisu);
			response.sendRedirect("WebSaisyukakutei.jsp");
		}else{
			System.out.println(shiharai);

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");
		doGet(request, response);
	}

}
