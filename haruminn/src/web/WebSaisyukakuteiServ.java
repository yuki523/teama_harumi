package web;

import gyomu.payDAO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.juchu;
import dto.kaiinDTO;
import dto.kyosanDTO;
import dto.shohinDTO;

/**
 * Servlet implementation class WebSaisyukakuteiServ
 */
@WebServlet("/WebSaisyukakuteiServ")
public class WebSaisyukakuteiServ extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WebSaisyukakuteiServ() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession ss = request.getSession();

		ArrayList<shohinDTO> sdt = (ArrayList<shohinDTO>) ss
				.getAttribute("selectShohin");

		kaiinDTO kaiin = (kaiinDTO) ss.getAttribute("member");
		String payway = (String) ss.getAttribute("shiharai");
		String paybank = (String) ss.getAttribute("ginkouname");
		String paykaisu = (String) ss.getAttribute("kaisu");
		int paykaisu2 = 1;
		if (paykaisu != null) {
			paykaisu2 = Integer.parseInt(paykaisu);
		}

		// 会員の協賛企業No取得
		kyosanDTO kk = WebJucyuDAO.select(kaiin.getKyousanname());

		int goukei = (Integer) ss.getAttribute("goukei");
		int kyosanno = kk.getKyosanno();
		String kaiinnoo = kaiin.getKaiinno();
		double riritu = 0;

		if (payway.equals("給与控除分割")) {
			riritu = 1.03;
		}

		int paysum2 = (int) Math.floor(goukei * 1.08);

		if (payway.equals("給与控除分割")) {
			paysum2 = (int) Math.floor(paysum2 * 1.03);
		}
		//届先情報取得
		String kubun = (String) ss.getAttribute("kubun");
		String kaiinname = (String)ss.getAttribute("kaiinname");
		String kaiinyubin =(String)ss.getAttribute("kaiinyubin");
		String kaiinjusho= (String)ss.getAttribute("kaiinjusho");
		String kaiintelno =(String)ss.getAttribute("kaiintelno");

		// 与信情報取得
		int yosin = kaiin.getYosin();
		int ruikeigaku = kaiin.getTotal();
		int ruikeigakuSum = ruikeigaku+paysum2;
		String kaiinno = kaiin.getKaiinno();

		//System.out.println(ruikeigaku);
		//System.out.println(ruikeigakuSum);

		Calendar calender = Calendar.getInstance();
		int a = calender.get(Calendar.YEAR);
		String year = String.valueOf(a);
		int b = calender.get(Calendar.MONTH) + 1;
		String month = String.valueOf(b);
		int c = calender.get(Calendar.DATE);
		String date = String.valueOf(c);

		String receptime = year + "年　" + month + "月　" + date + "日";


		//与信判定
		if(yosin >= ruikeigakuSum){


		//与信情報更新
		WebJucyuDAO.yosinUpdate(yosin, ruikeigakuSum, kaiinno);

		// 受注テーブル登録
		WebJucyuDAO.addJuchu(receptime, kyosanno, kaiinnoo, goukei, riritu);

		// 受注ナンバー取得
		juchu jc = WebJucyuDAO
				.juchunoGet(receptime, kyosanno, kaiinnoo, goukei);
		int juchuno = jc.getJuchuno();

		ss.setAttribute("juchuno", juchuno);

		String gou = null;
		String pagee = null;
		String shohinno = null;
		String shohinname = null;
		String size  = null;
		String iro = null;
		String gara = null;
		int tanka = 0;
		String model = null;
		String unit = null;

		// 受注商品
		for (shohinDTO aa : sdt) {
			gou = aa.getGou();
			pagee = aa.getPage();
			shohinno = aa.getShohinno();
			shohinname = aa.getShohinname();
			size = aa.getSize();
			iro = aa.getIro();
			gara = aa.getGara();
			tanka = aa.getTanka();
			model = aa.getModel();
			unit = aa.getUnit();

			// 受注商品テーブル登録
			WebJucyuDAO.addJuchuSyohin(juchuno, gou, pagee, shohinno,
					shohinname, iro, gara, size, tanka, model, unit);

		}

		// 届先登録

		WebJucyuDAO.newsendadd(juchuno, kaiinyubin,
				kaiinjusho, kaiinname, kubun,
				kaiintelno);

		// 支払テーブルに登録
		// 支払DAO
		if (payway.equals("給与控除分割")) {

			payDAO.newpayadd(kaiinnoo, juchuno, payway, paybank, paysum2,
					paykaisu2, paysum2);

			response.sendRedirect("WebSaisyukakuteiHikae.jsp");
		} else {

			paykaisu2 = 1;

			payDAO.newpayadd(kaiinnoo, juchuno, payway, paybank, paysum2,
					paykaisu2, paysum2);

			response.sendRedirect("WebSaisyukakuteiHikae.jsp");
		}

		} else {
			response.sendRedirect("yosinNG.html");
		}


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		response.setContentType("text/html; charset=UTF-8");

		request.setCharacterEncoding("UTF-8");
		doGet(request, response);

	}

}
