package gyomu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dto.kyosanDTO;

public class KyousankigyoDAO {

    //協賛企業情報全取得
	public static ArrayList<kyosanDTO> kyosankigyo(){

		int kyosanno;
		String kyosanname;
		String kyuyo;
		String kyosanyubin;
		String kyosantelno;
		String kyosanjusho;
//		String tantouname;
//		String tantoukana;
//		String tantoubusho;
//		String tantoutelno;

         // 接続情報を定義
        String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
        String user = "root";
        String pw = "";
        Connection con = null;
        Statement startment = null;

         ArrayList<kyosanDTO> data = new ArrayList<kyosanDTO>(); //recordを入れる配列

         try {

        // MySQLとの接続を開始
        Class.forName("org.gjt.mm.mysql.Driver");
        con = DriverManager.getConnection(url, user, pw);
        startment = con.createStatement();
        System.out.println("接続成功");
        //SQL文
        String sql = "SELECT * FROM `t_kyosan`";
        System.out.println(sql);
        // 定義したSQLを実行して、実行結果を変数resultに代入
        ResultSet result = startment.executeQuery(sql);

        // 変数resultの内容を順次出力
        int x;

        for (x=0; result.next(); x++) {
        	kyosanDTO record = new kyosanDTO();

        	record.setKyosanno(result.getInt("kyosanno"));
        	record.setKyosanname(result.getString("kyosanname"));
        	record.setKyuyo(result.getString("kyuyo"));
        	record.setKyosanyubin(result.getString("kyosanyubin"));
        	record.setKyosantelno(result.getString("kyosantelno"));
        	record.setKyosanjusho(result.getString("kyosanjusho"));
//        	record.setTantoubusho(result.getString("tantoubusho"));
//        	record.setTantoukana(result.getString("tantoukana"));
//        	record.setTantouname(result.getString("tantouname"));
//        	record.setTantoutelno(result.getString("tantoutelno"));


        	data.add(record); //配列に追加
        }

        } catch(ClassNotFoundException e){
        	System.out.println("JDBCドライバーのロードに失敗しました");
        	e.printStackTrace();
        } catch(SQLException e){
        	System.out.println(e.getMessage());
        	e.printStackTrace();
        }finally{
        	if(startment !=null){
        		try{
        			startment.close();
        		} catch(SQLException e){
        		}
        	}
        	if(con != null){
        		try{
        			con.close();
        		} catch(SQLException e){
        		}
        	}
        }
		return data;
    }

	//承認した会員のみの協賛企業情報を取得
	public static kyosanDTO getChoicekyosan(String kyosanmei){

		String kyosanno;
		String kyosanname;
		String kyuyo;
		String kyosanyubin;
		String kyosantelno;
		String kyosanjusho;
		String tantouname;
		String tantoukana;
		String tantoubusho;
		String tantoutelno;
		String tukijime;

         // 接続情報を定義
        String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
        String user = "root";
        String pw = "";
        Connection con = null;
        Statement startment = null;

       //  ArrayList<kyosanDTO> data = new ArrayList<kyosanDTO>(); //recordを入れる配列

         kyosanDTO record = new kyosanDTO();
         try {

        // MySQLとの接続を開始

        Class.forName("org.gjt.mm.mysql.Driver");
        con = DriverManager.getConnection(url, user, pw);
        startment = con.createStatement();
        System.out.println("接続成功");
        //SQL文
        String sql = "SELECT * FROM `t_kyosan` WHERE `kyosanname`=" + "'" + kyosanmei + "'";
        System.out.println(sql);
        // 定義したSQLを実行して、実行結果を変数resultに代入
        ResultSet result = startment.executeQuery(sql);

        // 変数resultの内容を順次出力
        int x;
        for (x=0; result.next(); x++) {
        	record.setKyosanno(result.getInt("kyosanno"));
        	record.setKyosanname(result.getString("kyosanname"));
        	record.setKyuyo(result.getString("kyuyo"));
        	record.setKyosanyubin(result.getString("kyosanyubin"));
        	record.setKyosantelno(result.getString("kyosantelno"));
        	record.setKyosanjusho(result.getString("kyosanjusho"));
        	record.setTantoubusho(result.getString("tantoubusho"));
        	record.setTantoukana(result.getString("tantoukana"));
        	record.setTantouname(result.getString("tantouname"));
        	record.setTantoutelno(result.getString("tantoutelno"));
        	record.setTukijime(result.getString("tukijime"));


        	//data.add(record); //配列に追加
        }

        } catch(ClassNotFoundException e){
        	System.out.println("JDBCドライバーのロードに失敗しました");
        	e.printStackTrace();
        } catch(SQLException e){
        	System.out.println(e.getMessage());
        	e.printStackTrace();
        }finally{
        	if(startment !=null){
        		try{
        			startment.close();
        		} catch(SQLException e){

        		}
        	}
        	if(con != null){
        		try{
        			con.close();
        		} catch(SQLException e){

        		}
        	}
        }
		return record;
    }


	public static int kyosanhenkou(int kyosanno, String kyosanname, String kyosantelno, String kyosanyubin, String kyuyo, String kyosanjusho, String tukijime, String tantouname, String tantoukana, String tantoubusyo, String tantoutel){


//		String keiyakuno;
//		String keiyakuname;
//		String keiyakujusho;
//		String motoarea;


	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	     //ArrayList<keiyakuDTO> data = new ArrayList<keiyakuDTO>(); //recordを入れる配列

	     kyosanDTO record = new kyosanDTO();
	     int count = 0;

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功（アップデート）");
	    //SQL文
//	    String sql = "SELECT * FROM `t_keiyaku` WHERE `keiyakuname`=" + "'" + kigyoumei + "'";

	    String sql = "UPDATE t_kyosan SET "
	    		+ "kyosanname=" + "'" + kyosanname
	    		+ "',kyosanjusho =" +"'" + kyosanjusho
	    		+ "'," + "kyuyo ="  + "'" + kyuyo
	    		+ "'," +"kyosanyubin =" +"'" +kyosanyubin
	    		+"',"+"kyosantelno ="+"'" +kyosantelno
	    		+"',"+"tukijime ="+"'" +tukijime
	    		+"',"+"tantouname ="+"'" +tantouname
	    		+"',"+"tantoukana ="+"'" +tantoukana
	    		+"',"+"tantoubusho ="+"'" +tantoubusyo
	    		+"',"+"tantoutelno ="+"'" +tantoutel
	    		+"'" + " WHERE kyosanno = '" + kyosanno +"'";

	    System.out.println(sql);
	    // 定義したSQLを実行して、実行結果を変数resultに代入
//		ResultSet result = startment.executeUpdate(sql);

	    count = startment.executeUpdate(sql);

	    if(count != 1){
        	System.err.println("追加失敗");
        }else{
        	System.out.println("追加成功");
        }

//	    if(startment.executeUpdate(sql) != 1){
//        	System.err.println("むりぽ");
//        }else{
//        	System.out.println("更新成功");
//        }

	    // 変数resultの内容を順次出力
//	    int x;
//	    for (x=0; result.next(); x++) {
//	    	record.setKeiyakuno(result.getString("keiyakuno"));
//	    	record.setKeiyakuname(result.getString("keiyakuname"));
//	    	record.setKeiyakujusho(result.getString("keiyakujusho"));
//	    	record.setMotoarea(result.getString("motoarea"));
//	    //	data.add(record); //配列に追加
//	    }



	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return count;
	}

	public static int kyosandelete(int kyosanno, String kyosanname, String kyosantelno, String kyosanyubin, String kyuyo, String kyosanjusho, String tantouname, String tantoukana, String tantoubusyo, String tantoutel, String tukijime){

//		String keiyakuno;
//		String keiyakuname;
//		String keiyakujusho;
//		String motoarea;


	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	     //ArrayList<keiyakuDTO> data = new ArrayList<keiyakuDTO>(); //recordを入れる配列

	     kyosanDTO record = new kyosanDTO();
	     int count = 0;

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功（消去）");
	    //SQL文




	   String sql = "DELETE from t_kyosan WHERE `kyosanno`="+"'"+ kyosanno +"'"
	   		+ "AND `kyosanname`="+"'"+ kyosanname +"'"
	   		+ "AND `kyuyo`="+"'"+ kyuyo +"'"
	   		+ "AND `kyosantelno`="+"'"+ kyosantelno +"'"
	   		+ "AND `kyosanjusho`="+"'"+ kyosanjusho +"'"
	   		+ "AND `kyosanyubin`="+"'"+ kyosanyubin +"'"
	   		+ "AND `tukijime`="+"'"+ tukijime +"'"
	   		+ "AND `tantouname`="+"'"+ tantouname +"'"
	   		+ "AND `tantoukana`="+"'"+ tantoukana +"'"
	   		+ "AND `tantoubusho`="+"'"+ tantoubusyo +"'"
	   		+ "AND `tantoutelno`="+"'"+ tantoutel +"'";


	    System.out.println(sql);
	    // 定義したSQLを実行して、実行結果を変数resultに代入
//		ResultSet result = startment.executeUpdate(sql);

//	    if(startment.executeUpdate(sql) != 1){
//        	System.err.println("削除むりぽよ～");
//        }else{
//        	System.out.println("削除成功");
//        }

	    count = startment.executeUpdate(sql);

	    if(count != 1){
        	System.err.println("追加失敗");
        }else{
        	System.out.println("追加成功");
        }

	    // 変数resultの内容を順次出力
//	    int x;
//	    for (x=0; result.next(); x++) {
//	    	record.setKeiyakuno(result.getString("keiyakuno"));
//	    	record.setKeiyakuname(result.getString("keiyakuname"));
//	    	record.setKeiyakujusho(result.getString("keiyakujusho"));
//	    	record.setMotoarea(result.getString("motoarea"));
//	    //	data.add(record); //配列に追加
//	    }



	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return count;
	}

	public static kyosanDTO newkyosanteadd(String kyosanname, String kyuyo, String kyosanyubin,String kyosantelno,String kyosanjusho, String tukijime, String tantouname, String tantoukana, String tantoubusyo, String tantoutel){

//		String keiyakuno;
//		String keiyakuname;
//		String keiyakujusho;
//		String motoarea;


	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	     //ArrayList<keiyakuDTO> data = new ArrayList<keiyakuDTO>(); //recordを入れる配列

	     kyosanDTO record = new kyosanDTO();

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功（追加");
	    //SQL文

	    String sql = "INSERT INTO `t_kyosan`(`kyosanname`, `kyuyo`, `kyosantelno`, `kyosanyubin`, `kyosanjusho`, `tukijime`, `tantouname`, `tantoukana`, `tantoubusho`, `tantoutelno`) VALUES"
        		+ " (" + "'" + kyosanname + "'" + "," + "'" + kyuyo + "'" + "," + "'" + kyosantelno + "'"+ "," + "'" + kyosanyubin + "'" + "," + "'" + kyosanjusho +  "'"+ "," + "'" + tukijime + "'"
        		 + "," + "'" + tantouname +  "'" + "," + "'" + tantoukana +  "'" + "," + "'" + tantoubusyo +  "'" + "," + "'" + tantoutel +  "'"+ ")";

	    System.out.println(sql);
	    // 定義したSQLを実行して、実行結果を変数resultに代入
//		ResultSet result = startment.executeUpdate(sql);

	    if(startment.executeUpdate(sql) != 1){
        	System.err.println("追加しっぱい");
        }else{
        	System.out.println("追加せいこう");
        }

	    // 変数resultの内容を順次出力
//	    int x;
//	    for (x=0; result.next(); x++) {
//	    	record.setKeiyakuno(result.getString("keiyakuno"));
//	    	record.setKeiyakuname(result.getString("keiyakuname"));
//	    	record.setKeiyakujusho(result.getString("keiyakujusho"));
//	    	record.setMotoarea(result.getString("motoarea"));
//	    //	data.add(record); //配列に追加
//	    }



	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return record;
	}

	public static int searchkyosancount(String kyosanmei){

		String kyosanno;
		String kyosanname;
		String kyuyo;
		String kyosanyubin;
		String kyosantelno;
		String kyosanjusho;
		String tantouname;
		String tantoukana;
		String tantoubusho;
		String tantoutelno;
		String tukijime;

        int x=0;

         // 接続情報を定義
        String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
        String user = "root";
        String pw = "";
        Connection con = null;
        Statement startment = null;

       //  ArrayList<kyosanDTO> data = new ArrayList<kyosanDTO>(); //recordを入れる配列

         kyosanDTO record = new kyosanDTO();
         try {

        // MySQLとの接続を開始

        Class.forName("org.gjt.mm.mysql.Driver");
        con = DriverManager.getConnection(url, user, pw);
        startment = con.createStatement();
        System.out.println("接続成功");
        //SQL文
        String sql = "SELECT * FROM `t_kyosan` WHERE `kyosanname`=" + "'" + kyosanmei + "'";
        System.out.println(sql);
        // 定義したSQLを実行して、実行結果を変数resultに代入
        ResultSet result = startment.executeQuery(sql);

        // 変数resultの内容を順次出力

        for (x=0; result.next(); x++) {
        	record.setKyosanno(result.getInt("kyosanno"));
        	record.setKyosanname(result.getString("kyosanname"));
        	record.setKyuyo(result.getString("kyuyo"));
        	record.setKyosanyubin(result.getString("kyosanyubin"));
        	record.setKyosantelno(result.getString("kyosantelno"));
        	record.setKyosanjusho(result.getString("kyosanjusho"));
        	record.setTantoubusho(result.getString("tantoubusho"));
        	record.setTantoukana(result.getString("tantoukana"));
        	record.setTantouname(result.getString("tantouname"));
        	record.setTantoutelno(result.getString("tantoutelno"));
        	record.setTukijime(result.getString("tukijime"));


        	//data.add(record); //配列に追加
        }

        } catch(ClassNotFoundException e){
        	System.out.println("JDBCドライバーのロードに失敗しました");
        	e.printStackTrace();
        } catch(SQLException e){
        	System.out.println(e.getMessage());
        	e.printStackTrace();
        }finally{
        	if(startment !=null){
        		try{
        			startment.close();
        		} catch(SQLException e){

        		}
        	}
        	if(con != null){
        		try{
        			con.close();
        		} catch(SQLException e){

        		}
        	}
        }
		return x;
    }

	public static kyosanDTO getkyosanname(String kyosanid){

		String kyosanno;
		String kyosanname;
		String kyuyo;
		String kyosanyubin;
		String kyosantelno;
		String kyosanjusho;
		String tantouname;
		String tantoukana;
		String tantoubusho;
		String tantoutelno;
		String tukijime;

         // 接続情報を定義
        String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
        String user = "root";
        String pw = "";
        Connection con = null;
        Statement startment = null;

       //  ArrayList<kyosanDTO> data = new ArrayList<kyosanDTO>(); //recordを入れる配列

         kyosanDTO record = new kyosanDTO();
         try {

        // MySQLとの接続を開始

        Class.forName("org.gjt.mm.mysql.Driver");
        con = DriverManager.getConnection(url, user, pw);
        startment = con.createStatement();
        System.out.println("接続成功");
        //SQL文
        String sql = "SELECT * FROM `t_kyosan` WHERE `kyosanno`=" + "'" + kyosanid + "'";
        System.out.println(sql);
        // 定義したSQLを実行して、実行結果を変数resultに代入
        ResultSet result = startment.executeQuery(sql);

        // 変数resultの内容を順次出力
        int x;
        for (x=0; result.next(); x++) {
        	record.setKyosanno(result.getInt("kyosanno"));
        	record.setKyosanname(result.getString("kyosanname"));
        	record.setKyuyo(result.getString("kyuyo"));
        	record.setKyosanyubin(result.getString("kyosanyubin"));
        	record.setKyosantelno(result.getString("kyosantelno"));
        	record.setKyosanjusho(result.getString("kyosanjusho"));
        	record.setTantoubusho(result.getString("tantoubusho"));
        	record.setTantoukana(result.getString("tantoukana"));
        	record.setTantouname(result.getString("tantouname"));
        	record.setTantoutelno(result.getString("tantoutelno"));
        	record.setTukijime(result.getString("tukijime"));


        	//data.add(record); //配列に追加
        }

        } catch(ClassNotFoundException e){
        	System.out.println("JDBCドライバーのロードに失敗しました");
        	e.printStackTrace();
        } catch(SQLException e){
        	System.out.println(e.getMessage());
        	e.printStackTrace();
        }finally{
        	if(startment !=null){
        		try{
        			startment.close();
        		} catch(SQLException e){

        		}
        	}
        	if(con != null){
        		try{
        			con.close();
        		} catch(SQLException e){

        		}
        	}
        }
		return record;
    }


}
