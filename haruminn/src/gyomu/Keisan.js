/**
 *
 */
$(function() {

	//ドロップダウンが変更されたとき
	$('.itemcount').change(function() {

		var goukeicnt=0;
		var goukeigaku=0;
		$('.itemcount').each(function(){

			//すべてのclass=itemcountを持つドロップダウンの値を加算
			goukeicnt = goukeicnt + Number($(this).val());

			//すべてのclass=itemcountを持つドロップダウンの「yen」要素の値を加算
			goukeigaku = goukeigaku + ( Number($(this).attr("yen")) * Number($(this).val()));

		});

		$('#goukeicount').text(goukeicnt);
		$('#goukeigaku').text(goukeigaku);
	});

});