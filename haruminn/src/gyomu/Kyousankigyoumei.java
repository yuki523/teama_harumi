package gyomu;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.kyosanDTO;

/**
 * Servlet implementation class Kyousankigyoumei
 */
@WebServlet("/Kyousankigyoumei")
public class Kyousankigyoumei extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Kyousankigyoumei() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		response.setContentType("text/html; charset=UTF8"); //文字コードと返すフォーマットを決める
		PrintWriter out = response.getWriter();

		//協賛企業情報を取得する
		ArrayList<kyosanDTO> kyosankigyo = KyousankigyoDAO.kyosankigyo();

		//セッションに協賛企業情報を入れる
		session.setAttribute("kyosankigyo", kyosankigyo);
		for(kyosanDTO record : kyosankigyo){
			System.out.println( record.getKyosanname() );
		}
		response.sendRedirect("Kaiinsyougou.jsp");
//		RequestDispatcher rd = request.getRequestDispatcher("/Kaiinsyougou.jsp");
//		rd.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF8");

		doGet(request, response);
	}

}
