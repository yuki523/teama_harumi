package gyomu;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dto.harumiDTO;


public class HarumiDAO {

    //営業担当者全取得
	public static ArrayList<harumiDTO> Harumi(){

		String harumiid;
		String haruminame;

         // 接続情報を定義
        String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
        String user = "root";
        String pw = "";
        Connection con = null;
        Statement startment = null;

         ArrayList<harumiDTO> data = new ArrayList<harumiDTO>(); //recordを入れる配列

         try {
        // MySQLとの接続を開始
        Class.forName("org.gjt.mm.mysql.Driver");
        con = DriverManager.getConnection(url, user, pw);
        startment = con.createStatement();
        System.out.println("接続成功");
        //SQL文
        String sql = "SELECT * FROM `t_harumi`";
        System.out.println(sql);
        // 定義したSQLを実行して、実行結果を変数resultに代入
        ResultSet result = startment.executeQuery(sql);

        // 変数resultの内容を順次出力
        int x;
        for (x=0; result.next(); x++) {
        	harumiDTO record = new harumiDTO();
        	record.setHarumiid(result.getString("harumiid"));
        	record.setHaruminame(result.getString("haruminame"));
        	data.add(record); //配列に追加
        }

        } catch(ClassNotFoundException e){
        	System.out.println("JDBCドライバーのロードに失敗しました");
        	e.printStackTrace();
        } catch(SQLException e){
        	System.out.println(e.getMessage());
        	e.printStackTrace();
        }finally{
        	if(startment !=null){
        		try{
        			startment.close();
        		} catch(SQLException e){
        		}
        	}
        	if(con != null){
        		try{
        			con.close();
        		} catch(SQLException e){
        		}
        	}
        }
		return data;
    }




	public static harumiDTO getChoiceHarumi(String haruminame){

		String harumiid;
//		String haruminame;

	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	     harumiDTO record = new harumiDTO();

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文
	    String sql = "SELECT * FROM `t_harumi` WHERE `haruminame`=" + "'" + haruminame + "'";
	    System.out.println(sql);
	    // 定義したSQLを実行して、実行結果を変数resultに代入
	    ResultSet result = startment.executeQuery(sql);

	    // 変数resultの内容を順次出力
	    int x;
	    for (x=0; result.next(); x++) {
	    	record.setHarumiid(result.getString("harumiid"));
	    	record.setHaruminame(result.getString("haruminame"));
	    //	data.add(record); //配列に追加
	    }

	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return record;
	}



//はるみ担当変更
	public static int harumitantouhenkou(String harumiid, String haruminame,String harumiid2, String haruminame2){

	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	     harumiDTO record = new harumiDTO();
	     int count = 0;

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");

	    //SQL文
	    String sql = "UPDATE t_harumi SET harumiid=" + "'" + harumiid + "',haruminame =" +  "'" + haruminame +"'"
	    			+ " WHERE harumiid = '" + harumiid2 +"'"
	    					+"AND  haruminame ='" + haruminame2 +"'";
	     System.out.println(sql);
	    // 定義したSQLを実行して、実行結果を変数resultに代入
//	    if(startment.executeUpdate(sql) != 1){
//        	System.err.println("追加失敗");
//        }else{
//        	System.out.println("追加成功");
//        }

	    count = startment.executeUpdate(sql);

	    if(count != 1){
        	System.err.println("追加失敗");
        }else{
        	System.out.println("追加成功");
        }

	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return count;
	}




	public static int harumitantoudelete(String harumiid, String haruminame){

	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	     harumiDTO record = new harumiDTO();
	     int count = 0;

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文

	    String sql = " DELETE FROM t_harumi WHERE `harumiid`="+"'"+ harumiid +"'" + "AND `haruminame`="+"'"+ haruminame +"'";

	    System.out.println(sql);

	    count = startment.executeUpdate(sql);

	    if(count != 1){
        	System.err.println("削除失敗");
        }else{
        	System.out.println("削除成功");
        }

	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return count;
	}




	public static harumiDTO harumitantouadd(String harumiid, String haruminame){

	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	     harumiDTO record = new harumiDTO();

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文
	    String sql = "INSERT INTO `t_harumi`(`harumiid`, `haruminame`) VALUES"
        		+ " (" + "'" + harumiid + "'" + "," + "'" + haruminame + "')";

	    System.out.println(sql);

	    if(startment.executeUpdate(sql) != 1){
        	System.err.println("追加失敗");
        }else{
        	System.out.println("追加成功");
        }

	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return record;
	}
}