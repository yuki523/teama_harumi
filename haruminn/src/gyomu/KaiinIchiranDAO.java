package gyomu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dto.kaiinDTO;

public class KaiinIchiranDAO {

    //会員情報属性全取得
	public static ArrayList<kaiinDTO> Kaiin(){

		String kyousanno;
		String kaiinno;
		String busho;
		String kaiinname;
		String kaiinkana;
		String kaiinntelno;
		String kaiinntelkubun;
		String kaiinnpref;
		String kaiinnjusho;
		String kaiinnyubin;
		String kaiinemail;
		String pass;
		String joindate;
		int yosin;
		int total;

         // 接続情報を定義
        String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
        String user = "root";
        String pw = "";
        Connection con = null;
        Statement startment = null;

         ArrayList<kaiinDTO> data = new ArrayList<kaiinDTO>(); //recordを入れる配列

         try {
        // MySQLとの接続を開始
        Class.forName("org.gjt.mm.mysql.Driver");
        con = DriverManager.getConnection(url, user, pw);
        startment = con.createStatement();
        System.out.println("接続成功");
        //SQL文
        String sql = "SELECT * FROM `t_member`";
        System.out.println(sql);
        // 定義したSQLを実行して、実行結果を変数resultに代入
        ResultSet result = startment.executeQuery(sql);

        // 変数resultの内容を順次出力
        int x;
        for (x=0; result.next(); x++) {
        	kaiinDTO record = new kaiinDTO();
        	record.setKyousanname(result.getString("kyosanname"));
        	record.setKaiinno(result.getString("kaiinno"));
        	record.setBusho(result.getString("busho"));
        	record.setKaiinname(result.getString("kaiinname"));
        	record.setKaiinkana(result.getString("kaiinkana"));
        	record.setKaiinntelno(result.getString("kaiintelno"));
        	record.setKaiinntelkubun(result.getString("kaiintelkubun"));
        	record.setKaiinnpref(result.getString("kaiinpref"));
        	record.setKaiinnjusho(result.getString("kaiinjusho"));
        	record.setKaiinnyubin(result.getString("kaiinyubin"));
           	record.setKaiinemail(result.getString("kaiinmail"));
        	record.setPass(result.getString("pass"));
        	record.setJoindate(result.getString("joindate"));
        	record.setYosin(result.getInt("yosin"));
        	record.setTotal(result.getInt("total"));
        	data.add(record); //配列に追加
        }



        } catch(ClassNotFoundException e){
        	System.out.println("JDBCドライバーのロードに失敗しました");
        	e.printStackTrace();
        } catch(SQLException e){
        	System.out.println(e.getMessage());
        	e.printStackTrace();
        }finally{
        	if(startment !=null){
        		try{
        			startment.close();
        		} catch(SQLException e){
        		}
        	}
        	if(con != null){
        		try{
        			con.close();
        		} catch(SQLException e){
        		}
        	}
        }
		return data;
    }

	//協賛企業番号と会員番号から検索する
	public static ArrayList<kaiinDTO> getInputKaiin(String kyosankigyoid, String kaiinno){

		String kyousanno;
//		String kaiinno;
		String busho;
		String kaiinname;
//		String kaiinkana;
		String kaiinntelno;
		String kaiinntelkubun;
		String kaiinnpref;
		String kaiinnjusho;
		String kaiinnyubin;
		String kaiinemail;
		String pass;
		String joindate;
		int yosin;
		int total;

	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;


	     kaiinDTO record = new kaiinDTO();

	     ArrayList<kaiinDTO> data = new ArrayList<kaiinDTO>();

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文
	    String sql = "SELECT * FROM t_member, t_kyosan WHERE t_member.kyosanno = t_kyosan.kyosanno and t_member.kaiinno =" + "'" + kaiinno + "'" + " AND t_member.kyosanno=" + "'" + kyosankigyoid + "'"
	    		 + " AND t_kyosan.kyosanno=" + "'" + kyosankigyoid + "'";
	    System.out.println(sql);
	    // 定義したSQLを実行して、実行結果を変数resultに代入
	    ResultSet result = startment.executeQuery(sql);

	    // 変数resultの内容を順次出力
	    int x;
	    for (x=0; result.next(); x++) {
        	record.setKyousanname(result.getString("kyosanname"));
        	record.setKaiinno(result.getString("kaiinno"));
        	record.setBusho(result.getString("busho"));
        	record.setKaiinname(result.getString("kaiinname"));
        	record.setKaiinkana(result.getString("kaiinkana"));
        	record.setKaiinntelno(result.getString("kaiintelno"));
        	record.setKaiinntelkubun(result.getString("kaiintelkubun"));
        	record.setKaiinnpref(result.getString("kaiinpref"));
        	record.setKaiinnjusho(result.getString("kaiinjusho"));
        	record.setKaiinnyubin(result.getString("kaiinyubin"));
           	record.setKaiinemail(result.getString("kaiinmail"));
        	record.setPass(result.getString("pass"));
        	record.setJoindate(result.getString("joindate"));
        	record.setYosin(result.getInt("yosin"));
        	record.setTotal(result.getInt("total"));

        	data.add(record); //配列に追加
	    }



	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return data;
	}

	//会員情報を変更する
	public static int kaiinhenkou(int kyousanno, String kyousanname,String kaiinno,String busho,String kaiinname,
	String kaiinkana,String kaiinntelno,String kaiinntelkubun,String kaiinnpref,String kaiinnjusho,
	String kaiinnyubin,String kaiinemail,String pass,String joindate,int yosin,int total){


	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;


	     kaiinDTO record = new kaiinDTO();
	     int count = 0;

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文
	    String sql = "UPDATE t_member SET `kyosanno`="+"'"+ kyousanno +"'" +
	    		", `kyosanname`="+"'"+ kyousanname +"'" +
	    		", `kaiinno`="+"'"+ kaiinno +"'" +
	    		", `busho`="+"'"+ busho +"'"+
	    		", `kaiinname`="+"'"+ kaiinname +"'"+
	    		", `kaiinkana`="+"'"+ kaiinkana +"'"+
	    		", `kaiintelno`="+"'"+ kaiinntelno +"'"+
	    		", `kaiintelkubun`="+"'"+ kaiinntelkubun +"'"+
	    		", `kaiinpref`="+"'"+ kaiinnpref +"'"+
	    		", `kaiinjusho`="+"'"+ kaiinnjusho +"'"+
	    		", `kaiinyubin`="+"'"+ kaiinnyubin +"'"+
	    		", `kaiinmail`="+"'"+ kaiinemail +"'"+
	    		", `pass`="+"'"+ pass +"'"+
	    		", `joindate`="+"'"+ joindate +"'"+
	    			", `yosin`="+"'"+ yosin +"'"+
	    		", `total`="+"'"+ total +"'"+
	    		"WHERE kyosanno ='"+kyousanno+"' "
	    		+"AND kaiinno ='"+kaiinno+"' ";

	    System.out.println(sql);

	    count = startment.executeUpdate(sql);

	    if(count != 1){
        	System.err.println("追加失敗");
        }else{
        	System.out.println("追加成功");
        }



	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return count;
	}
	//会員情報を削除する
	public static int kaiindelete(int kyousanid,String kaiinno,String busho,String kaiinname,
			String kaiinkana,String kaiinntelno,String kaiinntelkubun,String kaiinnpref,String kaiinnjusho,
			String kaiinnyubin,String kaiinemail,String pass,String joindate,int yosin,int total){

	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;


	     kaiinDTO record = new kaiinDTO();
	     int count = 0;

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文




	    String sql = " DELETE FROM t_member WHERE `kyosanno`="+"'"+ kyousanid +"'" +
	    		"AND `kaiinno`="+"'"+ kaiinno +"'" +
	    		"AND `busho`="+"'"+ busho +"'"+
	    		"AND `kaiinname`="+"'"+ kaiinname +"'"+
	    		"AND `kaiinkana`="+"'"+ kaiinkana +"'"+
	    		"AND `kaiintelno`="+"'"+ kaiinntelno +"'"+
	    		"AND `kaiintelkubun`="+"'"+ kaiinntelkubun +"'"+
	    		"AND `kaiinpref`="+"'"+ kaiinnpref +"'"+
	    		"AND `kaiinjusho`="+"'"+ kaiinnjusho +"'"+
	    		"AND `kaiinyubin`="+"'"+ kaiinnyubin +"'"+
	    		"AND `kaiinmail`="+"'"+ kaiinemail +"'"+
	    		"AND `pass`="+"'"+ pass +"'"+
	    		"AND `joindate`="+"'"+ joindate +"'"+
	    		"AND `yosin`="+"'"+ yosin +"'"+
	    		"AND `total`="+"'"+ total +"'";





	    System.out.println(sql);

	    count = startment.executeUpdate(sql);

	    if(count != 1){
        	System.err.println("削除失敗");
        }else{
        	System.out.println("削除成功");
        }
	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return count;
	}
	//会員情報を追加する
	public static int kaiinadd(String kyousanname, String kyousanid,String kaiinno,String busho,String kaiinname,
			String kaiinkana,String kaiinntelno,String kaiinntelkubun,String kaiinnpref,String kaiinnjusho,
			String kaiinnyubin,String kaiinemail,String pass,String joindate,int yosin,int total){

	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	     kaiinDTO record = new kaiinDTO();
	     int count=0;

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文
	    String sql = "INSERT INTO t_member(kyosanname, kyosanno, `kaiinno`,  `busho`, `kaiinname`, `kaiinkana`, kaiintelno, kaiintelkubun, kaiinpref, kaiinjusho, kaiinyubin, kaiinmail, pass, joindate, yosin, total) VALUES "
		+ "('"
		+ kyousanname
		+ "','"
		+ kyousanid
		+ "','"
		+ kaiinno
		+ "','"
		+ busho
		+ "','"
		+ kaiinname
		+ "','"
		+ kaiinkana
		+ "','"
		+ kaiinntelno
		+ "','"
		+ kaiinntelkubun
		+ "','"
		+ kaiinnpref
		+ "',' "
		+ kaiinnjusho
		+ "','"
		+ kaiinnyubin
		+ "','" + kaiinemail + "','"+pass+"','"+joindate+"','"+yosin+"','"+total+"')";


	    System.out.println(sql);

	    count = startment.executeUpdate(sql);

	    if(count != 1){
        	System.err.println("追加失敗");
        }else{
        	System.out.println("追加成功");
        }



	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return count;
	}
	//協賛企業番号と会員番号から検索数を出す
	public static int searchKaiincount(String kyosankigyoid, String kaiinno){



		String kyousanno;
//		String kaiinno;
		String busho;
		String kaiinname;
//		String kaiinkana;
		String kaiinntelno;
		String kaiinntelkubun;
		String kaiinnpref;
		String kaiinnjusho;
		String kaiinnyubin;
		String kaiinemail;
		String pass;
		String joindate;
		int yosin;
		int total;

		int x=0;

	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;


	     kaiinDTO record = new kaiinDTO();

	     ArrayList<kaiinDTO> data = new ArrayList<kaiinDTO>();

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文
	    String sql = "SELECT * FROM `t_member` WHERE `kaiinno`=" + "'" + kaiinno + "'" + " AND `kyosanno`=" + "'" + kyosankigyoid + "'";
	    System.out.println(sql);
	    // 定義したSQLを実行して、実行結果を変数resultに代入
	    ResultSet result = startment.executeQuery(sql);

	    // 変数resultの内容を順次出力
	    for (x=0; result.next(); x++) {
        	record.setKyousanname(result.getString("kyosanname"));
        	record.setKaiinno(result.getString("kaiinno"));
        	record.setBusho(result.getString("busho"));
        	record.setKaiinname(result.getString("kaiinname"));
        	record.setKaiinkana(result.getString("kaiinkana"));
        	record.setKaiinntelno(result.getString("kaiintelno"));
        	record.setKaiinntelkubun(result.getString("kaiintelkubun"));
        	record.setKaiinnpref(result.getString("kaiinpref"));
        	record.setKaiinnjusho(result.getString("kaiinjusho"));
        	record.setKaiinnyubin(result.getString("kaiinyubin"));
           	record.setKaiinemail(result.getString("kaiinmail"));
        	record.setPass(result.getString("pass"));
        	record.setJoindate(result.getString("joindate"));
        	record.setYosin(result.getInt("yosin"));
        	record.setTotal(result.getInt("total"));

        	data.add(record); //配列に追加
	    }



	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return x;
	}

	//協賛企業番号から協賛企業名を取得
public static kaiinDTO getkyosanname (String kaiinno){



		String kyousanno;
//		String kaiinno;
		String busho;
		String kaiinname;
//		String kaiinkana;
		String kaiinntelno;
		String kaiinntelkubun;
		String kaiinnpref;
		String kaiinnjusho;
		String kaiinnyubin;
		String kaiinemail;
		String pass;
		String joindate;
		int yosin;
		int total;

		int x=0;

	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;


	     kaiinDTO record = new kaiinDTO();

	     ArrayList<kaiinDTO> data = new ArrayList<kaiinDTO>();

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文
	    String sql = "SELECT * FROM `t_kyosan` WHERE `kyosanno`=" + "'" + kaiinno + "'";
	    System.out.println(sql);
	    // 定義したSQLを実行して、実行結果を変数resultに代入
	    ResultSet result = startment.executeQuery(sql);

	    // 変数resultの内容を順次出力
	    for (x=0; result.next(); x++) {
        	record.setKyousanname(result.getString("kyosanname"));
        	data.add(record); //配列に追加
	    }



	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return record;
	}

}
