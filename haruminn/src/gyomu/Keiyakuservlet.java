package gyomu;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.keiyakuDTO;

/**
 * Servlet implementation class Keiyakuservlet
 */
@WebServlet("/Keiyakuservlet")
public class Keiyakuservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Keiyakuservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		response.setContentType("text/html; charset=UTF8"); //文字コードと返すフォーマットを決める


		String jspaction = request.getParameter("submittype");

		//トップページからきたとき
		if(jspaction==null){

			//セレクトタブ用の契約企業名一覧取得
			ArrayList<keiyakuDTO> data = KeiyakuDAO.Keiyaku();
			session.setAttribute("keiyakunamelist", data);

			request.setAttribute("message", "企業名を選択してください");

			RequestDispatcher rd=request.getRequestDispatcher("Keiyaku.jsp");
			rd.forward(request,response);


		//検索する
		}else if(jspaction.equals("検索")){
			//契約企業名を取得
			String KeiyakukigyouName = request.getParameter("keiyakukigyoumei");
			session.setAttribute("choicekigyoumei", KeiyakukigyouName);

			//選択された企業の情報を取得
			keiyakuDTO kigyouinfo = KeiyakuDAO.getChoiceKeiyaku(KeiyakukigyouName);
			session.setAttribute("keiyakuinfo", kigyouinfo);
			request.setAttribute("keiyakuinfo", kigyouinfo);

//			System.out.println(kigyouinfo.getKeiyakuname() + "あい");
//			session.getAttribute("keiyakuinfo");
//
//			keiyakuDTO Kaiininfo2 = (keiyakuDTO)session.getAttribute("keiyakuinfo");
//				System.out.println( Kaiininfo2.getKeiyakuname() + "うえ");

			//セレクトタブ用の契約企業名一覧取得
			ArrayList<keiyakuDTO> data = KeiyakuDAO.Keiyaku();
			session.setAttribute("keiyakunamelist", data);

			request.setAttribute("message", "契約企業情報を変更、あるいは消去してください");

			RequestDispatcher rd=request.getRequestDispatcher("Keiyaku.jsp");
			rd.forward(request,response);

		//変更する
		}else if(jspaction.equals("変更")){
			keiyakuDTO keiyakuinfo = (keiyakuDTO)session.getAttribute("keiyakuinfo");

			//入力された変更を取得する
			int keiyakubangou = keiyakuinfo.getKeiyakuno();
			String keiyakuname = request.getParameter("keiyakuname");
			String keiyakujusho = request.getParameter("keiyakujusho");
			String keiyakumotoarea = request.getParameter("keiyakumotoarea");

			int count =KeiyakuDAO.keiyakutenhenkou(keiyakubangou, keiyakuname, keiyakujusho, keiyakumotoarea);

			request.setAttribute("count", count);

			if(count==0){
				request.setAttribute("message", "変更に失敗しました");
			}else{
				request.setAttribute("message", "変更しました");
			}

			//セレクトタブ用の契約企業名一覧取得
			ArrayList<keiyakuDTO> data = KeiyakuDAO.Keiyaku();
			session.setAttribute("keiyakunamelist", data);

			RequestDispatcher rd=request.getRequestDispatcher("Keiyaku.jsp");
			rd.forward(request,response);

		//削除する
		}else if(jspaction.equals("削除")){
			keiyakuDTO keiyakuinfo = (keiyakuDTO)session.getAttribute("keiyakuinfo");

			//入力された変更を取得する
			int keiyakubangou = keiyakuinfo.getKeiyakuno();
			String keiyakuname = request.getParameter("keiyakuname");
			String keiyakujusho = request.getParameter("keiyakujusho");
			String keiyakumotoarea = request.getParameter("keiyakumotoarea");

			int count = KeiyakuDAO.keiyakutendelete(keiyakubangou, keiyakuname, keiyakujusho, keiyakumotoarea);

			request.setAttribute("count", count);

			if(count==0){
				request.setAttribute("message", "削除に失敗しました");
			}else{
				request.setAttribute("message", "削除しました");
			}

			//セレクトタブ用の契約企業名一覧取得
			ArrayList<keiyakuDTO> data = KeiyakuDAO.Keiyaku();
			session.setAttribute("keiyakunamelist", data);

			RequestDispatcher rd=request.getRequestDispatcher("Keiyaku.jsp");
			rd.forward(request,response);

		//追加する
		}else if(jspaction.equals("追加")){

			//入力された情報を取得する
			String keiyakuname = request.getParameter("newkeiyakuname");
			String keiyakujusho = request.getParameter("newkeiyakujusho");
			String keiyakumotoarea = request.getParameter("newkeiyakumotoarea");

			int count = KeiyakuDAO.searchKeiyaku(keiyakuname);

			if(count==0){
				KeiyakuDAO.newkeiyakuteadd(keiyakuname, keiyakujusho, keiyakumotoarea);
				request.setAttribute("message", "追加しました");
			}else{
				request.setAttribute("message", "同じ企業名が存在します");
			}





			request.setAttribute("message", "追加しました");

			//セレクトタブ用の契約企業名一覧取得
			ArrayList<keiyakuDTO> data = KeiyakuDAO.Keiyaku();
			session.setAttribute("keiyakunamelist", data);

			RequestDispatcher rd=request.getRequestDispatcher("Keiyaku.jsp");
			rd.forward(request,response);

		}



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF8");

		doGet(request, response);
	}

}
