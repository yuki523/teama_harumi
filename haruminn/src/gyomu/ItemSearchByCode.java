package gyomu;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dto.shohinDTO;

/**
 * Servlet implementation class ItemSearchByCode
 */
@WebServlet("/ItemSearchByCode")
public class ItemSearchByCode extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemSearchByCode() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//日本語を使えるように文字コード設定
		request.setCharacterEncoding("UTF8");

		//DAO経由でカテゴリ名の取得
		ArrayList<shohinDTO> shohindata = ShohinDao.getShohin(
				request.getParameter("g"),
				request.getParameter("p"),
				request.getParameter("n")
				);

		//jsonで返す
		Gson gson = new Gson();
		response.setContentType("text/json;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.print(request.getParameter("callback") + "(" +gson.toJson(shohindata) + ")");
	}

}
