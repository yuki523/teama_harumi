package gyomu;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import web.WebJucyuDAO;
import dto.juchu;
import dto.kaiinDTO;
import dto.kyosanDTO;
import dto.shohinDTO;

/**
 * Servlet implementation class Tymonuketukehyo
 */
@WebServlet("/Tymonuketukehyo")
public class Tymonuketukehyo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Tymonuketukehyo() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		response.setContentType("text/html; charset=UTF8"); // 文字コードと返すフォーマットを決める

		kyosanDTO kyosankigyo = (kyosanDTO) session
				.getAttribute("kaiinkyousankigyo");
		kaiinDTO kaiininfo = (kaiinDTO) session.getAttribute("kaiininfo");

		// 受注関係
		String juchutantouid;
		String tyumonday;
		int juchuno;

		// ご依頼主関係
		String kyousankigyoumei;
		int kyousankigyoubangou;
		String syozokubusyo;
		String chumonsyaname;
		String chumonsyakana;
		String syainno;
		String chumonsyatel;
		String telkubun;
		int yosin;
		int ruikeigaku;

		// お届け先関係
		String todokesakiyubin;
		String todokesakipref;
		String todokesakijusho;
		String todokesakiname;
		String todokesakitel;
		String todokesakikubun;
		String rusu;

		// 注文商品
		String[] syouhingou = new String[6];
		String[] syouhinpage = new String[6];
		String[] syouhinno = new String[6];
		String[] syouhinmei = new String[6];
		String[] syouhinmodel = new String[6];
		String[] syouhincolor = new String[6];
		String[] syouhinpattern = new String[6];
		String[] syouhinsize = new String[6];
		int[] syouhintanka = new int[6];
		String[] syouhintani = new String[6];
		int[] syouhinsuryo = new int[6];
		int[] syouhinmoney = new int[6];// 単価×数量
		int[] othermoney = new int[6];// 他実費
		int[] totalsyouhinmoney = new int[6]; // 合計金額
		String[] explain = new String[6];// 補足
		String[] hatyusaki = new String[6];// 契約企業番号(発注先)
		// String[] hasoubi = new String[6];//発送日

		int totalsuryo; // 数量合計
		int totalmoney; // 合計金額
		int totalothermoney; // 他実費合計
		int alltotalmoney; // 総合計金額

		// 支払い関係
		String hurikomibank; // 振り込み元銀行
		int paycount;
		String howtopay;
		String seikyukubun;

		// 受注受付票から取得する
		juchutantouid = request.getParameter("jyutyutanto");
		tyumonday = session.getAttribute("year") + "年"
				+ session.getAttribute("month") + "月"
				+ session.getAttribute("date") + "日";

		// ご依頼主関係
		kyousankigyoumei = kyosankigyo.getKyosanname();
		kyousankigyoubangou = kyosankigyo.getKyosanno();
		syozokubusyo = kaiininfo.getBusho();
		chumonsyaname = request.getParameter("todokesaki");
		chumonsyakana = kaiininfo.getKaiinkana();
		syainno = kaiininfo.getKaiinno();
		chumonsyatel = kaiininfo.getKaiinntelno();
		telkubun = request.getParameter("telkubun");
		yosin = kaiininfo.getYosin();
		ruikeigaku = kaiininfo.getTotal();

		// お届け先関係
		todokesakiyubin = request.getParameter("yubin");
		todokesakipref = request.getParameter("todokesakipref");
		todokesakijusho = request.getParameter("todokesakijusho");
		todokesakiname = request.getParameter("name");
		todokesakitel = request.getParameter("tel");
		todokesakikubun = request.getParameter("todokesaki");
		rusu = request.getParameter("rusu");

		for (int i = 0; i < 6; i++) {
			int j = i + 1;

			// 注文商品
			if (request.getParameter("syohinname" + j).equals("")) {
				syouhintanka[i] = 0;
				syouhinsuryo[i] = 0;
				syouhinmoney[i] = 0;
				othermoney[i] = 0;
				totalsyouhinmoney[i] = 0;

			} else {
				syouhingou[i] = request.getParameter("gou" + j);
				syouhinpage[i] = request.getParameter("page" + j);
				syouhinno[i] = request.getParameter("No" + j);
				syouhinmei[i] = request.getParameter("syohinname" + j);
				System.out.println(syouhinmei[i]);
				syouhinmodel[i] = request.getParameter("kata" + j);
				syouhincolor[i] = request.getParameter("iro" + j);
				syouhinpattern[i] = request.getParameter("gara" + j);
				syouhinsize[i] = request.getParameter("size" + j);
				System.out.println("[" + request.getParameter("tanka" + j)
						+ "]");
				syouhintanka[i] = Integer.parseInt(request.getParameter("tanka"
						+ j));
				syouhintani[i] = request.getParameter("tani" + j);
				syouhinsuryo[i] = Integer.parseInt(request.getParameter("suryo"
						+ j));
				syouhinmoney[i] = Integer.parseInt(request
						.getParameter("kingaku" + j));

				if (request.getParameter("jippi" + j).equals("")) {
					othermoney[i] = 0;
				} else {
					othermoney[i] = Integer.parseInt(request
							.getParameter("jippi" + j));
				}
				totalsyouhinmoney[i] = Integer.parseInt(request
						.getParameter("gokei" + j));
				explain[i] = request.getParameter("hosoku" + j);
				// hatyusaki[i] = Integer.parseInt(request.getParameter("hattyu"
				// + i));
				// hasoubi[i] = request.getParameter("hassou");
			}

		}

		totalsuryo = 0;
		totalmoney = 0;
		totalothermoney = 0;
		alltotalmoney = 0;

		for (int i = 0; i < 6; i++) {
			totalsuryo = totalsuryo + syouhinsuryo[i];
			totalmoney = totalmoney + syouhinmoney[i];
			totalothermoney = totalothermoney + othermoney[i];
			alltotalmoney = alltotalmoney + totalsyouhinmoney[i];
		}

		// 支払い関係
		hurikomibank = request.getParameter("ginkoname");
		paycount = Integer.parseInt(request.getParameter("bunkatu"));
		howtopay = request.getParameter("radio3");
		seikyukubun = request.getParameter("seikyukubun");
		int totalpay;

		// はるみ担当者の名前からid取得
		// System.out.println("はるみ担当者の名前からid取得");
		// harumiDTO a = HarumiDAO.getChoiceHarumi(juchutantouid);
		// String harumiid = a.getHarumiid();

		String harumiid = juchutantouid;
		System.out.println(juchutantouid);

		// 分割の場合、利率を３％に設定
		double riritu;
		if (paycount == 1) {
			riritu = 1.00;
		} else {
			riritu = 1.03;
		}
		totalpay = (int) (alltotalmoney * riritu);

		// 発注先を取得する

		for (int i = 0; i < 6; i++) {
			int j = i + 1;
			shohinDTO data = ShohinDao.getShohin2(syouhingou[i],
					syouhinpage[i], syouhinno[i]);
			hatyusaki[i] = data.getKeiyakuno();
		}

		int ruikeigakuSum = ruikeigaku + totalpay;
//
//		for(int i=0; i<6; i++){
//			if(syouhingou[i]!="" || syouhinpage[i]!="" || syouhinno[i]!="" && syouhinmei[i]==""){
//				response.sendRedirect("Falsejutyu.html");
//			}
//		}

		if (yosin >= ruikeigakuSum) {

			//与信の更新
			WebJucyuDAO.yosinUpdate(yosin, ruikeigakuSum, syainno);

			// 受注テーブルに登録し、注文番号を取得
			System.out.println("受注テーブルに登録し、注文番号を取得");
			juchu b = JutyuDAO.juchuadd(tyumonday, kyousankigyoubangou,
					syainno, totalmoney, totalothermoney, alltotalmoney,
					harumiid, riritu);
			juchuno = b.getJuchuno();




			// 受注商品テーブルに登録
			for (int i = 0; i < 6; i++) {
				int j = i + 1;
				if (request.getParameter("syohinname" + j).equals("")) {

				} else {
					System.out.println("受注商品テーブルに登録");
					JutyushohinDAO.juchushohinadd(juchuno, syouhingou[i],
							syouhinpage[i], syouhinno[i], syouhinsuryo[i],
							syouhinmoney[i], totalsyouhinmoney[i], explain[i],
							syouhincolor[i], syouhinpattern[i], syouhinsize[i],
							syouhintanka[i], syouhinmei[i], syouhintani[i]);
				}
			}

			// 届先テーブルに登録
			System.out.println("届先テーブルに登録");
			SendDAO.newsendadd(juchuno, todokesakiyubin, todokesakijusho,
					todokesakiname, todokesakikubun, rusu, todokesakitel);

			// 支払いテーブルに登録
			System.out.println("支払いテーブルに登録");
			payDAO.newpayadd(syainno, juchuno, howtopay, hurikomibank,
					totalpay, paycount, alltotalmoney);

			response.sendRedirect("Ordersuccess.html");

		} else {
			response.sendRedirect("GyosinNG.html");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF8");

		doGet(request, response);
	}

}
