package gyomu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.kaiinDTO;
import dto.kyosanDTO;

/**
 * Servlet implementation class Kaiinservlet
 */
@WebServlet("/Kaiinservlet")
public class Kaiinservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Kaiinservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		response.setContentType("text/html; charset=UTF8"); //文字コードと返すフォーマットを決める

		String jspaction = request.getParameter("submittype");

		//トップページからきたとき
		if(jspaction==null){

			//セレクトタブ用の会員情報一覧取得
//			ArrayList<kaiinDTO> data = KaiinIchiranDAO.Kaiin();
//			session.setAttribute("kaiinlist", data);

			request.setAttribute("message", "協賛企業名を選択してください");

			//協賛企業情報を取得する
			ArrayList<kyosanDTO> kyosankigyo = KyousankigyoDAO.kyosankigyo();
			//セッションに協賛企業情報を入れる
			session.setAttribute("kyosankigyo", kyosankigyo);

			//入会日を取得
			Calendar calender =Calendar.getInstance();
			int a = calender.get(Calendar.YEAR);
			String year = String.valueOf(a);
			int b = calender.get(Calendar.MONTH) + 1;
			String month = String.valueOf(b);
			int c = calender.get(Calendar.DATE);
			String date = String.valueOf(c);

			String d = a + "年" + b + "月" + c + "日";

			//セッションに入会日を保存
			session.setAttribute("nyukaiday", d);

			String judge ="トップから";

			request.setAttribute("judge", judge);


			System.out.println("あ");
			RequestDispatcher rd=request.getRequestDispatcher("KaiinIchiran.jsp");
			rd.forward(request,response);

		//検索する
		}else if(jspaction.equals("検索")){
			//協賛企業名、会員番号を取得
			String kyosankigyoid = request.getParameter("kigyoumei");
			String kaiinno = request.getParameter("searchkaiinno");
			session.setAttribute("inputkaiinno", kaiinno);

			//選択された会員の情報を取得

			int count = KaiinIchiranDAO.searchKaiincount(kyosankigyoid, kaiinno);

			if(count==0){
				request.setAttribute("message", "該当する会員が存在しません");
			}else{
				ArrayList <kaiinDTO> getkaiinjouhou = KaiinIchiranDAO.getInputKaiin(kyosankigyoid, kaiinno);

				session.setAttribute("getkaiinjouhou", getkaiinjouhou);
				request.setAttribute("getkaiinjouhou", getkaiinjouhou);
				request.setAttribute("message", "会員情報を変更、あるいは消去してください");
				kaiinDTO a = KaiinIchiranDAO.getkyosanname(kyosankigyoid);
//				String kyosanname = a.getKyousanname();
//				session.setAttribute("kyosanname", kyosanname);
			}

//			ArrayList <kaiinDTO> getkaiinjouhou = KaiinIchiranDAO.getInputKaiin(kyosankigyomei, kaiinno);
//			session.setAttribute("getkaiinjouhou", getkaiinjouhou);
//			request.setAttribute("getkaiinjouhou", getkaiinjouhou);



//			//会員フリガナ取得
//			ArrayList<kaiinDTO> data = KaiinIchiranDAO.Kaiin();
//			session.setAttribute("kaiinlist", data);



			RequestDispatcher rd=request.getRequestDispatcher("KaiinIchiran.jsp");
			rd.forward(request,response);

		//変更する
		}else if(jspaction.equals("変更")){
			kaiinDTO kaiininfo = (kaiinDTO)session.getAttribute("kaiininfo");

			//入力された変更を取得する
			String kyousanname = request.getParameter("kyousaname");
			String kaiinno = request.getParameter("kaiinno");
			System.out.println(kaiinno +"kaiinno");
			String busho = request.getParameter("busho");
			String kaiinname = request.getParameter("kaiinname");
			String kaiinkana = request.getParameter("kaiinkana");
			String kaiinntelno = request.getParameter("kaiinntelno");
			String kaiinntelkubun = request.getParameter("kaiinntelkubun");
			String kaiinnpref = request.getParameter("kaiinnpref");
			String kaiinnjusho = request.getParameter("kaiinnjusho");
			String kaiinnyubin = request.getParameter("kaiinnyubin");
			String kaiinemail = request.getParameter("kaiinemail");
			String pass = request.getParameter("pass");
			String joindate = request.getParameter("joindate");
			int yosin = Integer.parseInt(request.getParameter("yosin"));
			int total = Integer.parseInt(request.getParameter("total"));

			kyosanDTO a  = KyousankigyoDAO.getChoicekyosan(kyousanname);
			int kyousanno = a.getKyosanno();

			//変更数を数える
			int count = KaiinIchiranDAO.kaiinhenkou(kyousanno, kyousanname,kaiinno,busho,kaiinname,kaiinkana, kaiinntelno, kaiinntelkubun,
					kaiinnpref, kaiinnjusho,kaiinnyubin, kaiinemail, pass, joindate, yosin, total);

			request.setAttribute("count", count);

			if(count==0){
				request.setAttribute("message", "変更に失敗しました");
			}else{
				request.setAttribute("message", "変更しました");
			}

			//会員名一覧取得
			ArrayList<kaiinDTO> data = KaiinIchiranDAO.Kaiin();
			session.setAttribute("kaiinlist", data);

			RequestDispatcher rd=request.getRequestDispatcher("KaiinIchiran.jsp");
			rd.forward(request,response);

		//削除する
		}else if(jspaction.equals("削除")){
			kaiinDTO kaiininfo = (kaiinDTO)session.getAttribute("kaiininfo");

			//入力された変更を取得する
			String kyousanname = request.getParameter("kyousaname");
			String kaiinno = request.getParameter("kaiinno");
			System.out.println(kaiinno +"kaiinno");
			String busho = request.getParameter("busho");
			String kaiinname = request.getParameter("kaiinname");
			String kaiinkana = request.getParameter("kaiinkana");
			String kaiinntelno = request.getParameter("kaiinntelno");
			String kaiinntelkubun = request.getParameter("kaiinntelkubun");
			String kaiinnpref = request.getParameter("kaiinnpref");
			String kaiinnjusho = request.getParameter("kaiinnjusho");
			String kaiinnyubin = request.getParameter("kaiinnyubin");
			String kaiinemail = request.getParameter("kaiinemail");
			String pass = request.getParameter("pass");
			String joindate = request.getParameter("joindate");
			int yosin = Integer.parseInt(request.getParameter("yosin"));
			int total = Integer.parseInt(request.getParameter("total"));

			kyosanDTO a  = KyousankigyoDAO.getChoicekyosan(kyousanname);
			int kyousanno = a.getKyosanno();


			//削除数を数える
			int count = KaiinIchiranDAO.kaiindelete(kyousanno,kaiinno,busho,kaiinname,kaiinkana, kaiinntelno, kaiinntelkubun,
					kaiinnpref, kaiinnjusho,kaiinnyubin, kaiinemail, pass, joindate, yosin, total);

			request.setAttribute("count", count);


			if(count==0){
				request.setAttribute("message", "削除に失敗しました");
			}else{
				request.setAttribute("message", "削除しました");
			}

			//会員名一覧取得
			ArrayList<kaiinDTO> data = KaiinIchiranDAO.Kaiin();
			session.setAttribute("kaiinlist", data);

			RequestDispatcher rd=request.getRequestDispatcher("KaiinIchiran.jsp");
			rd.forward(request,response);

		//追加する
		}else if(jspaction.equals("追加")){
			kaiinDTO kaiininfo = (kaiinDTO)session.getAttribute("kaiininfo");

			//入力された情報を取得する
			String kigyouid= request.getParameter("newkigyoumei");
			String kaiinno = request.getParameter("newkaiinno");
			String busho = request.getParameter("newbusho");
			String kaiinname = request.getParameter("newkaiinname");
			String kaiinkana = request.getParameter("newkaiinkana");
			String tel1 = request.getParameter("newkaiinntelno1");
			String tel2 = request.getParameter("newkaiinntelno2");
			String tel3 = request.getParameter("newkaiinntelno3");
			String kaiinntelno = tel1 + tel2 + tel3;
			String kaiinntelkubun = request.getParameter("newkaiinntelkubun");
			String kaiinnpref = request.getParameter("newkaiinnpref");
			String kaiinnjusho = request.getParameter("newkaiinnjusho");
			String yubin1 = request.getParameter("newkaiineyubin1");
			String yubin2 = request.getParameter("newkaiineyubin2");
			String kaiinnyubin = yubin1 + "-"+ yubin2;
			String kaiinemail = request.getParameter("newkaiinemail");
			String pass = request.getParameter("newpass");
			String joindate = request.getParameter("newjoindate");
			int yosin = Integer.parseInt(request.getParameter("newyosin"));
			int total = Integer.parseInt(request.getParameter("newtotal"));

			kyosanDTO a = KyousankigyoDAO.getkyosanname(kigyouid);
			String kyousanname = a.getKyosanname();



			int count =KaiinIchiranDAO.kaiinadd(kyousanname,kigyouid,kaiinno,busho,kaiinname,kaiinkana, kaiinntelno, kaiinntelkubun,
					kaiinnpref, kaiinnjusho,kaiinnyubin, kaiinemail, pass, joindate, yosin, total);

			request.setAttribute("count", count);

			request.setAttribute("message", "追加しました");

			//会員名一覧取得
			ArrayList<kaiinDTO> data = KaiinIchiranDAO.Kaiin();
			session.setAttribute("kaiinlist", data);



			RequestDispatcher rd=request.getRequestDispatcher("KaiinIchiran.jsp");
			rd.forward(request,response);

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF8");
		doGet(request, response);
	}

}
