package gyomu;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.shohinDTO;

/**
 * Servlet implementation class Shohinitiranservlet
 */
@WebServlet("/Shohinitiranservlet")
public class Shohinitiranservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Shohinitiranservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		response.setContentType("text/html; charset=UTF8"); //文字コードと返すフォーマットを決める
		String jspaction = request.getParameter("submittype");


		request.setAttribute("message", "ようこそ");

		//トップページからきたとき
		if(jspaction==null){
				session.removeAttribute("shohininfo");

				request.setAttribute("message", "");
				RequestDispatcher rd=request.getRequestDispatcher("Gshohinitiran.jsp");
				rd.forward(request,response);




		//一覧表示する
		}else if(jspaction.equals("一覧表示")){


			ArrayList<shohinDTO> data = ShohinitiranDAO.Shohin();
			session.setAttribute("shohinnamelist", data);

			request.setAttribute("message", "表示しました");

			RequestDispatcher rd=request.getRequestDispatcher("Gshohinitiran.jsp");
			rd.forward(request,response);



		//検索する
		}else if(jspaction.equals("番号検索")){

			String shohinno = request.getParameter("shohinno");
			String gou = request.getParameter("gou");
			String page = request.getParameter("page");

			ArrayList<shohinDTO> shohininfo = ShohinitiranDAO.getKensaku(gou,page,shohinno);

			session.setAttribute("shohininfo", shohininfo);

			request.setAttribute("message", "検索しました");
			RequestDispatcher rd=request.getRequestDispatcher("Gshohinitiran.jsp");
			rd.forward(request,response);

		//変更する
		}else if(jspaction.equals("変更")){


				//入力された変更を取得する
				String shohinname = request.getParameter("shohinname");
				String gou = request.getParameter("gou");
				String page = request.getParameter("page");
				String shohinno = request.getParameter("shohinno");
				String iro = request.getParameter("iro");
				String gara = request.getParameter("gara");
				String size = request.getParameter("size");
				String model = request.getParameter("model");
				int tanka = 0;
				if(request.getParameter("tanka").length()>0){
					tanka =Integer.parseInt(request.getParameter("tanka"));
				}
				String unit = request.getParameter("unit");
				String keiyakuno = request.getParameter("keiyakuno");
				String release = request.getParameter("release");
				String kigen = request.getParameter("kigen");
				//boolean soryoumu = true;
				//= request.getParameter("newsoryoumu");
				String boxsize = request.getParameter("boxsize");
				String setumei = request.getParameter("setumei");
				String cateno = request.getParameter("cateno");
				int sold = 0;
				if(request.getParameter("sold").length()>0){
					sold = Integer.parseInt(request.getParameter("sold"));
				}
				String gazo = request.getParameter("gazo");

				//変更前データ一部取得
				String shohinname2 = request.getParameter("shohinname2");
				String gou2 = request.getParameter("gou2");
				String page2 = request.getParameter("page2");
				String shohinno2 = request.getParameter("shohinno2");
				String iro2 = request.getParameter("iro2");
				String gara2 = request.getParameter("gara2");
				String size2 = request.getParameter("size2");


				ShohinitiranDAO.Shohinupdate(shohinname,gou,page,shohinno,
						iro, gara,size,model,tanka,unit,
						keiyakuno,release, kigen, boxsize,
						setumei,cateno,sold, gazo,shohinname2,gou2,page2,shohinno2,
						iro2, gara2,size2);

				request.setAttribute("message", "変更しました");

				ArrayList<shohinDTO> shohininfo = ShohinitiranDAO.getKensaku2(gou,page,shohinno);

				session.setAttribute("shohininfo", shohininfo);

				RequestDispatcher rd=request.getRequestDispatcher("Gshohinitiran.jsp");
				rd.forward(request,response);



		//削除する
		}else if(jspaction.equals("削除")){
				//shohinDTO shohininfo = (shohinDTO)session.getAttribute("shohininfo");


			System.out.println("削除");

			//入力された変更を取得する
			String shohinname = request.getParameter("shohinname");
			String gou = request.getParameter("gou");
			String page = request.getParameter("page");
			String shohinno = request.getParameter("shohinno");
			String iro = request.getParameter("iro");
			String gara = request.getParameter("gara");
			String size = request.getParameter("size");
			String model = request.getParameter("model");

			ShohinitiranDAO.Shohindelete(shohinname,gou,page,shohinno,iro, gara,size,model);

			request.setAttribute("message", "削除しました");

			ArrayList<shohinDTO> shohininfo = ShohinitiranDAO.getKensaku(gou,page,shohinno);

			session.setAttribute("shohininfo", shohininfo);

			RequestDispatcher rd=request.getRequestDispatcher("Gshohinitiran.jsp");
			rd.forward(request,response);

		//追加する
		}else if(jspaction.equals("追加")){

			//入力された情報を取得する
					String shohinname = request.getParameter("newshohinname");
					String gou = request.getParameter("newgou");
					String page = request.getParameter("newpage");
					String shohinno = request.getParameter("newshohinno");
					String iro = request.getParameter("newiro");
					String gara = request.getParameter("newgara");
					String size = request.getParameter("newsize");
					String model = request.getParameter("newmodel");
					int tanka = 0;
					if(request.getParameter("newtanka").length()>0){
						tanka =Integer.parseInt(request.getParameter("newtanka"));
					}
					String unit = request.getParameter("newunit");
					String keiyakuno = request.getParameter("newkeiyakuno");
					String release = request.getParameter("newrelease");
					String kigen = request.getParameter("newkigen");
					//boolean soryoumu = true;
					//= request.getParameter("newsoryoumu");
					String boxsize = request.getParameter("newboxsize");
					String setumei = request.getParameter("newsetumei");
					String cateno = request.getParameter("newcateno");
					int sold = 0;
					if(request.getParameter("newsold").length()>0){
						sold = Integer.parseInt(request.getParameter("newsold"));
					}
					String gazo = request.getParameter("newgazo");

					System.out.println("追加成功");

					ShohinitiranDAO.newShohinadd(shohinname,gou,page,shohinno,
													iro, gara,size,model,tanka,unit,
													keiyakuno,release, kigen,boxsize,
													setumei,cateno,sold, gazo);

						request.setAttribute("message", "追加しました");

						RequestDispatcher rd=request.getRequestDispatcher("Gshohinitiran.jsp");
						rd.forward(request,response);



				}



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF8");

		doGet(request, response);

	}

}
