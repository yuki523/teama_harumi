package gyomu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dto.keiyakuDTO;

public class KeiyakuDAO {

    //契約販売店属性全取得
	public static ArrayList<keiyakuDTO> Keiyaku(){

		String keiyakuno;
		String keiyakuname;
		String keiyakujusho;
		String motoarea;

         // 接続情報を定義
        String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
        String user = "root";
        String pw = "";
        Connection con = null;
        Statement startment = null;

         ArrayList<keiyakuDTO> data = new ArrayList<keiyakuDTO>(); //recordを入れる配列

         try {
        // MySQLとの接続を開始
        Class.forName("org.gjt.mm.mysql.Driver");
        con = DriverManager.getConnection(url, user, pw);
        startment = con.createStatement();
        System.out.println("接続成功");
        //SQL文
        String sql = "SELECT * FROM `t_keiyaku`";
        System.out.println(sql);
        // 定義したSQLを実行して、実行結果を変数resultに代入
        ResultSet result = startment.executeQuery(sql);

        // 変数resultの内容を順次出力
        int x;
        for (x=0; result.next(); x++) {
        	keiyakuDTO record = new keiyakuDTO();
        	record.setKeiyakuno(result.getInt("keiyakuno"));
        	record.setKeiyakuname(result.getString("keiyakuname"));
        	record.setKeiyakujusho(result.getString("keiyakujusho"));
        	record.setMotoarea(result.getString("motoarea"));
        	data.add(record); //配列に追加
        }



        } catch(ClassNotFoundException e){
        	System.out.println("JDBCドライバーのロードに失敗しました");
        	e.printStackTrace();
        } catch(SQLException e){
        	System.out.println(e.getMessage());
        	e.printStackTrace();
        }finally{
        	if(startment !=null){
        		try{
        			startment.close();
        		} catch(SQLException e){
        		}
        	}
        	if(con != null){
        		try{
        			con.close();
        		} catch(SQLException e){
        		}
        	}
        }
		return data;
    }


	public static keiyakuDTO getChoiceKeiyaku(String kigyoumei){

		int keiyakuno;
		String keiyakuname;
		String keiyakujusho;
		String motoarea;

	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	     //ArrayList<keiyakuDTO> data = new ArrayList<keiyakuDTO>(); //recordを入れる配列

	     keiyakuDTO record = new keiyakuDTO();

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文
	    String sql = "SELECT * FROM `t_keiyaku` WHERE `keiyakuname`=" + "'" + kigyoumei + "'";
	    System.out.println(sql);
	    // 定義したSQLを実行して、実行結果を変数resultに代入
	    ResultSet result = startment.executeQuery(sql);

	    // 変数resultの内容を順次出力
	    int x;
	    for (x=0; result.next(); x++) {
	    	record.setKeiyakuno(result.getInt("keiyakuno"));
	    	record.setKeiyakuname(result.getString("keiyakuname"));
	    	record.setKeiyakujusho(result.getString("keiyakujusho"));
	    	record.setMotoarea(result.getString("motoarea"));
	    //	data.add(record); //配列に追加
	    }



	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return record;
	}

	public static int keiyakutenhenkou(int keiyakubangou, String keiyakuname, String keiyakujusho, String keiyakumotoarea){

//		String keiyakuno;
//		String keiyakuname;
//		String keiyakujusho;
//		String motoarea;


	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	     //ArrayList<keiyakuDTO> data = new ArrayList<keiyakuDTO>(); //recordを入れる配列

	     keiyakuDTO record = new keiyakuDTO();
	     int count = 0;

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文
//	    String sql = "SELECT * FROM `t_keiyaku` WHERE `keiyakuname`=" + "'" + kigyoumei + "'";

	    String sql = "UPDATE t_keiyaku SET"
	    		+ " keiyakuname=" + "'" + keiyakuname
	    		+ "',keiyakujusho =" +  "'" + keiyakujusho
	    		+ "'," + "motoarea ="  + "'" + keiyakumotoarea
	    		+ "'" +
	    " WHERE keiyakuno = '" + keiyakubangou +"'";

	    System.out.println(sql);

	    count = startment.executeUpdate(sql);

	    if(count != 1){
        	System.err.println("追加失敗");
        }else{
        	System.out.println("追加成功");
        }
	    // 定義したSQLを実行して、実行結果を変数resultに代入
//		ResultSet result = startment.executeUpdate(sql);
//
//	    if(startment.executeUpdate(sql) != 1){
//        	System.err.println("追加失敗");
//        }else{
//        	System.out.println("追加成功");
//        }

	    // 変数resultの内容を順次出力
//	    int x;
//	    for (x=0; result.next(); x++) {
//	    	record.setKeiyakuno(result.getString("keiyakuno"));
//	    	record.setKeiyakuname(result.getString("keiyakuname"));
//	    	record.setKeiyakujusho(result.getString("keiyakujusho"));
//	    	record.setMotoarea(result.getString("motoarea"));
//	    //	data.add(record); //配列に追加
//	    }



	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return count;
	}

	public static int keiyakutendelete(int keiyakubangou, String keiyakuname, String keiyakujusho, String keiyakumotoarea){

//		String keiyakuno;
//		String keiyakuname;
//		String keiyakujusho;
//		String motoarea;


	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	     //ArrayList<keiyakuDTO> data = new ArrayList<keiyakuDTO>(); //recordを入れる配列

	     keiyakuDTO record = new keiyakuDTO();
	     int count = 0;

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文




	    String sql = "DELETE FROM t_keiyaku WHERE `keiyakuname`="+"'"+ keiyakuname +"'" + "AND `keiyakujusho`="+"'"+ keiyakujusho +"'" + "AND `motoarea`="+"'"+ keiyakumotoarea +"'";


	    System.out.println(sql);
	    count = startment.executeUpdate(sql);
	    // 定義したSQLを実行して、実行結果を変数resultに代入
//		ResultSet result = startment.executeUpdate(sql);

//	    if(startment.executeUpdate(sql) != 1){
//        	System.err.println("削除失敗");
//        }else{
//        	System.out.println("削除成功");
//        }

	    if(count != 1){
        	System.err.println("削除失敗");
        }else{
        	System.out.println("削除成功");
        }

	    // 変数resultの内容を順次出力
//	    int x;
//	    for (x=0; result.next(); x++) {
//	    	record.setKeiyakuno(result.getString("keiyakuno"));
//	    	record.setKeiyakuname(result.getString("keiyakuname"));
//	    	record.setKeiyakujusho(result.getString("keiyakujusho"));
//	    	record.setMotoarea(result.getString("motoarea"));
//	    //	data.add(record); //配列に追加
//	    }



	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return count;
	}

	public static keiyakuDTO newkeiyakuteadd(String keiyakuname, String keiyakujusho, String keiyakumotoarea){

//		String keiyakuno;
//		String keiyakuname;
//		String keiyakujusho;
//		String motoarea;


	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	     //ArrayList<keiyakuDTO> data = new ArrayList<keiyakuDTO>(); //recordを入れる配列

	     keiyakuDTO record = new keiyakuDTO();

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文




	    String sql = "INSERT INTO `t_keiyaku`(`keiyakuname`, `keiyakujusho`, `motoarea`) VALUES"
        		+ " (" + "'" + keiyakuname + "'" + "," + "'" + keiyakujusho + "'" + "," + "'" + keiyakumotoarea + "')";

	    System.out.println(sql);
	    // 定義したSQLを実行して、実行結果を変数resultに代入
//		ResultSet result = startment.executeUpdate(sql);

	    if(startment.executeUpdate(sql) != 1){
        	System.err.println("追加失敗");
        }else{
        	System.out.println("追加成功");
        }

	    // 変数resultの内容を順次出力
//	    int x;
//	    for (x=0; result.next(); x++) {
//	    	record.setKeiyakuno(result.getString("keiyakuno"));
//	    	record.setKeiyakuname(result.getString("keiyakuname"));
//	    	record.setKeiyakujusho(result.getString("keiyakujusho"));
//	    	record.setMotoarea(result.getString("motoarea"));
//	    //	data.add(record); //配列に追加
//	    }



	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return record;
	}

	public static int searchKeiyaku(String kigyoumei){

		int keiyakuno;
		String keiyakuname;
		String keiyakujusho;
		String motoarea;

		int x=0;

	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	     //ArrayList<keiyakuDTO> data = new ArrayList<keiyakuDTO>(); //recordを入れる配列

	     keiyakuDTO record = new keiyakuDTO();

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文
	    String sql = "SELECT * FROM `t_keiyaku` WHERE `keiyakuname`=" + "'" + kigyoumei + "'";
	    System.out.println(sql);
	    // 定義したSQLを実行して、実行結果を変数resultに代入
	    ResultSet result = startment.executeQuery(sql);

	    // 変数resultの内容を順次出力
	    for (x=0; result.next(); x++) {
	    	record.setKeiyakuno(result.getInt("keiyakuno"));
	    	record.setKeiyakuname(result.getString("keiyakuname"));
	    	record.setKeiyakujusho(result.getString("keiyakujusho"));
	    	record.setMotoarea(result.getString("motoarea"));
	    //	data.add(record); //配列に追加
	    }



	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return x;
	}

}