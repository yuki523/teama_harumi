package gyomu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dto.Tyumon_itiranDTO;
import dto.jutyushohin;

public class Tyumon_itiranDAO {
	//一覧
	public static ArrayList<Tyumon_itiranDTO> tyumonitiran1() {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String pw = "";
		Connection con = null;
		Statement startment = null;

		ArrayList<Tyumon_itiranDTO> data = new ArrayList<Tyumon_itiranDTO>(); // recordを入れる配列

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, pw);
			startment = con.createStatement();
			System.out.println("接続成功");
			// SQL文
			String sql = "SELECT haruminame, t_juchu.tantouid, t_juchu.juchuno, t_kyosan.kyosanname,"
					+ " t_juchu.kyosanno,busho, kaiinname, kaiinkana, t_juchu.kaiinno, kaiintelno, kaiintelkubun,sendkubun,sendjusho,sendname,sendtelno, rusu,"
					+ " sumgaku, cost, sumtax,riritu, paykaisu, payway, paybank, paydate, hachuumu, seikyuumu,receptime, nyukinumu"
					+ " FROM t_juchu ,t_send, t_pay, t_member, t_kyosan, t_harumi "
					+ "WHERE 	t_juchu.juchuno=t_send.juchuno and	t_juchu.juchuno=t_pay.juchuno and t_juchu.kyosanno=t_kyosan.kyosanno and "
					+ "t_juchu.kaiinno=t_member.kaiinno and t_pay.nowpaykaisu = 1"
					+ " and t_juchu.tantouid=t_harumi.harumiid and cancel=0 "
					+"ORDER BY t_juchu.juchuno DESC"; // ////////////ここ変える
			System.out.println(sql);
			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);
			// 変数resultの内容を順次出力
			int x;
			for (x = 0; result.next(); x++) {
				Tyumon_itiranDTO record = new Tyumon_itiranDTO();

				record.setTantoid(result.getString("tantouid"));
				record.setTyumonday(result.getString("receptime"));
				record.setJuchutantou(result.getString("haruminame"));
				record.setJuchuno(result.getInt("juchuno"));
				record.setKyosanno(result.getInt("kyosanno"));
				record.setKyousankigyoumei(result.getString("kyosanname"));
				record.setSyozokubusyo(result.getString("busho"));
				record.setChumonsyaname(result.getString("kaiinname"));
				record.setChumonsyakana(result.getString("kaiinkana"));
				record.setSyainno(result.getString("kaiinno"));
				record.setChumonsyatel(result.getString("kaiintelno"));
				record.setTelkubun(result.getString("kaiintelkubun"));
				record.setTodokesakikubun(result.getString("sendkubun"));
				record.setTodokesakijusho(result.getString("sendjusho"));
				record.setTodokesakiname(result.getString("sendname"));
				record.setTodokesakitel(result.getString("sendtelno"));
				record.setRusu(result.getString("rusu"));
				record.setTotalmoney(result.getInt("sumgaku"));
				record.setTotalothermoney(result.getInt("cost"));
				record.setAlltotalmoney(result.getInt("sumtax"));
				record.setPaycount(result.getInt("paykaisu"));
				record.setHowtopay(result.getString("payway"));
				record.setHurikomimotoginko(result.getString("paybank"));
				record.setHattyuumu(result.getBoolean("hachuumu"));
				record.setSeikyuumu(result.getBoolean("seikyuumu"));
				record.setNyukinumu(result.getBoolean("nyukinumu"));
				record.setRiritu(result.getDouble("riritu"));
				record.setSiharaibi(result.getString("paydate"));

				record.setShohins(tyumonitiran2(record.getJuchuno()));// 受注商品一覧

				data.add(record); // 配列に追加

			}
			System.out.println(data);

		} catch (ClassNotFoundException e) {
			System.out.println("JDBCドライバーのロードに失敗しました");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}

	//商品
	public static ArrayList<jutyushohin> tyumonitiran2(int juchuno) {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String pw = "";
		Connection con = null;
		Statement startment = null;

		ArrayList<jutyushohin> data = new ArrayList<jutyushohin>(); // recordを入れる配列

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, pw);
			startment = con.createStatement();
			System.out.println("接続成功");
			// SQL文
			String sql = "SELECT * FROM t_juchushohin WHERE juchuno = " + "'"
					+ juchuno + "' "; // ////////////ここ変える
			System.out.println(sql);
			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);
			// 変数resultの内容を順次出力
			int x;
			for (x = 0; result.next(); x++) {
				jutyushohin record = new jutyushohin();
				record.setGou(result.getString("gou"));
				record.setPage(result.getString("page"));
				record.setShohinno(result.getString("shohinno"));
				record.setShohinname(result.getString("shohinname"));
				record.setModel(result.getString("model"));
				record.setIro(result.getString("iro"));
				record.setGara(result.getString("gara"));
				record.setSize(result.getString("shohinsize"));
				record.setTanka(result.getInt("tanka"));
				record.setTani(result.getString("unit"));
				record.setSuryo(result.getInt("suryo"));
				record.setNotaxtotal(result.getInt("nontaxsum"));
				// record.setOthermoney(result.getInt("othermoney"));
				record.setTotal(result.getInt("taxsum"));
				record.setExplain(result.getString("hosoku"));
				record.setHasoubi(result.getString("hassodate"));

				data.add(record); // 配列に追加
			}

		} catch (ClassNotFoundException e) {
			System.out.println("JDBCドライバーのロードに失敗しました");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}

	//更新
	public static ArrayList<Tyumon_itiranDTO> kousin(String hachu, String seikyu,
			String nyukin, String juchuno) {
		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String pw = "";
		Connection con = null;
		Statement startment = null;

		ArrayList<Tyumon_itiranDTO> data = new ArrayList<Tyumon_itiranDTO>(); // recordを入れる配列

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, pw);
			startment = con.createStatement();
			System.out.println("接続成功");
			System.out.println("こうしんなう接続");
			// SQL文
			String sql = "UPDATE t_juchu SET `hachuumu`=" +  hachu
					+ ", `seikyuumu`=" + seikyu + ", `nyukinumu`="
					+  nyukin + " WHERE juchuno ='" + juchuno + "' "; //////////////ここ変える
			System.out.println(sql);

			System.out.println("");
			if (startment.executeUpdate(sql) != 1) {
				System.err.println("更新失敗");
			} else {
				System.out.println("更新成功");
			}

		} catch (ClassNotFoundException e) {
			System.out.println("JDBCドライバーのロードに失敗しました");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}

	//削除
	public static ArrayList<Tyumon_itiranDTO> delete(String juchuno, String hachu) {
		 // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	    ArrayList<Tyumon_itiranDTO> data = new ArrayList<Tyumon_itiranDTO>(); // recordを入れる配列

	    try {
		    // MySQLとの接続を開始
		    Class.forName("org.gjt.mm.mysql.Driver");
		    con = DriverManager.getConnection(url, user, pw);
		    startment = con.createStatement();
		    System.out.println("接続成功");
		    //SQL文


		    String sql ="UPDATE t_juchu SET cancel = 1"
					+ " WHERE  juchuno =" + juchuno + " "; //////////////ここ変える
			System.out.println(sql);



		    // 定義したSQLを実行して、実行結果を変数resultに代入
//			ResultSet result = startment.executeUpdate(sql);

		    if(startment.executeUpdate(sql) != 1){
	        	System.err.println("削除失敗");
	        }else{
	        	System.out.println("削除成功");
	        }


		    } catch(ClassNotFoundException e){
		    	System.out.println("JDBCドライバーのロードに失敗しました");
		    	e.printStackTrace();
		    } catch(SQLException e){
		    	System.out.println(e.getMessage());
		    	e.printStackTrace();
		    }finally{
		    	if(startment !=null){
		    		try{
		    			startment.close();
		    		} catch(SQLException e){
		    		}
		    	}
		    	if(con != null){
		    		try{
		    			con.close();
		    		} catch(SQLException e){
		    		}
		    	}
		    }
			return data;


	}
	//検索
	public static ArrayList<Tyumon_itiranDTO> getKensaku(String kaiinname, String tyumonday, String juchuno) {


		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Connection con = null;
		Statement startment = null;

		ArrayList<Tyumon_itiranDTO> data = new ArrayList<Tyumon_itiranDTO>();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();

			// 実行するSQLを定義
			String sql ="SELECT haruminame, t_juchu.tantouid, t_juchu.juchuno, t_kyosan.kyosanname,"
					+ " t_juchu.kyosanno,busho, kaiinname, kaiinkana, t_juchu.kaiinno, kaiintelno,"
					+ " kaiintelkubun,sendkubun,sendjusho,sendname,sendtelno, rusu, sumgaku, cost,"
					+ " sumtax,riritu, paykaisu, payway, paybank, paydate, hachuumu, seikyuumu,receptime, nyukinumu"
					+ " FROM t_juchu ,t_send, t_pay, t_member, t_kyosan, t_harumi"
					+ " WHERE t_juchu.juchuno=t_send.juchuno and"
					+ " t_juchu.juchuno=t_pay.juchuno and t_juchu.kyosanno=t_kyosan.kyosanno and"
					+ " t_juchu.kaiinno=t_member.kaiinno and t_pay.nowpaykaisu = 1 and t_juchu.tantouid=t_harumi.harumiid and"
					+ " kaiinname ='"+kaiinname+"' and receptime ='"+tyumonday+"' and t_juchu.juchuno ='"+juchuno+"' ";////////////////////////////変える
			System.out.println(sql);
			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 変数resultの内容を順次出力
			int x;
			for (x = 0; result.next(); x++) {
				Tyumon_itiranDTO record = new Tyumon_itiranDTO();

				record.setTantoid(result.getString("tantouid"));
				record.setTyumonday(result.getString("receptime"));
				record.setJuchutantou(result.getString("haruminame"));
				record.setJuchuno(result.getInt("juchuno"));
				record.setKyosanno(result.getInt("kyosanno"));
				record.setKyousankigyoumei(result.getString("kyosanname"));
				record.setSyozokubusyo(result.getString("busho"));
				record.setChumonsyaname(result.getString("kaiinname"));
				record.setChumonsyakana(result.getString("kaiinkana"));
				record.setSyainno(result.getString("kaiinno"));
				record.setChumonsyatel(result.getString("kaiintelno"));
				record.setTelkubun(result.getString("kaiintelkubun"));
				record.setTodokesakikubun(result.getString("sendkubun"));
				record.setTodokesakijusho(result.getString("sendjusho"));
				record.setTodokesakiname(result.getString("sendname"));
				record.setTodokesakitel(result.getString("sendtelno"));
				record.setRusu(result.getString("rusu"));
				record.setTotalmoney(result.getInt("sumgaku"));
				record.setTotalothermoney(result.getInt("cost"));
				record.setAlltotalmoney(result.getInt("sumtax"));
				record.setPaycount(result.getInt("paykaisu"));
				record.setHowtopay(result.getString("payway"));
				record.setHurikomimotoginko(result.getString("paybank"));
				record.setHattyuumu(result.getBoolean("hachuumu"));
				record.setSeikyuumu(result.getBoolean("seikyuumu"));
				record.setNyukinumu(result.getBoolean("nyukinumu"));
				record.setRiritu(result.getDouble("riritu"));
				record.setSiharaibi(result.getString("paydate"));

				record.setShohins(tyumonitiran2(record.getJuchuno()));// 受注商品一覧

				data.add(record); // 配列に追加

			}


		} catch (ClassNotFoundException e) {
			System.err.println("JDBCのドライバのロードに失敗しました。");
			e.printStackTrace();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		// エラー情報を取得
		catch (Exception e) {
			System.out.println("例外発生:" + e);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}
	//未発注
	public static ArrayList<Tyumon_itiranDTO> mihachu() {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String pw = "";
		Connection con = null;
		Statement startment = null;

		ArrayList<Tyumon_itiranDTO> data = new ArrayList<Tyumon_itiranDTO>(); // recordを入れる配列

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, pw);
			startment = con.createStatement();
			System.out.println("接続成功");
			// SQL文
			String sql = "SELECT haruminame, t_juchu.tantouid, t_juchu.juchuno,t_kyosan.kyosanname, "
					+ "t_juchu.kyosanno,busho, kaiinname,kaiinkana, t_juchu.kaiinno, kaiintelno,"
					+ " kaiintelkubun,sendkubun,sendjusho,sendname,sendtelno, rusu,sumgaku, cost,"
					+ " sumtax,riritu, paykaisu, payway, paybank, paydate, hachuumu, seikyuumu,receptime, nyukinumu"
					+ " FROM t_juchu ,t_send, t_pay, t_member, t_kyosan, t_harumi "
					+ "WHERE t_juchu.juchuno=t_send.juchuno and t_juchu.juchuno=t_pay.juchuno and t_juchu.kyosanno=t_kyosan.kyosanno"
					+ " and t_juchu.kaiinno=t_member.kaiinno and t_pay.nowpaykaisu = 1 and hachuumu = false and t_juchu.tantouid=t_harumi.harumiid  "; // ////////////ここ変える
			System.out.println(sql);
			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);
			// 変数resultの内容を順次出力
			int x;
			for (x = 0; result.next(); x++) {
				Tyumon_itiranDTO record = new Tyumon_itiranDTO();

				record.setTantoid(result.getString("tantouid"));
				record.setTyumonday(result.getString("receptime"));
				record.setJuchutantou(result.getString("haruminame"));
				record.setJuchuno(result.getInt("juchuno"));
				record.setKyosanno(result.getInt("kyosanno"));
				record.setKyousankigyoumei(result.getString("kyosanname"));
				record.setSyozokubusyo(result.getString("busho"));
				record.setChumonsyaname(result.getString("kaiinname"));
				record.setChumonsyakana(result.getString("kaiinkana"));
				record.setSyainno(result.getString("kaiinno"));
				record.setChumonsyatel(result.getString("kaiintelno"));
				record.setTelkubun(result.getString("kaiintelkubun"));
				record.setTodokesakikubun(result.getString("sendkubun"));
				record.setTodokesakijusho(result.getString("sendjusho"));
				record.setTodokesakiname(result.getString("sendname"));
				record.setTodokesakitel(result.getString("sendtelno"));
				record.setRusu(result.getString("rusu"));
				record.setTotalmoney(result.getInt("sumgaku"));
				record.setTotalothermoney(result.getInt("cost"));
				record.setAlltotalmoney(result.getInt("sumtax"));
				record.setPaycount(result.getInt("paykaisu"));
				record.setHowtopay(result.getString("payway"));
				record.setHurikomimotoginko(result.getString("paybank"));
				record.setHattyuumu(result.getBoolean("hachuumu"));
				record.setSeikyuumu(result.getBoolean("seikyuumu"));
				record.setNyukinumu(result.getBoolean("nyukinumu"));
				record.setRiritu(result.getDouble("riritu"));
				record.setSiharaibi(result.getString("paydate"));

				record.setShohins(tyumonitiran2(record.getJuchuno()));// 受注商品一覧

				data.add(record); // 配列に追加

			}
			System.out.println(data);

		} catch (ClassNotFoundException e) {
			System.out.println("JDBCドライバーのロードに失敗しました");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}
	//未請求
	public static ArrayList<Tyumon_itiranDTO> Miseikyu() {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String pw = "";
		Connection con = null;
		Statement startment = null;

		ArrayList<Tyumon_itiranDTO> data = new ArrayList<Tyumon_itiranDTO>(); // recordを入れる配列

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, pw);
			startment = con.createStatement();
			System.out.println("接続成功");
			// SQL文
			String sql = "SELECT haruminame, t_juchu.tantouid, t_juchu.juchuno, t_kyosan.kyosanname,"
					+ " t_juchu.kyosanno,busho, kaiinname, kaiinkana, t_juchu.kaiinno, kaiintelno, kaiintelkubun,sendkubun,sendjusho,sendname,sendtelno, rusu,"
					+ " sumgaku, cost, sumtax,riritu, paykaisu, payway, paybank, paydate, hachuumu, seikyuumu,receptime, nyukinumu"
					+ " FROM t_juchu ,t_send, t_pay, t_member, t_kyosan, t_harumi "
					+ "WHERE 	t_juchu.juchuno=t_send.juchuno and	t_juchu.juchuno=t_pay.juchuno and t_juchu.kyosanno=t_kyosan.kyosanno and "
					+ "t_juchu.kaiinno=t_member.kaiinno and t_pay.nowpaykaisu = 1 and seikyuumu = false"
					+ " and t_juchu.tantouid=t_harumi.harumiid  "; // ////////////ここ変える
			System.out.println(sql);
			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);
			// 変数resultの内容を順次出力
			int x;
			for (x = 0; result.next(); x++) {
				Tyumon_itiranDTO record = new Tyumon_itiranDTO();

				record.setTantoid(result.getString("tantouid"));
				record.setTyumonday(result.getString("receptime"));
				record.setJuchutantou(result.getString("haruminame"));
				record.setJuchuno(result.getInt("juchuno"));
				record.setKyosanno(result.getInt("kyosanno"));
				record.setKyousankigyoumei(result.getString("kyosanname"));
				record.setSyozokubusyo(result.getString("busho"));
				record.setChumonsyaname(result.getString("kaiinname"));
				record.setChumonsyakana(result.getString("kaiinkana"));
				record.setSyainno(result.getString("kaiinno"));
				record.setChumonsyatel(result.getString("kaiintelno"));
				record.setTelkubun(result.getString("kaiintelkubun"));
				record.setTodokesakikubun(result.getString("sendkubun"));
				record.setTodokesakijusho(result.getString("sendjusho"));
				record.setTodokesakiname(result.getString("sendname"));
				record.setTodokesakitel(result.getString("sendtelno"));
				record.setRusu(result.getString("rusu"));
				record.setTotalmoney(result.getInt("sumgaku"));
				record.setTotalothermoney(result.getInt("cost"));
				record.setAlltotalmoney(result.getInt("sumtax"));
				record.setPaycount(result.getInt("paykaisu"));
				record.setHowtopay(result.getString("payway"));
				record.setHurikomimotoginko(result.getString("paybank"));
				record.setHattyuumu(result.getBoolean("hachuumu"));
				record.setSeikyuumu(result.getBoolean("seikyuumu"));
				record.setNyukinumu(result.getBoolean("nyukinumu"));
				record.setRiritu(result.getDouble("riritu"));
				record.setSiharaibi(result.getString("paydate"));

				record.setShohins(tyumonitiran2(record.getJuchuno()));// 受注商品一覧

				data.add(record); // 配列に追加

			}
			System.out.println(data);

		} catch (ClassNotFoundException e) {
			System.out.println("JDBCドライバーのロードに失敗しました");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}
	//未入金
	public static ArrayList<Tyumon_itiranDTO> Minyukin() {

		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String pw = "";
		Connection con = null;
		Statement startment = null;

		ArrayList<Tyumon_itiranDTO> data = new ArrayList<Tyumon_itiranDTO>(); // recordを入れる配列

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, pw);
			startment = con.createStatement();
			System.out.println("接続成功");
			// SQL文
			String sql = "SELECT haruminame, t_juchu.tantouid, t_juchu.juchuno, t_kyosan.kyosanname,"
					+ " t_juchu.kyosanno,busho, kaiinname, kaiinkana, t_juchu.kaiinno, kaiintelno, kaiintelkubun,sendkubun,sendjusho,sendname,sendtelno, rusu,"
					+ " sumgaku, cost, sumtax,riritu, paykaisu, payway, paybank, paydate, hachuumu, seikyuumu,receptime, nyukinumu"
					+ " FROM t_juchu ,t_send, t_pay, t_member, t_kyosan, t_harumi "
					+ "WHERE 	t_juchu.juchuno=t_send.juchuno and	t_juchu.juchuno=t_pay.juchuno and t_juchu.kyosanno=t_kyosan.kyosanno and "
					+ "t_juchu.kaiinno=t_member.kaiinno and t_pay.nowpaykaisu = 1 and nyukinumu = false"
					+ " and t_juchu.tantouid=t_harumi.harumiid  "; // ////////////ここ変える
			System.out.println(sql);
			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);
			// 変数resultの内容を順次出力
			int x;
			for (x = 0; result.next(); x++) {
				Tyumon_itiranDTO record = new Tyumon_itiranDTO();

				record.setTantoid(result.getString("tantouid"));
				record.setTyumonday(result.getString("receptime"));
				record.setJuchutantou(result.getString("haruminame"));
				record.setJuchuno(result.getInt("juchuno"));
				record.setKyosanno(result.getInt("kyosanno"));
				record.setKyousankigyoumei(result.getString("kyosanname"));
				record.setSyozokubusyo(result.getString("busho"));
				record.setChumonsyaname(result.getString("kaiinname"));
				record.setChumonsyakana(result.getString("kaiinkana"));
				record.setSyainno(result.getString("kaiinno"));
				record.setChumonsyatel(result.getString("kaiintelno"));
				record.setTelkubun(result.getString("kaiintelkubun"));
				record.setTodokesakikubun(result.getString("sendkubun"));
				record.setTodokesakijusho(result.getString("sendjusho"));
				record.setTodokesakiname(result.getString("sendname"));
				record.setTodokesakitel(result.getString("sendtelno"));
				record.setRusu(result.getString("rusu"));
				record.setTotalmoney(result.getInt("sumgaku"));
				record.setTotalothermoney(result.getInt("cost"));
				record.setAlltotalmoney(result.getInt("sumtax"));
				record.setPaycount(result.getInt("paykaisu"));
				record.setHowtopay(result.getString("payway"));
				record.setHurikomimotoginko(result.getString("paybank"));
				record.setHattyuumu(result.getBoolean("hachuumu"));
				record.setSeikyuumu(result.getBoolean("seikyuumu"));
				record.setNyukinumu(result.getBoolean("nyukinumu"));
				record.setRiritu(result.getDouble("riritu"));
				record.setSiharaibi(result.getString("paydate"));

				record.setShohins(tyumonitiran2(record.getJuchuno()));// 受注商品一覧

				data.add(record); // 配列に追加

			}
			System.out.println(data);

		} catch (ClassNotFoundException e) {
			System.out.println("JDBCドライバーのロードに失敗しました");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}
}
