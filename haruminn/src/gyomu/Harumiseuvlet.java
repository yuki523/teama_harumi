package gyomu;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.harumiDTO;

/**
 * Servlet implementation class Harumiseuvlet
 */
@WebServlet("/Harumiseuvlet")
public class Harumiseuvlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Harumiseuvlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		response.setContentType("text/html; charset=UTF8"); //文字コードと返すフォーマットを決める


		String jspaction = request.getParameter("submittype");

		//トップページからきたとき
		if(jspaction==null){

			//セレクトタブ用の契約企業名一覧取得
			ArrayList<harumiDTO> data = HarumiDAO.Harumi();
			session.setAttribute("harumitantoulist", data);

			request.setAttribute("message", "担当者名を選択してください");

			RequestDispatcher rd=request.getRequestDispatcher("Harumi.jsp");
			rd.forward(request,response);


		//検索する
		}else if(jspaction.equals("検索")){
			//営業担当者名を取得
			String HarumitantouName = request.getParameter("harumitantoushamei");
			session.setAttribute("choiceharumitantou", HarumitantouName);

			//選択された営業担当者の情報を取得
			harumiDTO harumiinfo = HarumiDAO.getChoiceHarumi(HarumitantouName);
			session.setAttribute("harumiinfo", harumiinfo);
			request.setAttribute("harumiinfo", harumiinfo);

			//セレクトタブ用の営業担当者一覧取得
			ArrayList<harumiDTO> data = HarumiDAO.Harumi();
			session.setAttribute("harumitantoulist", data);

			request.setAttribute("message", "営業担当者情報を変更、あるいは消去してください");

			RequestDispatcher rd=request.getRequestDispatcher("Harumi.jsp");
			rd.forward(request,response);

		//変更する
		}else if(jspaction.equals("変更")){
			harumiDTO harumiinfo = (harumiDTO)session.getAttribute("harumiinfo");

			//入力された変更を取得する

			String harumiid = request.getParameter("harumiid");
			String haruminame = request.getParameter("haruminame");

			String harumiid2 = request.getParameter("harumiid2");
			String haruminame2 = request.getParameter("haruminame2");

			int count = HarumiDAO.harumitantouhenkou(harumiid, haruminame,harumiid2, haruminame2);

			request.setAttribute("count", count);

			if(count==0){
				request.setAttribute("message", "変更に失敗しました");
			}else{
				request.setAttribute("message", "変更しました");
			}

//			harumiDTO haruminfo = HarumiDAO.getChoiceHarumi(haruminame);
//
//			session.setAttribute("harumiinfo", harumiinfo);


			//セレクトタブ用の営業担当者一覧取得
			ArrayList<harumiDTO> data = HarumiDAO.Harumi();
			session.setAttribute("harumitantoulist", data);

			RequestDispatcher rd=request.getRequestDispatcher("Harumi.jsp");
			rd.forward(request,response);

		//削除する
		}else if(jspaction.equals("削除")){
			harumiDTO harumiinfo = (harumiDTO)session.getAttribute("harumiinfo");

			//入力された変更を取得する
			String harumiid = request.getParameter("harumiid");
			String haruminame = request.getParameter("haruminame");

			int count = HarumiDAO.harumitantoudelete(harumiid, haruminame);

			request.setAttribute("count", count);

			if(count==0){
				request.setAttribute("message", "削除に失敗しました");
			}else{
				request.setAttribute("message", "削除しました");
			}


			//セレクトタブ用の営業担当者一覧取得
			ArrayList<harumiDTO> data = HarumiDAO.Harumi();
			session.setAttribute("harumitantoulist", data);

			RequestDispatcher rd=request.getRequestDispatcher("Harumi.jsp");
			rd.forward(request,response);



		//追加する
		}else if(jspaction.equals("追加")){

			//入力された情報を取得する
			String harumiid = request.getParameter("newharumiid");
			String haruminame = request.getParameter("newharuminame");

			HarumiDAO.harumitantouadd(harumiid, haruminame);

			request.setAttribute("message", "追加しました");

			//セレクトタブ用の営業担当者一覧取得
			ArrayList<harumiDTO> data = HarumiDAO.Harumi();
			session.setAttribute("harumitantoulist", data);

			RequestDispatcher rd=request.getRequestDispatcher("Harumi.jsp");
			rd.forward(request,response);

		}


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF8");

		doGet(request, response);
	}

}
