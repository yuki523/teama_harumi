package gyomu;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.Tyumon_itiranDTO;

/**
 * Servlet implementation class Tyumon_itiran
 */
@WebServlet("/Tyumon_itiran")
public class Tyumon_itiran extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Tyumon_itiran() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF8");// 文字コードと返すフォーマットを決める
		HttpSession session = request.getSession();

		String jspaction = request.getParameter("submittype");

		if (jspaction == null) {

			// トップページからきたとき
			ArrayList<Tyumon_itiranDTO> data = Tyumon_itiranDAO.tyumonitiran1();
			session.setAttribute("tyumoninfo", data);

			for (Tyumon_itiranDTO a : data) {
				System.out.println(a.getJuchuno());
				System.out.println(a.getAlltotalmoney());

			}

			RequestDispatcher rd = request.getRequestDispatcher("Tyumon.jsp");
			rd.forward(request, response);

		} else if (jspaction.equals("更新")) {
			// 入力された変更を取得する
			String hachu = request.getParameter("hachu");
			String seikyu = request.getParameter("seikyu");
			String nyukin = request.getParameter("nyukin");

			if(hachu == null){
				hachu = "0";
			}
			if(seikyu==null){
				seikyu = "0";
			}
			if(nyukin==null){
				nyukin = "0";
			}

			// 変更前データ一部取得
			String juchuno = request.getParameter("juchuno");
			System.out.println("ああ"+juchuno);

			Tyumon_itiranDAO.kousin(hachu, seikyu, nyukin, juchuno);

			////情報を更新する
			ArrayList<Tyumon_itiranDTO> data = Tyumon_itiranDAO.tyumonitiran1();
			session.setAttribute("tyumoninfo", data);

			request.setAttribute("message", "変更しました");

			RequestDispatcher rd = request.getRequestDispatcher("Tyumon.jsp");
			rd.forward(request, response);

		} else if (jspaction.equals("削除")) {

			System.out.println("削除");

			// 入力された変更を取得する
			String juchuno = request.getParameter("juchuno");
			String hachu = request.getParameter("hachu");
			//boolean b = Boolean.parseBoolean(hachu);



			//もし未発注ならキャンセル可能　ちがったら無理
			if(null!=hachu && hachu.equals("1")){
//発注されてる
				request.setAttribute("message", "削除できません");
				RequestDispatcher rd = request.getRequestDispatcher("Tyumon.jsp");
				rd.forward(request, response);

			}else{
//未発注
				Tyumon_itiranDAO.delete(juchuno,hachu);

			////情報を更新する
				ArrayList<Tyumon_itiranDTO> data = Tyumon_itiranDAO.tyumonitiran1();
				session.setAttribute("tyumoninfo", data);

			request.setAttribute("message", "削除しました");
			RequestDispatcher rd = request.getRequestDispatcher("Tyumon.jsp");
			rd.forward(request, response);

			}


		}else if(jspaction.equals("検索")){

		String kaiinname = request.getParameter("kaiinname");
		String tyumonday = request.getParameter("tyumonday");
		String juchuno = request.getParameter("juchuno");

		System.out.println("検索します－－－－－－－");
		System.out.println(kaiinname);
		System.out.println(tyumonday);
		System.out.println(juchuno);

		ArrayList<Tyumon_itiranDTO> kensaku = Tyumon_itiranDAO.getKensaku(kaiinname,tyumonday,juchuno);

		session.setAttribute("tyumoninfo", kensaku);

		for (Tyumon_itiranDTO a : kensaku) {
		System.out.println(a.getChumonsyaname());
		}

		request.setAttribute("message", "検索しました");
		RequestDispatcher rd=request.getRequestDispatcher("Tyumon.jsp");
		rd.forward(request,response);

		}else if(jspaction.equals("未発注")){

			ArrayList<Tyumon_itiranDTO> mihachu = Tyumon_itiranDAO.mihachu();
			session.setAttribute("tyumoninfo", mihachu);

			request.setAttribute("message", "未発注");
			RequestDispatcher rd=request.getRequestDispatcher("Tyumon.jsp");
			rd.forward(request,response);

		}else if(jspaction.equals("未請求")){

			ArrayList<Tyumon_itiranDTO> miseikyu = Tyumon_itiranDAO.Miseikyu();
			session.setAttribute("tyumoninfo", miseikyu);

			request.setAttribute("message", "未請求");
			RequestDispatcher rd=request.getRequestDispatcher("Tyumon.jsp");
			rd.forward(request,response);

		}else if(jspaction.equals("未入金")){

			ArrayList<Tyumon_itiranDTO> minyukin = Tyumon_itiranDAO.Minyukin();
			session.setAttribute("tyumoninfo", minyukin);

			request.setAttribute("message", "未入金");
			RequestDispatcher rd=request.getRequestDispatcher("Tyumon.jsp");
			rd.forward(request,response);
		}



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF8");

		doGet(request, response);
	}

}
