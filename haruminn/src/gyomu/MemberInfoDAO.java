package gyomu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dto.kaiinDTO;

public class MemberInfoDAO {

    //会員属性全取得
	public static ArrayList<kaiinDTO> member(){

		String kyousanno;
		String kaiinno;
		String busho;
		String kaiinname;
		String kaiinkana;
		String kaiinntelno;
		String kaiinntelkubun;
		String kaiinnjusho;
		String kaiinnyubin;
		String kaiinemail;
		String pass;
		String joindate;

         // 接続情報を定義
        String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
        String user = "root";
        String pw = "";
        Connection con = null;
        Statement startment = null;

         ArrayList<kaiinDTO> data = new ArrayList<kaiinDTO>(); //recordを入れる配列

         try {
        // MySQLとの接続を開始
        Class.forName("org.gjt.mm.mysql.Driver");
        con = DriverManager.getConnection(url, user, pw);
        startment = con.createStatement();
        System.out.println("接続成功");
        //SQL文
        String sql = "SELECT * FROM `t_member`";
        System.out.println(sql);
        // 定義したSQLを実行して、実行結果を変数resultに代入
        ResultSet result = startment.executeQuery(sql);

        // 変数resultの内容を順次出力
        int x;
        for (x=0; result.next(); x++) {
        	kaiinDTO record = new kaiinDTO();
        	record.setKyousanname(result.getString("kyousanno"));
        	record.setKaiinno(result.getString("kaiinno"));
        	record.setBusho(result.getString("busho"));
        	record.setKaiinname(result.getString("kaiinname"));
        	record.setKaiinkana(result.getString("kaiinkana"));
        	record.setKaiinntelno(result.getString("kaiinntelno"));
        	record.setKaiinntelkubun(result.getString("kaiinntelkubun"));
        	record.setKaiinnjusho(result.getString("kaiinnjusho"));
        	record.setKaiinnyubin(result.getString("kaiinnyubin"));
        	record.setKaiinemail(result.getString("kaiinemail"));
        	record.setPass(result.getString("pass"));
        	record.setJoindate(result.getString("joindate"));
        	data.add(record); //配列に追加
        }



        } catch(ClassNotFoundException e){
        	System.out.println("JDBCドライバーのロードに失敗しました");
        	e.printStackTrace();
        } catch(SQLException e){
        	System.out.println(e.getMessage());
        	e.printStackTrace();
        }finally{
        	if(startment !=null){
        		try{
        			startment.close();
        		} catch(SQLException e){
        		}
        	}
        	if(con != null){
        		try{
        			con.close();
        		} catch(SQLException e){
        		}
        	}
        }
		return data;
    }
}
