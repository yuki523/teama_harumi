package gyomu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import dto.shohinDTO;

public class ShohinitiranDAO {
    //商品一覧全取得
	public static ArrayList<shohinDTO> Shohin(){

		String gou;
		String page;
		String shohinno;
		String shohinname;
		String model;
		String iro;
		String gara;
		String size;
		int tanka;
		String unit;
		String keiyakuno;
		String soryoumu;
		Date release;
		String gazo;
		Date kigen;
		String boxsize;
		String setumei;
		String cateno;
		int sold;

         // 接続情報を定義
        String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
        String user = "root";
        String pw = "";
        Connection con = null;
        Statement startment = null;

         ArrayList<shohinDTO> data = new ArrayList<shohinDTO>(); //recordを入れる配列

         try {
        // MySQLとの接続を開始
        Class.forName("org.gjt.mm.mysql.Driver");
        con = DriverManager.getConnection(url, user, pw);
        startment = con.createStatement();
        System.out.println("接続成功");
        //SQL文
        String sql = "SELECT * FROM `t_shohin`";
        System.out.println(sql);
        // 定義したSQLを実行して、実行結果を変数resultに代入
        ResultSet result = startment.executeQuery(sql);

        // 変数resultの内容を順次出力
        int x;
        for (x=0; result.next(); x++) {
        	shohinDTO record = new shohinDTO();
        	record.setGou(result.getString("gou"));
        	record.setPage(result.getString("page"));
        	record.setShohinno(result.getString("shohinno"));
        	record.setShohinname(result.getString("shohinname"));
        	record.setModel(result.getString("model"));
        	record.setIro(result.getString("iro"));
        	record.setGara(result.getString("gara"));
        	record.setSize(result.getString("size"));
        	record.setTanka(result.getInt("tanka"));
        	record.setUnit(result.getString("unit"));
        	record.setKeiyakuno(result.getString("keiyakuno"));
        	record.setSoryoumu(result.getBoolean("soryoumu"));
        	record.setRelease(result.getString("release"));
        	record.setGazo(result.getString("gazo"));
        	record.setKigen(result.getString("kigen"));
        	record.setBoxsize(result.getString("boxsize"));
        	record.setSetumei(result.getString("setumei"));
        	record.setCateno(result.getString("cateno"));
        	record.setSold(result.getInt("sold"));
        	data.add(record); //配列に追加
        }

        } catch(ClassNotFoundException e){
        	System.out.println("JDBCドライバーのロードに失敗しました");
        	e.printStackTrace();
        } catch(SQLException e){
        	System.out.println(e.getMessage());
        	e.printStackTrace();
        }finally{
        	if(startment !=null){
        		try{
        			startment.close();
        		} catch(SQLException e){
        		}
        	}
        	if(con != null){
        		try{
        			con.close();
        		} catch(SQLException e){
        		}
        	}
        }
		return data;
    }
	//商品検索
	public static ArrayList<shohinDTO> getKensaku(String gou, String page, String shohinno) {

		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Connection con = null;
		Statement startment = null;

		ArrayList<shohinDTO> data = new ArrayList<shohinDTO>();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "SELECT * FROM t_shohin WHERE gou ='"+gou+"' AND page ='"+page+"' AND shohinno ='"+shohinno+"'";

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 変数resultの内容を順次出力
			while (result.next()) {
				shohinDTO record = new shohinDTO();
//				System.out.println(result.getString("shohinname"));
				record.setGazo(result.getString("gazo"));
				record.setGou(result.getString("gou"));
				record.setPage(result.getString("page"));
				record.setShohinno(result.getString("shohinno"));
				record.setShohinname(result.getString("shohinname"));
				record.setModel(result.getString("model"));
				record.setIro(result.getString("iro"));
				record.setGara(result.getString("gara"));
				record.setSize(result.getString("size"));
				record.setTanka(result.getInt("tanka"));
				record.setUnit(result.getString("unit"));
				record.setKeiyakuno(result.getString("keiyakuno"));
				record.setSoryoumu(result.getBoolean("soryoumu"));
				record.setKigen(result.getString("kigen"));
				record.setSetumei(result.getString("setumei"));
				record.setCateno(result.getString("cateno"));
				data.add(record);
			}


		} catch (ClassNotFoundException e) {
			System.err.println("JDBCのドライバのロードに失敗しました。");
			e.printStackTrace();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		// エラー情報を取得
		catch (Exception e) {
			System.out.println("例外発生:" + e);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}

	public static ArrayList<shohinDTO> getKensaku2(String gou, String page, String shohinno) {

		String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Connection con = null;
		Statement startment = null;

		ArrayList<shohinDTO> data = new ArrayList<shohinDTO>();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "SELECT * FROM t_shohin WHERE gou ='"+gou+"' AND page ='"+page+"' AND shohinno ='"+shohinno+"'";

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 変数resultの内容を順次出力
			while (result.next()) {
				shohinDTO record = new shohinDTO();
//				System.out.println(result.getString("shohinname"));
				record.setGazo(result.getString("gazo"));
				record.setGou(result.getString("gou"));
				record.setPage(result.getString("page"));
				record.setShohinno(result.getString("shohinno"));
				record.setShohinname(result.getString("shohinname"));
				record.setModel(result.getString("model"));
				record.setIro(result.getString("iro"));
				record.setGara(result.getString("gara"));
				record.setSize(result.getString("size"));
				record.setTanka(result.getInt("tanka"));
				record.setUnit(result.getString("unit"));
				record.setKeiyakuno(result.getString("keiyakuno"));
				record.setSoryoumu(result.getBoolean("soryoumu"));
				record.setKigen(result.getString("kigen"));
				record.setSetumei(result.getString("setumei"));
				record.setCateno(result.getString("cateno"));
				record.setBoxsize(result.getString("boxsize"));
				record.setRelease(result.getString("release"));
				data.add(record);
			}


		} catch (ClassNotFoundException e) {
			System.err.println("JDBCのドライバのロードに失敗しました。");
			e.printStackTrace();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		// エラー情報を取得
		catch (Exception e) {
			System.out.println("例外発生:" + e);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return data;
	}

	//商品更新
	public static shohinDTO Shohinupdate(String shohinname,String gou, String page, String shohinno,
			 String iro, String gara, String size,  String model,int tanka,String unit,
			 String  keiyakuno, String release, String kigen, String boxsize,
			 String setumei,String cateno,int sold,String gazo,String shohinname2,String gou2, String page2, String shohinno2,
			 String iro2, String gara2, String size2){

	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;


	     shohinDTO record = new shohinDTO();

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文

	    String sql = "UPDATE t_shohin SET `shohinname`="+"'"+ shohinname +"'" +
	    									", `gou`="+"'"+ gou +"'" +
	    									", `page`="+"'"+ page +"'"+
	    									", `shohinno`="+"'"+ shohinno +"'"+
	    									", `iro`="+"'"+ iro +"'"+
	    									", `gara`="+"'"+ gara +"'"+
	    									", `size`="+"'"+ size +"'"+
	    									", `model`="+"'"+ model +"'"+
	    									", `tanka`="+"'"+ tanka +"'"+
	    									", `unit`="+"'"+ unit +"'"+
	    									", `keiyakuno`="+"'"+ keiyakuno +"'"+
	    									", `release`="+"'"+ release +"'"+
	    									", `kigen`="+"'"+ kigen +"'"+
 	    									", `soryoumu`="+"'"+ 0 +"'"+
	    									", `boxsize`="+"'"+ boxsize +"'"+
	    									", `setumei`="+"'"+ setumei +"'"+
	    									", `cateno`="+"'"+ cateno +"'"+
	    									", `sold`="+"'"+ sold +"'"+
	    									", `gazo`="+"'"+ gazo +"'"+
	    									"WHERE gou ='"+gou2+"' "
	    									+"AND page ='"+page2+"' "
	    									+"AND `shohinno` ='"+shohinno2+"'"
	    									+"AND `shohinname`="+"'"+ shohinname2 +"'"
	    									+"AND `iro`="+"'"+ iro2 +"'"
	    									+"AND `gara`="+"'"+ gara2 +"'"
	    									+"AND `size`="+"'"+ size2 +"'";


	    System.out.println(sql);
	    // 定義したSQLを実行して、実行結果を変数resultに代入
//		ResultSet result = startment.executeUpdate(sql);
	    System.out.println("");
	    if(startment.executeUpdate(sql) != 1){
        	System.err.println("更新失敗");
        }else{
        	System.out.println("更新成功");
        }


	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return record;
	}

	//商品削除
	public static shohinDTO Shohindelete(String shohinname,String gou, String page, String shohinno,
			 								String iro, String gara, String size ,String model){



	     // 接続情報を定義
	    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
	    String user = "root";
	    String pw = "";
	    Connection con = null;
	    Statement startment = null;

	    shohinDTO record = new shohinDTO();

	     try {
	    // MySQLとの接続を開始
	    Class.forName("org.gjt.mm.mysql.Driver");
	    con = DriverManager.getConnection(url, user, pw);
	    startment = con.createStatement();
	    System.out.println("接続成功");
	    //SQL文


	    String sql = " DELETE FROM t_shohin WHERE `shohinname`="+"'"+ shohinname +"'" +
	    									"AND `gou`="+"'"+ gou +"'" +
	    									"AND `page`="+"'"+ page +"'"+
	    									"AND `shohinno`="+"'"+ shohinno +"'"+
	    									"AND `iro`="+"'"+ iro +"'"+
	    									"AND `gara`="+"'"+ gara +"'"+
	    									"AND `size`="+"'"+ size +"'"+
	    									"AND `model`="+"'"+ model +"'";




	    // 定義したSQLを実行して、実行結果を変数resultに代入
//		ResultSet result = startment.executeUpdate(sql);

	    if(startment.executeUpdate(sql) != 1){
        	System.err.println("削除失敗");
        }else{
        	System.out.println("削除成功");
        }





	    } catch(ClassNotFoundException e){
	    	System.out.println("JDBCドライバーのロードに失敗しました");
	    	e.printStackTrace();
	    } catch(SQLException e){
	    	System.out.println(e.getMessage());
	    	e.printStackTrace();
	    }finally{
	    	if(startment !=null){
	    		try{
	    			startment.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    	if(con != null){
	    		try{
	    			con.close();
	    		} catch(SQLException e){
	    		}
	    	}
	    }
		return record;
	}

	//商品追加
		public static shohinDTO newShohinadd(String shohinname,String gou, String page, String shohinno,
												 String iro, String gara, String size,  String model,int tanka,String unit,
												 String  keiyakuno, String release, String kigen, String boxsize,
												 String setumei,String cateno,int sold,String gazo){

			// 接続情報を定義
		    String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
		    String user = "root";
		    String pw = "";
		    Connection con = null;
		    Statement startment = null;

		     //ArrayList<keiyakuDTO> data = new ArrayList<keiyakuDTO>(); //recordを入れる配列

		     shohinDTO record = new shohinDTO();

		     try {
		    // MySQLとの接続を開始
		    Class.forName("org.gjt.mm.mysql.Driver");
		    con = DriverManager.getConnection(url, user, pw);
		    startment = con.createStatement();
		    System.out.println("接続成功");
		    //SQL文





		    String sql = "INSERT INTO `t_shohin`(`shohinname`, `gou`, `page`, `shohinno`, "
		    								+ "`iro`, `gara`, `size`, `model`, `tanka`, `unit`,"
		    								+ " `keiyakuno`, `release`, `kigen`, `soryoumu`, `boxsize`, "
		    								+ "`setumei`, `cateno`, `sold`, `gazo`) VALUES"
	        		+ " (" + "'" + shohinname + "'" + "," + "'" + gou + "'" + "," + "'" + page + "'" + "," + "'" + shohinno + "'"+ ","
	        			   + "'" + iro + "'" + "," + "'" + gara + "'" + "," + "'" + size + "'" + "," + "'" + model + "'" + "," + "'" + tanka + "'" + "," + "'" + unit + "'"+ ","
	        			   + "'" + keiyakuno + "'" + "," + "'" + release + "'" + "," + "'" + kigen + "'" + "," + "'" + 0 + "'" + "," + "'" + boxsize + "'" + ","
	        			   + "'" + setumei + "'" + "," + "'" + cateno + "'" + "," + "'" + sold + "'" + "," + "'" + gazo + "'" +")";
		    System.out.println(sql);
		    // 定義したSQLを実行して、実行結果を変数resultに代入
//			ResultSet result = startment.executeUpdate(sql);

		    if(startment.executeUpdate(sql) != 1){
	        	System.err.println("追加失敗");
	        }else{
	        	System.out.println("追加成功");
	        }



		    } catch(ClassNotFoundException e){
		    	System.out.println("JDBCドライバーのロードに失敗しました");
		    	e.printStackTrace();
		    } catch(SQLException e){
		    	System.out.println(e.getMessage());
		    	e.printStackTrace();
		    }finally{
		    	if(startment !=null){
		    		try{
		    			startment.close();
		    		} catch(SQLException e){
		    		}
		    	}
		    	if(con != null){
		    		try{
		    			con.close();
		    		} catch(SQLException e){
		    		}
		    	}
		    }
			return record;
		}

}