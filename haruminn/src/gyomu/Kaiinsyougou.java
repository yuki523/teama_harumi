package gyomu;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.harumiDTO;
import dto.kaiinDTO;
import dto.kyosanDTO;
import dto.passwordDTO;

/**
 * Servlet implementation class Kaiinsyougou
 */
@WebServlet("/Kaiinsyougou")
public class Kaiinsyougou extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Kaiinsyougou() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		response.setContentType("text/html; charset=UTF8"); //文字コードと返すフォーマットを決める
		PrintWriter out = response.getWriter();

		//協賛企業名を取得
		String kyosanname = request.getParameter("kigyoumei");
		//会員番号（社員番号）を取得
		String kaiinID = request.getParameter("kaiinnumber");

		System.out.println(kyosanname);
		System.out.println(kaiinID);


		//会員照合を行う
		ArrayList<passwordDTO> Kaiin = KaiinDAO.referIDPW(kyosanname, kaiinID);
		if(Kaiin.isEmpty()){
			//ログイン認証失敗
			System.out.println("会員IDとパスワードが一致しません");
			response.sendRedirect("Falselogin.html");

		}else{
			//ログイン認証成功
//			session.setAttribute("UserID", kyosanname);
//			session.setAttribute("UserPassword", kaiinID);

			//会員情報を取得
			kaiinDTO Kaiininfo = KaiinDAO.getKaiininfo(kyosanname, kaiinID);
			//セッションに会員情報を保存
			session.setAttribute("kaiininfo", Kaiininfo);

			//ログインした会員の企業情報を取得
			kyosanDTO Kaiinkyousankigyo = KyousankigyoDAO.getChoicekyosan(kyosanname);
			session.setAttribute("kaiinkyousankigyo", Kaiinkyousankigyo);

			//注文日を取得
			Calendar calender =Calendar.getInstance();
			int a = calender.get(Calendar.YEAR);
			String year = String.valueOf(a);
			int b = calender.get(Calendar.MONTH) + 1;
			String month = String.valueOf(b);
			int c = calender.get(Calendar.DATE);
			String date = String.valueOf(c);

			//セッションに注文日を保存
			session.setAttribute("year", year);
			session.setAttribute("month", month);
			session.setAttribute("date", date);

			//はるみ社員の情報を取得
			ArrayList <harumiDTO> harumisyain = HarumiDAO.Harumi();
			session.setAttribute("harumisyain", harumisyain);

			for(harumiDTO record : harumisyain){
				System.out.println( record.getHarumiid());
				System.out.println(record.getHaruminame());
			}

			//取得したデータの表示

//			ArrayList<kaiinDTO> Kaiininfo2 = (ArrayList<kaiinDTO>)session.getAttribute("kaiininfo");
//			for(kaiinDTO record : Kaiininfo2){
//				System.out.println( record.getKaiinname());
//				System.out.println(record.getKaiinnjusho());
//			}
//			ArrayList<kyosanDTO> kyosankigyo = (ArrayList<kyosanDTO>)session.getAttribute("kaiinkyousankigyo");
//
//			for(kyosanDTO record : kyosankigyo){
//				System.out.println( record.getKyosanname());
//				System.out.println(record.getKyosanjusho());
//			}





			response.sendRedirect("tyumonuketuke.jsp");
//			response.sendRedirect("tyumonuketuke.jsp");
//			RequestDispatcher rd = request.getRequestDispatcher("/MemberOnly.jsp");
//			rd.forward(request, response);
		}


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF8");

		doGet(request, response);
	}

}
