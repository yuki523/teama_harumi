package gyomu;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.kyosanDTO;

/**
 * Servlet implementation class Kyosanservlet
 */
@WebServlet("/Kyosanservlet")
public class Kyosanservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Kyosanservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		response.setContentType("text/html; charset=UTF8"); //文字コードと返すフォーマットを決める

		String jspaction = request.getParameter("submittype");

		//トップページからきたとき
		if(jspaction==null){

			//セレクトタブ用の契約企業名一覧取得
			ArrayList<kyosanDTO> data = KyousankigyoDAO.kyosankigyo();
			session.setAttribute("kyosannamelist", data);

			request.setAttribute("message", "企業名を選択してください");

			RequestDispatcher rd=request.getRequestDispatcher("kyosankigyou.jsp");
			rd.forward(request,response);


		//検索する
		}else if(jspaction.equals("検索")){
			//協賛企業名を取得
			String kyosanname = request.getParameter("kyosankigyo");
			session.setAttribute("choicekigyoumei", kyosanname);

			//選択された企業の情報を取得
			kyosanDTO kyosaninfo = KyousankigyoDAO.getChoicekyosan(kyosanname);
			session.setAttribute("kyosaninfo", kyosaninfo);
			request.setAttribute("kyosaninfo", kyosaninfo);

			System.out.println(kyosaninfo.getKyosanname());

			//セレクトタブ用の契約企業名一覧取得
			ArrayList<kyosanDTO> data = KyousankigyoDAO.kyosankigyo();
			session.setAttribute("kyosannamelist", data);

			request.setAttribute("message", "協賛企業情報を変更、あるいは消去してください");

			RequestDispatcher rd=request.getRequestDispatcher("kyosankigyou.jsp");
			rd.forward(request,response);

		//変更する
		}else if(jspaction.equals("変更")){
			kyosanDTO kyosaninfo = (kyosanDTO)session.getAttribute("kyosaninfo");

			//入力された変更を取得する
			int kyosanno = kyosaninfo.getKyosanno();
			String kyosanname = request.getParameter("kyosanname");
			String kyosantelno = request.getParameter("kyosantelno");
			String kyosanyubin = request.getParameter("kyosanyubin");
			String kyuyo = request.getParameter("kyuyo");
			String kyosanjusho = request.getParameter("kyosanjusho");
			String tukijime = request.getParameter("kyosantukijime");

			String tantouname = request.getParameter("tantouname");
			String tantoukana = request.getParameter("tantoukana");
			String tantoubusyo = request.getParameter("tantoubusyo");
			String tantoutel = request.getParameter("tantoutel");

			//変更数を数える
			int count = KyousankigyoDAO.kyosanhenkou(kyosanno, kyosanname, kyosantelno, kyosanyubin, kyuyo, kyosanjusho, tukijime, tantouname, tantoukana, tantoubusyo, tantoutel);


			request.setAttribute("count", count);

			if(count==0){
				request.setAttribute("message", "変更に失敗しました");
			}else{
				request.setAttribute("message", "変更しました");
			}

			//セレクトタブ用の契約企業名一覧取得
			ArrayList<kyosanDTO> data = KyousankigyoDAO.kyosankigyo();
			session.setAttribute("kyosannamelist", data);

			RequestDispatcher rd=request.getRequestDispatcher("kyosankigyou.jsp");
			rd.forward(request,response);

		//削除する
		}else if(jspaction.equals("削除")){
			kyosanDTO kyosaninfo = (kyosanDTO)session.getAttribute("kyosaninfo");

			//入力された変更を取得する
			int kyosanno = kyosaninfo.getKyosanno();
			String kyosanname = request.getParameter("kyosanname");
			String kyosantelno = request.getParameter("kyosantelno");
			String kyosanyubin = request.getParameter("kyosanyubin");
			String kyuyo = request.getParameter("kyuyo");
			String kyosanjusho = request.getParameter("kyosanjusho");
			String tukijime = request.getParameter("kyosantukijime");

			String tantouname = request.getParameter("tantouname");
			String tantoukana = request.getParameter("tantoukana");
			String tantoubusyo = request.getParameter("tantoubusyo");
			String tantoutel = request.getParameter("tantoutel");

			int count = KyousankigyoDAO.kyosandelete(kyosanno, kyosanname, kyosantelno, kyosanyubin, kyuyo, kyosanjusho, tantouname, tantoukana, tantoubusyo, tantoutel, tukijime);

			if(count==0){
				request.setAttribute("message", "削除に失敗しました");
			}else{
				request.setAttribute("message", "削除しました");
			}


			//セレクトタブ用の契約企業名一覧取得
			ArrayList<kyosanDTO> data = KyousankigyoDAO.kyosankigyo();
			session.setAttribute("kyosannamelist", data);

			RequestDispatcher rd=request.getRequestDispatcher("kyosankigyou.jsp");
			rd.forward(request,response);

		//追加する
		}else if(jspaction.equals("追加")){

			//入力された情報を取得する
			String kyosanname = request.getParameter("newkyosanname");
			String kyosantelno = request.getParameter("newkyosantelno");
			String kyosanyubin = request.getParameter("newkyosanyubin");
			String kyuyo = request.getParameter("newkyuyo");
			String tukijime = request.getParameter("newkyosantukijime");
			String kyosanjusho = request.getParameter("newkyosanjusho");

			String tantouname = request.getParameter("newtantouname");
			String tantoukana = request.getParameter("newtantoukana");
			String tantoubusyo = request.getParameter("newtantoubusyo");
			String tantoutel = request.getParameter("newtantoutel");

			int count = KyousankigyoDAO.searchkyosancount(kyosanname);

			if(count==0){
				KyousankigyoDAO. newkyosanteadd(kyosanname, kyuyo, kyosanyubin, kyosantelno, kyosanjusho, tukijime, tantouname, tantoukana, tantoubusyo, tantoutel);
				request.setAttribute("message", "追加しました");
			}else{
				request.setAttribute("message", "同じ企業名が存在します");
			}

			//セレクトタブ用の契約企業名一覧取得
			ArrayList<kyosanDTO> data = KyousankigyoDAO.kyosankigyo();
			session.setAttribute("kyosannamelist", data);

			RequestDispatcher rd=request.getRequestDispatcher("kyosankigyou.jsp");
			rd.forward(request,response);

		}



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF8");

		doGet(request, response);
	}

}
