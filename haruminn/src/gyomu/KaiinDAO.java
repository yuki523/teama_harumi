package gyomu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dto.kaiinDTO;
import dto.passwordDTO;

public class KaiinDAO {


	//会員照合メソッド
	public static ArrayList<passwordDTO> referIDPW(String kyosanname, String kaiinnumber){
	    	String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String password = "";

			Connection con = null;
			Statement startment = null;

			ArrayList data = new ArrayList();

			try {
				// MySQLとの接続を開始
				Class.forName("org.gjt.mm.mysql.Driver");
				con = DriverManager.getConnection(url, user, password);
				startment = con.createStatement();
				System.out.println("接続成功");

				//SQL文
				String sql ="SELECT `kyosanname`,`kaiinno` FROM `t_member` WHERE `kyosanname`=" + "'" + kyosanname + "'" + " AND `kaiinno`=" + "'" + kaiinnumber + "'";

				ResultSet result = startment.executeQuery(sql);

				while (result.next()) {
					passwordDTO record=new passwordDTO();
					record.setKyousanno(result.getString("kyosanname"));
					record.setKaiinno(result.getString("kaiinno"));
					data.add(record);
				}
			}catch (ClassNotFoundException e) {
				System.err.println("JDBCドライバのロードに失敗しました。");
				e.printStackTrace();

			} catch (SQLException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}

			// エラー情報を取得
			catch (Exception e) {
				System.out.println("例外発生:" + e);
			}

			finally {
				if (startment != null) {
					try {
						startment.close();
					} catch (SQLException e) {

					}
				}

				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {

					}
				}
				return data;
			}
	    }


	  public static kaiinDTO getKaiininfo(String kyosanname, String kaiinnumber){
	    	String url = "jdbc:mysql://localhost:3306/harumi?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String password = "";

			Connection con = null;
			Statement startment = null;

			ArrayList data = new ArrayList();

			kaiinDTO record=new kaiinDTO();
			try {
				// MySQLとの接続を開始
				Class.forName("org.gjt.mm.mysql.Driver");
				con = DriverManager.getConnection(url, user, password);
				startment = con.createStatement();
				System.out.println("接続成功");

				//SQL文
				String sql ="SELECT * FROM `t_member` WHERE `kyosanname`=" + "'" + kyosanname + "'" + " AND `kaiinno`=" + "'" + kaiinnumber + "'";

				ResultSet result = startment.executeQuery(sql);

				while (result.next()) {

					record.setKyousanname(result.getString("kyosanname"));
					record.setKaiinno(result.getString("kaiinno"));
					record.setBusho(result.getString("busho"));
					record.setKaiinname(result.getString("kaiinname"));
					record.setKaiinkana(result.getString("kaiinkana"));
					record.setKaiinntelno(result.getString("kaiintelno"));
					record.setKaiinntelkubun(result.getString("kaiintelkubun"));
					record.setKaiinnpref(result.getString("kaiinpref"));
					record.setKaiinnjusho(result.getString("kaiinjusho"));
					record.setKaiinnyubin(result.getString("kaiinyubin"));
					record.setKaiinemail(result.getString("kaiinmail"));
					record.setPass(result.getString("pass"));
					record.setJoindate(result.getString("joindate"));
					record.setYosin(result.getInt("yosin"));
					record.setTotal(result.getInt("total"));

					data.add(record);
				}
			}catch (ClassNotFoundException e) {
				System.err.println("JDBCドライバのロードに失敗しました。");
				e.printStackTrace();

			} catch (SQLException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}

			// エラー情報を取得
			catch (Exception e) {
				System.out.println("例外発生:" + e);
			}

			finally {
				if (startment != null) {
					try {
						startment.close();
					} catch (SQLException e) {

					}
				}

				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {

				}
			}
			return record;
		}
    }

}


