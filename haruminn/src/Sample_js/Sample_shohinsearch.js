/**
 *
 */
$(function() {

	//商品コードの入力テキストボックスからカーソルが外れたとき
	$('.itemcode').blur(function() {

		//alert("focus released");
		var rownum = $(this).attr("rownum");
		//alert($("#item_gou_"+rownum).val());

		if($("#item_gou_"+rownum).val()!="" && $("#item_page_"+rownum).val()!="" && $("#item_no_"+rownum).val()!=""){
			$.ajax({
				  type: 'GET',
				  url: 'http://localhost:8080/Anticyclone/ItemSearchByCode?g='+$("#item_gou_"+rownum).val()+"&p="+$("#item_page_"+rownum).val()+"&n="+$("#item_no_"+rownum).val(),
				  dataType: 'json',
				  success: function(json){
					  $("#item_name_"+rownum).val(json.shohinname);
					  $("#item_tanka_"+rownum).val(json.tanka);
				  }
			});
		}
	});
});