package Sample_js;

public class Sample_Shohin {

	private String shohinname;
	private String gou;
	private String page;
	private String shohinno;
	private int size;
	private String gara;
	private String iro;
	private String model;
	private int tanka;
	public String getShohinname() {
		return shohinname;
	}
	public void setShohinname(String shohinname) {
		this.shohinname = shohinname;
	}
	public String getGou() {
		return gou;
	}
	public void setGou(String gou) {
		this.gou = gou;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getShohinno() {
		return shohinno;
	}
	public void setShohinno(String shohinno) {
		this.shohinno = shohinno;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getGara() {
		return gara;
	}
	public void setGara(String gara) {
		this.gara = gara;
	}
	public String getIro() {
		return iro;
	}
	public void setIro(String iro) {
		this.iro = iro;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getTanka() {
		return tanka;
	}
	public void setTanka(int tanka) {
		this.tanka = tanka;
	}


}
