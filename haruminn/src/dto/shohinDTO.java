package dto;


public class shohinDTO {

	String gou;
	String page;
	String shohinno;
	String shohinname;
	String iro;
	String gara;
	String size;
	String model;
	int tanka;
	String unit;
	String keiyakuno;
	String release;
	String kigen;
	boolean soryoumu;
	String boxsize;
	String setumei;
	String cateno;
	int sold;
	String gazo;

	public String getGou() {
		return gou;
	}

	public void setGou(String gou) {
		this.gou = gou;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getShohinno() {
		return shohinno;
	}

	public void setShohinno(String shohinno) {
		this.shohinno = shohinno;
	}

	public String getShohinname() {
		return shohinname;
	}

	public void setShohinname(String shohinname) {
		this.shohinname = shohinname;
	}

	public String getIro() {
		return iro;
	}

	public void setIro(String iro) {
		this.iro = iro;
	}

	public String getGara() {
		return gara;
	}

	public void setGara(String gara) {
		this.gara = gara;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getTanka() {
		return tanka;
	}

	public void setTanka(int tanka) {
		this.tanka = tanka;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getKeiyakuno() {
		return keiyakuno;
	}

	public void setKeiyakuno(String keiyakuno) {
		this.keiyakuno = keiyakuno;
	}
	public String getRelease() {
		return release;
	}

	public void setRelease(String release) {
		this.release = release;
	}

	public String getKigen() {
		return kigen;
	}

	public void setKigen(String kigen) {
		this.kigen = kigen;
	}

	public  boolean getSoryoumu() {
		return soryoumu;
	}

	public void setSoryoumu(boolean soryoumu) {
		this.soryoumu = soryoumu;
	}

	public String getBoxsize() {
		return boxsize;
	}

	public void setBoxsize(String boxsize) {
		this.boxsize = boxsize;
	}

	public String getSetumei() {
		return setumei;
	}

	public void setSetumei(String setumei) {
		this.setumei = setumei;
	}
	public String getCateno() {
		return cateno;
	}

	public void setCateno(String cateno) {
		this.cateno = cateno;
	}

	public int getSold() {
		return sold;
	}

	public void setSold(int sold) {
		this.sold = sold;
	}

	public String getGazo() {
		return gazo;
	}

	public void setGazo(String gazo) {
		this.gazo = gazo;
	}

	public String getUniqNo() {
		return this.gou+"-"+this.getPage()+"-"+this.getShohinno();
	}
}