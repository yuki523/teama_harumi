package dto;


public class keiyakuDTO implements java.io.Serializable{

	int keiyakuno;
	String keiyakuname;
	String keiyakujusho;
	String motoarea;

	public int getKeiyakuno() {
		return keiyakuno;
	}
	public void setKeiyakuno(int keiyakuno) {
		this.keiyakuno = keiyakuno;
	}
	public String getKeiyakuname() {
		return keiyakuname;
	}
	public void setKeiyakuname(String keiyakuname) {
		this.keiyakuname = keiyakuname;
	}
	public String getKeiyakujusho() {
		return keiyakujusho;
	}
	public void setKeiyakujusho(String keiyakujusho) {
		this.keiyakujusho = keiyakujusho;
	}
	public String getMotoarea() {
		return motoarea;
	}
	public void setMotoarea(String motoarea) {
		this.motoarea = motoarea;
	}


}
