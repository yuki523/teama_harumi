package dto;

public class TyumonuketukeDTO {

	private String syouhingou;
	private String syouhinpage;
	private String syouhinno;
	private String syouhinmei;
	private String syouhinmodel;
	private String syouhincolor;
	private String syouhinpattern;
	private String syouhinsize;
	private int syouhintanka;
	private String syouhintani;
	private int syouhinsuryo;
	private int syouhinmoney;
	private int othermoney;
	private String totalsyouhinmoney;
	private String explain;
	private int hatyusaki;	//契約企業番号
	private String hasoubi;

	public String getSyouhingou() {
		return syouhingou;
	}
	public void setSyouhingou(String syouhingou) {
		this.syouhingou = syouhingou;
	}
	public String getSyouhinpage() {
		return syouhinpage;
	}
	public void setSyouhinpage(String syouhinpage) {
		this.syouhinpage = syouhinpage;
	}
	public String getSyouhinno() {
		return syouhinno;
	}
	public void setSyouhinno(String syouhinno) {
		this.syouhinno = syouhinno;
	}
	public String getSyouhinmei() {
		return syouhinmei;
	}
	public void setSyouhinmei(String syouhinmei) {
		this.syouhinmei = syouhinmei;
	}
	public String getSyouhinmodel() {
		return syouhinmodel;
	}
	public void setSyouhinmodel(String syouhinmodel) {
		this.syouhinmodel = syouhinmodel;
	}
	public String getSyouhincolor() {
		return syouhincolor;
	}
	public void setSyouhincolor(String syouhincolor) {
		this.syouhincolor = syouhincolor;
	}
	public String getSyouhinpattern() {
		return syouhinpattern;
	}
	public void setSyouhinpattern(String syouhinpattern) {
		this.syouhinpattern = syouhinpattern;
	}
	public String getSyouhinsize() {
		return syouhinsize;
	}
	public void setSyouhinsize(String syouhinsize) {
		this.syouhinsize = syouhinsize;
	}
	public int getSyouhintanka() {
		return syouhintanka;
	}
	public void setSyouhintanka(int syouhintanka) {
		this.syouhintanka = syouhintanka;
	}
	public String getSyouhintani() {
		return syouhintani;
	}
	public void setSyouhintani(String syouhintani) {
		this.syouhintani = syouhintani;
	}
	public int getSyouhinsuryo() {
		return syouhinsuryo;
	}
	public void setSyouhinsuryo(int syouhinsuryo) {
		this.syouhinsuryo = syouhinsuryo;
	}
	public int getSyouhinmoney() {
		return syouhinmoney;
	}
	public void setSyouhinmoney(int syouhinmoney) {
		this.syouhinmoney = syouhinmoney;
	}
	public int getOthermoney() {
		return othermoney;
	}
	public void setOthermoney(int othermoney) {
		this.othermoney = othermoney;
	}
	public String getTotalsyouhinmoney() {
		return totalsyouhinmoney;
	}
	public void setTotalsyouhinmoney(String totalsyouhinmoney) {
		this.totalsyouhinmoney = totalsyouhinmoney;
	}
	public String getExplain() {
		return explain;
	}
	public void setExplain(String explain) {
		this.explain = explain;
	}
	public int getHatyusaki() {
		return hatyusaki;
	}
	public void setHatyusaki(int hatyusaki) {
		this.hatyusaki = hatyusaki;
	}
	public String getHasoubi() {
		return hasoubi;
	}
	public void setHasoubi(String hasoubi) {
		this.hasoubi = hasoubi;
	}

}
