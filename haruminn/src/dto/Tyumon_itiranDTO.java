package dto;

import java.util.ArrayList;


public class Tyumon_itiranDTO {
	//受注関係
		private	String tantoid;//担当者ID
		private String juchutantou;//受注担当
		private String tyumonday;//注文日時
		private int juchuno;//受注No

			//ご依頼主関係
		private String kyousankigyoumei;//協賛企業名
		private int kyosanno;//協賛企業No
		public int getKyosanno() {
			return kyosanno;
		}
		public void setKyosanno(int kyosanno) {
			this.kyosanno = kyosanno;
		}

		private	String syozokubusyo;//所属部署
		private	String chumonsyaname;//会員名
		private	String chumonsyakana;//カタカナ
		private	String syainno;//社番
		private	String chumonsyatel;//TEL
		private	String telkubun;//TEL区分

			//お届け先関係
		private	String todokesakikubun;//区分
		private	String todokesakijusho;//住所
		private	String todokesakiname;//名前
		private	String todokesakitel;//TEL
		public ArrayList<jutyushohin> getShohins() {
			return shohins;
		}
		public void setShohins(ArrayList<jutyushohin> shohins) {
			this.shohins = shohins;
		}
		private	String rusu;//留守

		private ArrayList<jutyushohin> shohins;

//			//注文商品
//		private	String syouhingou;//号
//		private	String syouhinpage;//頁
//		private	String syouhinno;//No
//		private	String syouhinmei;//商品名
//		private	String syouhinmodel;//型
//		private	String syouhincolor;//色
//		private	String syouhinpattern;//柄
//		private	String syouhinsize;//サイズ
//		private	int syouhintanka;//単価
//		private	String syouhintani;//単位
//		private	int syouhinsuryo;//送料
//
//		private	int syouhinmoney;//単価×数量
//		private	int othermoney;//他実費
//		private	String totalsyouhinmoney;//合計金額
//		private	String explain;//補足
//		private	int hatyusaki;	//契約企業番号(発注先)
//		private	String hasoubi;//発送日
//


		public int getTotalmoney() {
			return totalmoney;
		}
		public void setTotalmoney(int totalmoney) {
			this.totalmoney = totalmoney;
		}
		public int getTotalothermoney() {
			return totalothermoney;
		}
		public void setTotalothermoney(int totalothermoney) {
			this.totalothermoney = totalothermoney;
		}
		public int getAlltotalmoney() {
			return alltotalmoney;
		}
		public void setAlltotalmoney(int alltotalmoney) {
			this.alltotalmoney = alltotalmoney;
		}
		private	int totalmoney;//合計金額
		private	int totalothermoney;//合計他実費
		private	int alltotalmoney;//すべての合計金額
		private double riritu;//利率

			public double getRiritu() {
			return riritu;
		}
		public void setRiritu(double riritu) {
			this.riritu = riritu;
		}

		//支払い関係
		private	int paycount;//お支払い回数
		private	String howtopay;//支払い方法
		private	String seikyukubun;//請求区分
		private	String hurikomimotoginko;//振り込み元銀行
		private String siharaibi;//支払開始月


			public String getSiharaibi() {
			return siharaibi;
		}
		public void setSiharaibi(String siharaibi) {
			this.siharaibi = siharaibi;
		}
			public String getHurikomimotoginko() {
				return hurikomimotoginko;
			}
			public void setHurikomimotoginko(String hurikomimotoginko) {
				this.hurikomimotoginko = hurikomimotoginko;
			}
			//発注・請求・入金
			boolean hattyuumu;//発注有無
			boolean seikyuumu;//請求有無
			boolean nyukinumu;//入金有無


			public String getTantoid() {
				return tantoid;
			}
			public void setTantoid(String tantoid) {
				this.tantoid = tantoid;
			}
			public String getJuchutantou() {
				return juchutantou;
			}
			public void setJuchutantou(String juchutantou) {
				this.juchutantou = juchutantou;
			}
			public String getTyumonday() {
				return tyumonday;
			}
			public void setTyumonday(String tyumonday) {
				this.tyumonday = tyumonday;
			}
			public int getJuchuno() {
				return juchuno;
			}
			public void setJuchuno(int juchuno) {
				this.juchuno = juchuno;
			}
			public String getKyousankigyoumei() {
				return kyousankigyoumei;
			}
			public void setKyousankigyoumei(String kyousankigyoumei) {
				this.kyousankigyoumei = kyousankigyoumei;
			}

			public String getSyozokubusyo() {
				return syozokubusyo;
			}
			public void setSyozokubusyo(String syozokubusyo) {
				this.syozokubusyo = syozokubusyo;
			}
			public String getChumonsyaname() {
				return chumonsyaname;
			}
			public void setChumonsyaname(String chumonsyaname) {
				this.chumonsyaname = chumonsyaname;
			}
			public String getChumonsyakana() {
				return chumonsyakana;
			}
			public void setChumonsyakana(String chumonsyakana) {
				this.chumonsyakana = chumonsyakana;
			}
			public String getSyainno() {
				return syainno;
			}
			public void setSyainno(String syainno) {
				this.syainno = syainno;
			}
			public String getChumonsyatel() {
				return chumonsyatel;
			}
			public void setChumonsyatel(String chumonsyatel) {
				this.chumonsyatel = chumonsyatel;
			}
			public String getTelkubun() {
				return telkubun;
			}
			public void setTelkubun(String telkubun) {
				this.telkubun = telkubun;
			}
			public String getTodokesakikubun() {
				return todokesakikubun;
			}
			public void setTodokesakikubun(String todokesakikubun) {
				this.todokesakikubun = todokesakikubun;
			}

			public String getTodokesakijusho() {
				return todokesakijusho;
			}
			public void setTodokesakijusho(String todokesakijusho) {
				this.todokesakijusho = todokesakijusho;
			}
			public String getTodokesakiname() {
				return todokesakiname;
			}
			public void setTodokesakiname(String todokesakiname) {
				this.todokesakiname = todokesakiname;
			}
			public String getTodokesakitel() {
				return todokesakitel;
			}
			public void setTodokesakitel(String todokesakitel) {
				this.todokesakitel = todokesakitel;
			}
			public String getRusu() {
				return rusu;
			}
			public void setRusu(String rusu) {
				this.rusu = rusu;
			}

			public int getPaycount() {
				return paycount;
			}
			public void setPaycount(int paycount) {
				this.paycount = paycount;
			}
			public String getHowtopay() {
				return howtopay;
			}
			public void setHowtopay(String howtopay) {
				this.howtopay = howtopay;
			}
			public String getSeikyukubun() {
				return seikyukubun;
			}
			public void setSeikyukubun(String seikyukubun) {
				this.seikyukubun = seikyukubun;
			}
			public boolean isHattyuumu() {
				return hattyuumu;
			}
			public void setHattyuumu(boolean hattyuumu) {
				this.hattyuumu = hattyuumu;
			}
			public boolean isSeikyuumu() {
				return seikyuumu;
			}
			public void setSeikyuumu(boolean seikyuumu) {
				this.seikyuumu = seikyuumu;
			}
			public boolean isNyukinumu() {
				return nyukinumu;
			}
			public void setNyukinumu(boolean nyukinumu) {
				this.nyukinumu = nyukinumu;
			}

}
