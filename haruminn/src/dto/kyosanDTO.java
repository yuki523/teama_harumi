package dto;


public class kyosanDTO implements java.io.Serializable{



	int kyosanno;
	String kyosanname;
	String kyuyo;
	String tukijime;
	String kyosantelno;
	String kyosanjusho;
	String tantouname;
	String tantoukana;
	String tantoubusho;
	String tantoutelno;
	String kyosanyubin;
	public String getKyosanyubin() {
		return kyosanyubin;
	}
	public void setKyosanyubin(String kyosanyubin) {
		this.kyosanyubin = kyosanyubin;
	}
	public int getKyosanno() {
		return kyosanno;
	}
	public void setKyosanno(int kyosanno) {
		this.kyosanno = kyosanno;
	}
	public String getKyosanname() {
		return kyosanname;
	}
	public void setKyosanname(String kyosanname) {
		this.kyosanname = kyosanname;
	}
	public String getKyuyo() {
		return kyuyo;
	}
	public void setKyuyo(String string) {
		this.kyuyo = string;
	}
	public String getTukijime() {
		return tukijime;
	}
	public void setTukijime(String string) {
		this.tukijime = string;
	}
	public String getKyosantelno() {
		return kyosantelno;
	}
	public void setKyosantelno(String kyosantelno) {
		this.kyosantelno = kyosantelno;
	}
	public String getKyosanjusho() {
		return kyosanjusho;
	}
	public void setKyosanjusho(String kyosanjusho) {
		this.kyosanjusho = kyosanjusho;
	}
	public String getTantouname() {
		return tantouname;
	}
	public void setTantouname(String tantouname) {
		this.tantouname = tantouname;
	}
	public String getTantoukana() {
		return tantoukana;
	}
	public void setTantoukana(String tantoukana) {
		this.tantoukana = tantoukana;
	}
	public String getTantoubusho() {
		return tantoubusho;
	}
	public void setTantoubusho(String tantoubusho) {
		this.tantoubusho = tantoubusho;
	}
	public String getTantoutelno() {
		return tantoutelno;
	}
	public void setTantoutelno(String tantoutelno) {
		this.tantoutelno = tantoutelno;
	}




}
