package dto;

import java.util.Date;

public class WEBjuchuDTO {

	//受注
	int juchuno;
	String receptime;
	String kyosanno;
	String kaiinno;
	int sumgaku;
	int cost;
	int sumtax;
	double riritu;
	String tantouid;
	String hachuumu;
	String seikyuumu;
	String nyukinumu;
	//受注商品
	//int juchuno;
	String gou;
	String page;
	String shohinno;
	String shohinname;
	String model;
	String unit;
	int suryo;
	String hasoubi;
	int notaxtotal;
	int total;
	String explain;
	String iro;
	String gara;
	String size;
	String hatyuumu;
	int tax;
	//届先
	//int juchuno;
	String sendjusho;
	String sendname;
	String sendkubun;
	String rusu;
	String sendtelno;
	String sakierea;
	//支払
	//String kaiinno;
	//int juchuno;
	String payway;
	String paybank;
	int paysum;
	int paykaisu;
	int paygaku;
	Date paydate;
	//会員
	String kyousanname;;
	String busho;
	String kaiinname;
	String kaiinkana;
	String kaiinntelno;
	String kaiinntelkubun;
	String kaiinnpref;
	String kaiinnjusho;
	String kaiinnyubin;
	String kaiinemail;
	String pass;
	int yosin;
	int riyoutotal;
	//協賛
	//int kyosanno;
	String kyosanname;
	String kyuyo;
	String tukijime;
	String kyosantelno;
	String kyosanjusho;
	String tantouname;
	String tantoukana;
	String tantoubusho;
	String tantoutelno;
	String kyosanyubin;


	public String getKyousanname() {
		return kyousanname;
	}
	public void setKyousanname(String kyousanname) {
		this.kyousanname = kyousanname;
	}
	public String getBusho() {
		return busho;
	}
	public void setBusho(String busho) {
		this.busho = busho;
	}
	public String getKaiinname() {
		return kaiinname;
	}
	public void setKaiinname(String kaiinname) {
		this.kaiinname = kaiinname;
	}
	public String getKaiinkana() {
		return kaiinkana;
	}
	public void setKaiinkana(String kaiinkana) {
		this.kaiinkana = kaiinkana;
	}
	public String getKaiinntelno() {
		return kaiinntelno;
	}
	public void setKaiinntelno(String kaiinntelno) {
		this.kaiinntelno = kaiinntelno;
	}
	public String getKaiinntelkubun() {
		return kaiinntelkubun;
	}
	public void setKaiinntelkubun(String kaiinntelkubun) {
		this.kaiinntelkubun = kaiinntelkubun;
	}
	public String getKaiinnpref() {
		return kaiinnpref;
	}
	public void setKaiinnpref(String kaiinnpref) {
		this.kaiinnpref = kaiinnpref;
	}
	public String getKaiinnjusho() {
		return kaiinnjusho;
	}
	public void setKaiinnjusho(String kaiinnjusho) {
		this.kaiinnjusho = kaiinnjusho;
	}
	public String getKaiinnyubin() {
		return kaiinnyubin;
	}
	public void setKaiinnyubin(String kaiinnyubin) {
		this.kaiinnyubin = kaiinnyubin;
	}
	public String getKaiinemail() {
		return kaiinemail;
	}
	public void setKaiinemail(String kaiinemail) {
		this.kaiinemail = kaiinemail;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getKyosanname() {
		return kyosanname;
	}
	public void setKyosanname(String kyosanname) {
		this.kyosanname = kyosanname;
	}
	public String getKyuyo() {
		return kyuyo;
	}
	public void setKyuyo(String kyuyo) {
		this.kyuyo = kyuyo;
	}
	public String getTukijime() {
		return tukijime;
	}
	public void setTukijime(String tukijime) {
		this.tukijime = tukijime;
	}
	public String getKyosantelno() {
		return kyosantelno;
	}
	public void setKyosantelno(String kyosantelno) {
		this.kyosantelno = kyosantelno;
	}
	public String getKyosanjusho() {
		return kyosanjusho;
	}
	public void setKyosanjusho(String kyosanjusho) {
		this.kyosanjusho = kyosanjusho;
	}
	public String getTantouname() {
		return tantouname;
	}
	public void setTantouname(String tantouname) {
		this.tantouname = tantouname;
	}
	public String getTantoukana() {
		return tantoukana;
	}
	public void setTantoukana(String tantoukana) {
		this.tantoukana = tantoukana;
	}
	public String getTantoubusho() {
		return tantoubusho;
	}
	public void setTantoubusho(String tantoubusho) {
		this.tantoubusho = tantoubusho;
	}
	public String getTantoutelno() {
		return tantoutelno;
	}
	public void setTantoutelno(String tantoutelno) {
		this.tantoutelno = tantoutelno;
	}
	public String getKyosanyubin() {
		return kyosanyubin;
	}
	public void setKyosanyubin(String kyosanyubin) {
		this.kyosanyubin = kyosanyubin;
	}
	public int getJuchuno() {
		return juchuno;
	}
	public void setJuchuno(int juchuno) {
		this.juchuno = juchuno;
	}
	public String getReceptime() {
		return receptime;
	}
	public void setReceptime(String receptime) {
		this.receptime = receptime;
	}
	public String getKyosanno() {
		return kyosanno;
	}
	public void setKyosanno(String kyosanno) {
		this.kyosanno = kyosanno;
	}
	public String getKaiinno() {
		return kaiinno;
	}
	public void setKaiinno(String kaiinno) {
		this.kaiinno = kaiinno;
	}
	public int getSumgaku() {
		return sumgaku;
	}
	public void setSumgaku(int sumgaku) {
		this.sumgaku = sumgaku;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public int getSumtax() {
		return sumtax;
	}
	public void setSumtax(int sumtax) {
		this.sumtax = sumtax;
	}
	public double getRiritu() {
		return riritu;
	}
	public void setRiritu(double riritu) {
		this.riritu = riritu;
	}
	public String getTantouid() {
		return tantouid;
	}
	public void setTantouid(String tantouid) {
		this.tantouid = tantouid;
	}
	public String getHachuumu() {
		return hachuumu;
	}
	public void setHachuumu(String hachuumu) {
		this.hachuumu = hachuumu;
	}
	public String getSeikyuumu() {
		return seikyuumu;
	}
	public void setSeikyuumu(String seikyuumu) {
		this.seikyuumu = seikyuumu;
	}
	public String getNyukinumu() {
		return nyukinumu;
	}
	public void setNyukinumu(String nyukinumu) {
		this.nyukinumu = nyukinumu;
	}
	public String getGou() {
		return gou;
	}
	public void setGou(String gou) {
		this.gou = gou;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getShohinno() {
		return shohinno;
	}
	public void setShohinno(String shohinno) {
		this.shohinno = shohinno;
	}
	public int getSuryo() {
		return suryo;
	}
	public void setSuryo(int suryo) {
		this.suryo = suryo;
	}
	public String getHasoubi() {
		return hasoubi;
	}
	public void setHasoubi(String hasoubi) {
		this.hasoubi = hasoubi;
	}
	public int getNotaxtotal() {
		return notaxtotal;
	}
	public void setNotaxtotal(int notaxtotal) {
		this.notaxtotal = notaxtotal;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getExplain() {
		return explain;
	}
	public void setExplain(String explain) {
		this.explain = explain;
	}
	public String getIro() {
		return iro;
	}
	public void setIro(String iro) {
		this.iro = iro;
	}
	public String getGara() {
		return gara;
	}
	public void setGara(String gara) {
		this.gara = gara;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getHatyuumu() {
		return hatyuumu;
	}
	public void setHatyuumu(String hatyuumu) {
		this.hatyuumu = hatyuumu;
	}
	public int getTax() {
		return tax;
	}
	public void setTax(int tax) {
		this.tax = tax;
	}
	public String getSendjusho() {
		return sendjusho;
	}
	public void setSendjusho(String sendjusho) {
		this.sendjusho = sendjusho;
	}
	public String getSendname() {
		return sendname;
	}
	public void setSendname(String sendname) {
		this.sendname = sendname;
	}
	public String getSendkubun() {
		return sendkubun;
	}
	public void setSendkubun(String sendkubun) {
		this.sendkubun = sendkubun;
	}
	public String getRusu() {
		return rusu;
	}
	public void setRusu(String rusu) {
		this.rusu = rusu;
	}
	public String getSendtelno() {
		return sendtelno;
	}
	public void setSendtelno(String sendtelno) {
		this.sendtelno = sendtelno;
	}
	public String getSakierea() {
		return sakierea;
	}
	public void setSakierea(String sakierea) {
		this.sakierea = sakierea;
	}
	public String getPayway() {
		return payway;
	}
	public void setPayway(String payway) {
		this.payway = payway;
	}
	public String getPaybank() {
		return paybank;
	}
	public void setPaybank(String paybank) {
		this.paybank = paybank;
	}
	public int getPaysum() {
		return paysum;
	}
	public void setPaysum(int paysum) {
		this.paysum = paysum;
	}
	public int getPaykaisu() {
		return paykaisu;
	}
	public void setPaykaisu(int paykaisu) {
		this.paykaisu = paykaisu;
	}
	public int getPaygaku() {
		return paygaku;
	}
	public void setPaygaku(int paygaku) {
		this.paygaku = paygaku;
	}
	public Date getPaydate() {
		return paydate;
	}
	public void setPaydate(Date paydate) {
		this.paydate = paydate;
	}
	public String getShohinname() {
		return shohinname;
	}
	public void setShohinname(String shohinname) {
		this.shohinname = shohinname;
	}

	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public int getYosin() {
		return yosin;
	}
	public void setYosin(int yosin) {
		this.yosin = yosin;
	}
	public int getRiyoutotal() {
		return riyoutotal;
	}
	public void setRiyoutotal(int riyoutotal) {
		this.riyoutotal = riyoutotal;
	}




}
