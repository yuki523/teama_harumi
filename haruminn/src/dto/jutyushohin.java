package dto;

public class jutyushohin {

	int juchuno;
	String gou;
	String page;
	String shohinno;
	int suryo;
	String hasoubi;
	int notaxtotal;
	int total;
	String explain;
	String iro;
	String gara;
	String size;
	String hatyuumu;
	int tax;
	String shohinname;
	int tanka;
	String model;
	String tani;




	public String getShohinname() {
		return shohinname;
	}
	public void setShohinname(String shohinname) {
		this.shohinname = shohinname;
	}
	public int getTanka() {
		return tanka;
	}
	public void setTanka(int tanka) {
		this.tanka = tanka;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getTani() {
		return tani;
	}
	public void setTani(String tani) {
		this.tani = tani;
	}
	public int getJuchuno() {
		return juchuno;
	}
	public void setJuchuno(int juchuno) {
		this.juchuno = juchuno;
	}
	public String getGou() {
		return gou;
	}
	public void setGou(String gou) {
		this.gou = gou;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getShohinno() {
		return shohinno;
	}
	public void setShohinno(String shohinno) {
		this.shohinno = shohinno;
	}
	public int getSuryo() {
		return suryo;
	}
	public void setSuryo(int suryo) {
		this.suryo = suryo;
	}
	public String getHasoubi() {
		return hasoubi;
	}
	public void setHasoubi(String hasoubi) {
		this.hasoubi = hasoubi;
	}
	public int getNotaxtotal() {
		return notaxtotal;
	}
	public void setNotaxtotal(int notaxtotal) {
		this.notaxtotal = notaxtotal;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getExplain() {
		return explain;
	}
	public void setExplain(String explain) {
		this.explain = explain;
	}
	public String getIro() {
		return iro;
	}
	public void setIro(String iro) {
		this.iro = iro;
	}
	public String getGara() {
		return gara;
	}
	public void setGara(String gara) {
		this.gara = gara;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getHatyuumu() {
		return hatyuumu;
	}
	public void setHatyuumu(String hatyuumu) {
		this.hatyuumu = hatyuumu;
	}
	public int getTax() {
		return tax;
	}
	public void setTax(int tax) {
		this.tax = tax;
	}



}
