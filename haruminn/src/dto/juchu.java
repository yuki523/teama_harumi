package dto;
import java.util.Date;

import javax.xml.crypto.Data;


public class juchu {

	int juchuno;
	Data receptime;
	String kyosanno;
	String kaiinno;
	int sumgaku;
	int cost;
	int sumtax;
	int riritu;
	String tantouid;
	String hachuumu;
	String seikyuumu;
	String nyukinumu;

	String gou;
	String page;
	String shohinno;
	String iro;
	String gara;
	String shoshinsize;
	int suryo;
	Date hassodate;
	int nontaxsum;
	int taxsum;
	String hosoku;
	boolean rate;


	public int getJuchuno() {
		return juchuno;
	}
	public void setJuchuno(int juchuno) {
		this.juchuno = juchuno;
	}
	public Data getReceptime() {
		return receptime;
	}
	public void setReceptime(Data receptime) {
		this.receptime = receptime;
	}
	public String getKyosanno() {
		return kyosanno;
	}
	public void setKyosanno(String kyosanno) {
		this.kyosanno = kyosanno;
	}
	public String getKaiinno() {
		return kaiinno;
	}
	public void setKaiinno(String kaiinno) {
		this.kaiinno = kaiinno;
	}
	public int getSumgaku() {
		return sumgaku;
	}
	public void setSumgaku(int sumgaku) {
		this.sumgaku = sumgaku;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public int getSumtax() {
		return sumtax;
	}
	public void setSumtax(int sumtax) {
		this.sumtax = sumtax;
	}
	public int getRiritu() {
		return riritu;
	}
	public void setRiritu(int riritu) {
		this.riritu = riritu;
	}
	public String getTantouid() {
		return tantouid;
	}
	public void setTantouid(String tantouid) {
		this.tantouid = tantouid;
	}
	public String getHachuumu() {
		return hachuumu;
	}
	public void setHachuumu(String hachuumu) {
		this.hachuumu = hachuumu;
	}
	public String getSeikyuumu() {
		return seikyuumu;
	}
	public void setSeikyuumu(String seikyuumu) {
		this.seikyuumu = seikyuumu;
	}
	public String getNyukinumu() {
		return nyukinumu;
	}
	public void setNyukinumu(String nyukinumu) {
		this.nyukinumu = nyukinumu;
	}
	public String getGou() {
		return gou;
	}
	public void setGou(String gou) {
		this.gou = gou;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getShohinno() {
		return shohinno;
	}
	public void setShohinno(String shohinno) {
		this.shohinno = shohinno;
	}
	public String getIro() {
		return iro;
	}
	public void setIro(String iro) {
		this.iro = iro;
	}
	public String getGara() {
		return gara;
	}
	public void setGara(String gara) {
		this.gara = gara;
	}
	public String getShoshinsize() {
		return shoshinsize;
	}
	public void setShoshinsize(String shoshinsize) {
		this.shoshinsize = shoshinsize;
	}
	public int getSuryo() {
		return suryo;
	}
	public void setSuryo(int suryo) {
		this.suryo = suryo;
	}
	public Date getHassodate() {
		return hassodate;
	}
	public void setHassodate(Date hassodate) {
		this.hassodate = hassodate;
	}
	public int getNontaxsum() {
		return nontaxsum;
	}
	public void setNontaxsum(int nontaxsum) {
		this.nontaxsum = nontaxsum;
	}
	public int getTaxsum() {
		return taxsum;
	}
	public void setTaxsum(int taxsum) {
		this.taxsum = taxsum;
	}
	public String getHosoku() {
		return hosoku;
	}
	public void setHosoku(String hosoku) {
		this.hosoku = hosoku;
	}
	public boolean isRate() {
		return rate;
	}
	public void setRate(boolean rate) {
		this.rate = rate;
	}



}

