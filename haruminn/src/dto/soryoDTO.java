package dto;

public class soryoDTO {
	String gyosha;
	String motoarea;
	String sakiarea;
	String boxsize;
	int soryo;
	public String getGyosha() {
		return gyosha;
	}
	public void setGyosha(String gyosha) {
		this.gyosha = gyosha;
	}
	public String getMotoarea() {
		return motoarea;
	}
	public void setMotoarea(String motoarea) {
		this.motoarea = motoarea;
	}
	public String getSakiarea() {
		return sakiarea;
	}
	public void setSakiarea(String sakiarea) {
		this.sakiarea = sakiarea;
	}
	public String getBoxsize() {
		return boxsize;
	}
	public void setBoxsize(String boxsize) {
		this.boxsize = boxsize;
	}
	public int getSoryo() {
		return soryo;
	}
	public void setSoryo(int soryo) {
		this.soryo = soryo;
	}

}
