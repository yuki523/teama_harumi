package dto;
import java.util.Date;


public class payDTO {

	String kaiinno;
	int juchuno;
	String payway;
	String paybank;
	int paysum;
	int paykaisu;
	int paygaku;
	Date paydate;
	public String getKaiinno() {
		return kaiinno;
	}
	public void setKaiinno(String kaiinno) {
		this.kaiinno = kaiinno;
	}
	public int getJuchuno() {
		return juchuno;
	}
	public void setJuchuno(int juchuno) {
		this.juchuno = juchuno;
	}
	public String getPayway() {
		return payway;
	}
	public void setPayway(String payway) {
		this.payway = payway;
	}
	public String getPaybank() {
		return paybank;
	}
	public void setPaybank(String paybank) {
		this.paybank = paybank;
	}
	public int getPaysum() {
		return paysum;
	}
	public void setPaysum(int paysum) {
		this.paysum = paysum;
	}
	public int getPaykaisu() {
		return paykaisu;
	}
	public void setPaykaisu(int paykaisu) {
		this.paykaisu = paykaisu;
	}
	public int getPaygaku() {
		return paygaku;
	}
	public void setPaygaku(int paygaku) {
		this.paygaku = paygaku;
	}
	public Date getPaydate() {
		return paydate;
	}
	public void setPaydate(Date paydate) {
		this.paydate = paydate;
	}

}
