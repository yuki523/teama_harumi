
<%@page import="web.WebJucyuDAO"%>
<%@page import="dto.shohinDTO"%>
<%@page import="dto.kaiinDTO"%>
<%@page import="dto.juchu"%>
<%@page import="java.util.ArrayList"%>

<%@ page language="java" contentType="text/html; charset=UTF8"
    pageEncoding="UTF8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% request.setCharacterEncoding("UTF8");%>
<%
HttpSession ss = request.getSession();
ArrayList<shohinDTO> shohin = (ArrayList<shohinDTO>)session.getAttribute("selectShohin");
kaiinDTO member = (kaiinDTO)ss.getAttribute("member");
//セッションが切れているなら
if (member == null){
	response.sendRedirect("cartMiss.html");
}else{
	if(shohin==null){

		response.sendRedirect("CartNG.html");
	}else{

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<title>ショッピングカートの内容</title>
</head>
<body>

<hr size="25" color="#333333" width="max">
<br>
<h2>　　　　<a href="toppage.jsp">HARUMI</a></h2>

<Div align=right>
<% if (member==null) { %>
<a href="LoginJudge.jsp">ログイン</a>
<% } else { %>
<a href="http://localhost:8080/haruminn/LogOut">ログアウト</a>
<a href="http://localhost:8080/haruminn/cart.jsp">カート</a>
<% }  %>
</Div>
<br>
<hr size="1" color="#333333" width="900">
<br>
<br>
<br>


<form action="http://localhost:8080/haruminn/WebSaisyukakuteiServ" method="post">

<%


	int goukei=0;
%>

<Div align=center>
<h3>購入予定商品一覧表</h3>
<br>
 <table border="1">
 <thead>
      <tr>
        <th>商品番号</th>
        <th>商品名</th>
        <th>サイズ</th>
        <th>色</th>
        <th>柄</th>
        <th>単価</th>
        <th>数量</th>
      </tr>
    </thead>
 <%
for(shohinDTO a : shohin){
 %>
    <tbody>
      <tr>
        <td><%= a.getGou() %><%= a.getPage() %><%= a.getShohinno() %></td>
        <td><%= a.getShohinname() %></td>
        <td><%= a.getSize() %></td>
        <td><%= a.getIro() %></td>
        <td><%= a.getGara() %></td>
        <td><%= a.getTanka() %></td>
        <% goukei = goukei+a.getTanka(); %>
        <td>1</td>
      </tr>
    </tbody>
<%
}
%>
  </table>
 </Div>

 <br><br>
 <br>

   <Div Align="center">
   <Font Size="7">
   <table border="1">
    <thead>
      <tr>
        <th>合計金額</th>
        <th><%=(int)Math.floor(goukei*1.08) %>円</th>
      </tr>
    </thead>
    </tbody>
   </table>
   </Font>
   </Div>

<br>
<br>
<hr size="1" color="#333333" width="900">
<br>

<%

String kaiinname =(String)ss.getAttribute("kaiinname");
String kaiinyubin=(String)ss.getAttribute("kaiinyubin");
String jyusho=(String)ss.getAttribute("kaiinjusho");
String tel=(String)ss.getAttribute("kaiintelno");
String kubun =(String)ss.getAttribute("kubun");
String rusu=(String)ss.getAttribute("rusunotoki");

%>
<br><br>
<Div Align="center">
<h3>届先入力</h3>
  <table border="1">
  <tr>
    <td>届先氏名</td>
    <td>郵便番号</td>
    <td>住所</td>
    <td>届先区分</td>
    <td>TEL</td>
    <td>留守の時</td>
  </tr>
  <tr>
    <td><%=kaiinname %></td>
    <td><%=kaiinyubin%></td>
    <td><%=jyusho%></td>
	<td><%=kubun %></td>
	 <td><%=tel%></td>
    <td><%=rusu%></td>
  </tr>
</table>

<%
String shiharai=(String)ss.getAttribute("shiharai");
String kaisu=(String)ss.getAttribute("kaisu");
String ginkouname=(String)ss.getAttribute("ginkouname");


%>

<br><br>
<h3>支払条件</h3>
 <table border="1">
  <tr>
    <td>支払方法</td>

  </tr>
  <tr>
    <td><%=shiharai %></td>
    <%if(shiharai.equals("銀行振り込み")){ %>
    <td><%=ginkouname%>銀行</td>
    <%}else if(shiharai.equals("給与控除1回")){ %>
    <td></td>
    <%}else{%>
      <td><%=kaisu %>回</td>
     <%} %>
      </tr>
</table>
</Div>
<br><br><br>


 <INPUT type="submit" value="上記の内容で注文する" >
</form>

<INPUT type="button" Value="前のページに戻る" onClick="history.go(-1);">
<br>
<br>
<br>
<div style="position: relative;">
<img src="foot.png" width="100%" height="130">
</div>

</body>
<%}} %>
</html>