<%@ page language="java" contentType="text/html; charset=UTF8"
    pageEncoding="UTF8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang= "ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<link rel="stylesheet"  type="text/css" href="tyumonuketuke.css"/>
<title>注文受付票</title>

<!-- jQueryの読み込み -->
<script src="js/jquery-1.10.2.js" ></script>
<script src="js/shohinsearch.js"></script>

</head>
<body>

<script type="text/javascript">
<!--
function CheckBlank(){
	//alert("blank");

	if(document.Form1["yubin"].value=="" || document.Form1["todokesakipref"].value=="" || document.Form1["todokesakijusho"].value=="" || document.Form1["name"].value==""){

		alert('お届け先をご入力ください');
			return false;

	}else if(document.Form1["syohinname1"].value==""){

		alert('商品が選択されていません');
			return false;

	}else if((document.Form1["gou1"].value=="" || document.Form1["page1"].value=="" || document.Form1["No1"].value=="") && document.Form1["syohinname1"].value!=""){

		alert('商品番号をご入力ください');
			return false;
	}else if((document.Form1["gou2"].value=="" || document.Form1["page2"].value=="" || document.Form1["No2"].value=="") && document.Form1["syohinname2"].value!=""){

		alert('商品番号をご入力ください');
			return false;
	}else if((document.Form1["gou3"].value=="" || document.Form1["page3"].value=="" || document.Form1["No3"].value=="") && document.Form1["syohinname3"].value!=""){

		alert('商品番号をご入力ください');
			return false;
	}else if((document.Form1["gou4"].value=="" || document.Form1["page4"].value=="" || document.Form1["No4"].value=="") && document.Form1["syohinname4"].value!=""){

		alert('商品番号をご入力ください');
			return false;
	}else if((document.Form1["gou5"].value=="" || document.Form1["page5"].value=="" || document.Form1["No5"].value=="") && document.Form1["syohinname5"].value!=""){

		alert('商品番号をご入力ください');
			return false;
	}else if((document.Form1["gou6"].value=="" || document.Form1["page6"].value=="" || document.Form1["No6"].value=="") && document.Form1["syohinname6"].value!=""){

		alert('商品番号をご入力ください');
			return false;
	}else if((document.Form1["gou2"].value!="" || document.Form1["page2"].value!="" || document.Form1["No2"].value!="") && document.Form1["syohinname2"].value==""){

		alert('商品名をご入力ください');
			return false;
	}else if((document.Form1["gou3"].value!="" || document.Form1["page3"].value!="" || document.Form1["No3"].value!="") && document.Form1["syohinname3"].value==""){

		alert('商品名をご入力ください');
			return false;
	}else if((document.Form1["gou4"].value!="" || document.Form1["page4"].value!="" || document.Form1["No4"].value!="") && document.Form1["syohinname4"].value==""){

		alert('商品名をご入力ください');
			return false;
	}else if((document.Form1["gou5"].value!="" || document.Form1["page5"].value!="" || document.Form1["No5"].value!="") && document.Form1["syohinname5"].value==""){

		alert('商品名をご入力ください');
			return false;
	}else if((document.Form1["gou6"].value!="" || document.Form1["page6"].value!="" || document.Form1["No6"].value!="") && document.Form1["syohinname6"].value==""){

		alert('商品名をご入力ください');
			return false;
	}




}



// -->
</script>

<form action="Tymonuketukehyo" method="post" name="Form1"onSubmit="return (CheckBlank())">
<table>
	<tr>
	<%@page import="java.util.ArrayList" %>
	<%@page import="dto.kyosanDTO" %>
	<%@page import="dto.kaiinDTO" %>
	<%@page import="dto.shohinDTO" %>
	<%@page import="dto.harumiDTO" %>

	<%kyosanDTO kyosankigyo = (kyosanDTO)session.getAttribute("kaiinkyousankigyo");%>
	<%kaiinDTO kaiininfo = (kaiinDTO)session.getAttribute("kaiininfo");%>
	<%ArrayList<harumiDTO> harumisyain = (ArrayList<harumiDTO>)session.getAttribute("harumisyain");%>



<%--ご依頼主情報 --%>
		<td colspan="4">ご依頼主</td>

		<td colspan="2"><label for="jyutyutanto">受注担当者</label> <select id="jyutyutanto" name="jyutyutanto">
		<%for(harumiDTO a : harumisyain){ %>
			<option value=<%=a.getHarumiid() %>><%=a.getHaruminame() %></option>
		<%}%>
		</td>
		<td colspan="2">

		</select></td>
		<td colspan="2">注文受付日</td>
		<td colspan="5"><%= session.getAttribute("year") %>年<%= session.getAttribute("month") %>月<%= session.getAttribute("date") %>日</td>
		<%--/form --%>
		<td>番号</td>
		<td colspan="3"></td>
	</tr>
	<tr>
		<td colspan="2">協賛企業名</td>
		<td colspan="2"><%= kyosankigyo.getKyosanname()%></td>
		<td colspan="2">台帳番号</td>
		<td><%= kyosankigyo.getKyosanno() %></td>
		<td>所属</td>
		<td><%=kaiininfo.getBusho() %></td>
		<td colspan="4">お届先</td>
		<td colspan="3">届先区分</td>
		<td colspan="5"><select id="todokesaki" name="todokesaki">
			<option value="自宅">自宅</option>
			<option value="その他">その他</option></select></td>
	</tr>
	<tr>
		<td colspan="2">会員フリガナ</td>
		<td colspan="3"><%=kaiininfo.getKaiinkana() %></td>
		<td colspan="2">社員番号</td>
		<td colspan="2"><%=kaiininfo.getKaiinno() %></td>

		<td colspan="11">〒<input type="text" size="10" maxlength="100" name="yubin" value="<%=kaiininfo.getKaiinnyubin() %>">
		<input type="text" size="5" maxlength="100" name="todokesakipref" value="<%=kaiininfo.getKaiinnpref()%>"><input type="text" size="20" maxlength="100" name="todokesakijusho" value="<%=kaiininfo.getKaiinnjusho() %>"></td>
	</tr>
	<tr>
		<td colspan="2">会員氏名</td>
		<td colspan="3"><%=kaiininfo.getKaiinname() %></td>
		<td colspan="4"></td>
		<td colspan="4"></td>
		<td colspan="3">留守のとき</td>
		<td colspan="4"><input type="text" size="10" maxlength="100" name="rusu"></td>
	</tr>
	<tr>
		<td colspan="2">連絡先TEL</td>
		<td colspan="3"><%=kaiininfo.getKaiinntelno() %></td>
		<td>区分</td>
		<td colspan="3"><select id="telkubun" name="telkubun">
			<option value="会社">会社</option>
			<option value="住所">自宅</option>
			<option value="その他">その他</option></select></td>
		<td>氏名</td>
		<td colspan="3"><input type="text" size="10" maxlength="100" name="name" value="<%=kaiininfo.getKaiinname() %>"></td>
		<td colspan="3">電話番号</td>
		<td colspan="4"><input type="text" size="10" maxlength="100" name="tel" value="<%=kaiininfo.getKaiinntelno() %>"></td>
	</tr>

	<%--ご注文商品--%>

	<tr>
		<td>号</td>
		<td>頁</td>
		<td>No</td>
		<td>商品名</td>
		<td>型式</td>
		<td>色</td>
		<td>柄</td>
		<td>サイズ</td>
		<td>単価</td>
		<td>単位</td>
		<td>数量</td>
		<td>金額</td>
		<td>実費</td>
		<td colspan="2">合計金額</td>
		<td colspan="2">補足</td>
		<td>発注先</td>
		<td>発送日</td>
	</tr>
	<%--商品1つ目--%>
	<tr>
		<td><input type="text" size="2" maxlength="100" name="gou1" id="gou1" class="itemcode" rownum="1">
		</td>
		<td><input type="text" size="2" maxlength="100" name="page1" id="page1"  class="itemcode" rownum="1">
		</td>
		<td><input type="text" size="2" maxlength="100" name="No1" id="No1" class="itemcode"  rownum="1">
		</td>
		<td>
			<input type="text" size="15" maxlength="100" name="syohinname1"  id="syohinname1" rownum="1"readonly>
		</td>
		<td><input type="text" size="5" maxlength="100" name="kata1" rownum="1"readonly>
		</td>
		<td>
			<!-- <input type="text" size="2" maxlength="100" name="iro1" rownum="1" class="iro_single"> -->
			<select name="iro1" id="iro1_select" ></select>

		</td>
		<td>
			<!--<input type="text" size="2" maxlength="100" name="gara1" rownum="1"> -->
			<select name="gara1" id="gara1_select" ></select>
		</td>
		<td>
			<!--<input type="text" size="2" maxlength="100" name="size1" rownum="1"> -->
			<select name="size1" id="size1_select" ></select>
		</td>
		<td><input type="text" size="10" maxlength="100" name="tanka1" id="tanka1" rownum="1"readonly>
		</td>
		<td><input type="text" size="10" maxlength="100" name="tani1" id="tani1" rownum="1"readonly>
		</td>
		<td><select id="suryo1" name="suryo1" yen="0" class="itemcount" rownum="1">
			<option value=1>1</option>
			<option value=2>2</option>
			<option value=3>3</option>
			<option value=4>4</option>
			<option value=5>5</option>
			<option value=6>6</option>
			<option value=7>7</option>
			<option value=8>8</option>
			<option value=9>9</option>
			<option value=10>10</option></select></td>
		<td><div><input type="text" size="10" maxlength="100" name="kingaku1" id="kingaku1" rownum="1"readonly></div></td>
		<td><input type="text" size="10" maxlength="100" name="jippi1" id="jippi1" rownum="1"readonly></td>
		<td colspan="2"><input type="text" size="10" maxlength="100" name="gokei1" id="gokei1" rownum="1"readonly></td>
		<td colspan="2"><input type="text" size="10" maxlength="100" name="hosoku1" id="hosoku1" rownum="1"readonly></td>
		<td><input type="text" size="5" maxlength="100" name="hattyu1" id="hattyu1" rownum="1"></td>
		<td><input type="text" size="5" maxlength="100" name="hassou1" id="hassou1" rownum="1"></td>
	</tr>

		<%--商品2つ目--%>
	<tr>
		<td><input type="text" size="2" maxlength="100" name="gou2" id="gou2" class="itemcode" rownum="2">
		</td>
		<td><input type="text" size="2" maxlength="100" name="page2" id="page2"  class="itemcode" rownum="2">
		</td>
		<td><input type="text" size="2" maxlength="100" name="No2" id="No2" class="itemcode"  rownum="2">
		</td>
		<td>
			<input type="text" size="15" maxlength="100" name="syohinname2"  id="syohinname2" rownum="2"readonly>
		</td>
		<td><input type="text" size="5" maxlength="100" name="kata2" rownum="2"readonly>
		</td>
		<td>
			<!-- <input type="text" size="2" maxlength="100" name="iro2" rownum="2" class="iro_single"> -->
			<select name="iro2" id="iro2_select" ></select>

		</td>
		<td>
			<!--<input type="text" size="2" maxlength="100" name="gara2" rownum="2"> -->
			<select name="gara2" id="gara2_select" ></select>
		</td>
		<td>
			<!--<input type="text" size="2" maxlength="100" name="size2" rownum="2"> -->
			<select name="size2" id="size2_select" ></select>
		</td>
		<td><input type="text" size="10" maxlength="100" name="tanka2" id="tanka2" rownum="2"readonly>
		</td>
		<td><input type="text" size="10" maxlength="100" name="tani2" id="tani2" rownum="2"readonly>
		</td>
		<td><select id="suryo2" name="suryo2" yen="0" class="itemcount" rownum="2">
			<option value=1>1</option>
			<option value=2>2</option>
			<option value=3>3</option>
			<option value=4>4</option>
			<option value=5>5</option>
			<option value=6>6</option>
			<option value=7>7</option>
			<option value=8>8</option>
			<option value=9>9</option>
			<option value=10>10</option></select></td>
		<td><div><input type="text" size="10" maxlength="100" name="kingaku2" id="kingaku2" rownum="2"readonly></div></td>
		<td><input type="text" size="10" maxlength="100" name="jippi2" id="jippi2" rownum="2"readonly></td>
		<td colspan="2"><input type="text" size="10" maxlength="100" name="gokei2" id="gokei2" rownum="2"readonly></td>
		<td colspan="2"><input type="text" size="10" maxlength="100" name="hosoku2" id="hosoku2" rownum="2"readonly></td>
		<td><input type="text" size="5" maxlength="100" name="hattyu2" id="hattyu2" rownum="2"></td>
		<td><input type="text" size="5" maxlength="100" name="hassou2" id="hassou2" rownum="2"></td>
	</tr>


		<%--商品3つ目--%>

	<tr>
		<td><input type="text" size="2" maxlength="100" name="gou3" id="gou3" class="itemcode" rownum="3">
		</td>
		<td><input type="text" size="2" maxlength="100" name="page3" id="page3"  class="itemcode" rownum="3">
		</td>
		<td><input type="text" size="2" maxlength="100" name="No3" id="No3" class="itemcode"  rownum="3">
		</td>
		<td>
			<input type="text" size="15" maxlength="100" name="syohinname3"  id="syohinname3" rownum="3"readonly>
		</td>
		<td><input type="text" size="5" maxlength="100" name="kata3" rownum="3"readonly>
		</td>
		<td>
			<!-- <input type="text" size="2" maxlength="100" name="iro3" rownum="3" class="iro_single"> -->
			<select name="iro3" id="iro3_select" ></select>

		</td>
		<td>
			<!--<input type="text" size="2" maxlength="100" name="gara3" rownum="3"> -->
			<select name="gara3" id="gara3_select" ></select>
		</td>
		<td>
			<!--<input type="text" size="2" maxlength="100" name="size3" rownum="3"> -->
			<select name="size3" id="size3_select" ></select>
		</td>
		<td><input type="text" size="10" maxlength="100" name="tanka3" id="tanka3" rownum="3"readonly>
		</td>
		<td><input type="text" size="10" maxlength="100" name="tani3" id="tani3" rownum="3"readonly>
		</td>
		<td><select id="suryo3" name="suryo3" yen="0" class="itemcount" rownum="3">
			<option value=1>1</option>
			<option value=2>2</option>
			<option value=3>3</option>
			<option value=4>4</option>
			<option value=5>5</option>
			<option value=6>6</option>
			<option value=7>7</option>
			<option value=8>8</option>
			<option value=9>9</option>
			<option value=10>10</option></select></td>
		<td><div><input type="text" size="10" maxlength="100" name="kingaku3" id="kingaku3" rownum="3"readonly></div></td>
		<td><input type="text" size="10" maxlength="100" name="jippi3" id="jippi3" rownum="3"readonly></td>
		<td colspan="2"><input type="text" size="10" maxlength="100" name="gokei3" id="gokei3" rownum="3"readonly></td>
		<td colspan="2"><input type="text" size="10" maxlength="100" name="hosoku3" id="hosoku3" rownum="3"readonly></td>
		<td><input type="text" size="5" maxlength="100" name="hattyu3" id="hattyu3" rownum="3"></td>
		<td><input type="text" size="5" maxlength="100" name="hassou3" id="hassou3" rownum="3"></td>
	</tr>

	<%--商品4つ目--%>

	<tr>
		<td><input type="text" size="2" maxlength="100" name="gou4" id="gou4" class="itemcode" rownum="4">
		</td>
		<td><input type="text" size="2" maxlength="100" name="page4" id="page4"  class="itemcode" rownum="4">
		</td>
		<td><input type="text" size="2" maxlength="100" name="No4" id="No4" class="itemcode"  rownum="4">
		</td>
		<td>
			<input type="text" size="15" maxlength="100" name="syohinname4"  id="syohinname4" rownum="4"readonly>
		</td>
		<td><input type="text" size="5" maxlength="100" name="kata4" rownum="4"readonly>
		</td>
		<td>
			<!-- <input type="text" size="2" maxlength="100" name="iro4" rownum="4" class="iro_single"> -->
			<select name="iro4" id="iro4_select" ></select>

		</td>
		<td>
			<!--<input type="text" size="2" maxlength="100" name="gara4" rownum="4"> -->
			<select name="gara4" id="gara4_select" ></select>
		</td>
		<td>
			<!--<input type="text" size="2" maxlength="100" name="size4" rownum="4"> -->
			<select name="size4" id="size4_select" ></select>
		</td>
		<td><input type="text" size="10" maxlength="100" name="tanka4" id="tanka4" rownum="4"readonly>
		</td>
		<td><input type="text" size="10" maxlength="100" name="tani4" id="tani4" rownum="4"readonly>
		</td>
		<td><select id="suryo4" name="suryo4" yen="0" class="itemcount" rownum="4">
			<option value=1>1</option>
			<option value=2>2</option>
			<option value=3>3</option>
			<option value=4>4</option>
			<option value=5>5</option>
			<option value=6>6</option>
			<option value=7>7</option>
			<option value=8>8</option>
			<option value=9>9</option>
			<option value=10>10</option></select></td>
		<td><div><input type="text" size="10" maxlength="100" name="kingaku4" id="kingaku4" rownum="4"readonly></div></td>
		<td><input type="text" size="10" maxlength="100" name="jippi4" id="jippi4" rownum="4"readonly></td>
		<td colspan="2"><input type="text" size="10" maxlength="100" name="gokei4" id="gokei4" rownum="4"readonly></td>
		<td colspan="2"><input type="text" size="10" maxlength="100" name="hosoku4" id="hosoku4" rownum="4"readonly></td>
		<td><input type="text" size="5" maxlength="100" name="hattyu4" id="hattyu4" rownum="4"></td>
		<td><input type="text" size="5" maxlength="100" name="hassou4" id="hassou4" rownum="4"></td>
	</tr>

	<%--商品5つ目--%>

	<tr>
		<td><input type="text" size="2" maxlength="100" name="gou5" id="gou5" class="itemcode" rownum="5">
		</td>
		<td><input type="text" size="2" maxlength="100" name="page5" id="page5"  class="itemcode" rownum="5">
		</td>
		<td><input type="text" size="2" maxlength="100" name="No5" id="No5" class="itemcode"  rownum="5">
		</td>
		<td>
			<input type="text" size="15" maxlength="100" name="syohinname5"  id="syohinname5" rownum="5"readonly>
		</td>
		<td><input type="text" size="5" maxlength="100" name="kata5"  rownum="5"readonly>
		</td>
		<td>
			<!-- <input type="text" size="2" maxlength="100" name="iro5" rownum="5" class="iro_single"> -->
			<select name="iro5" id="iro5_select" ></select>

		</td>
		<td>
			<!--<input type="text" size="2" maxlength="100" name="gara5" rownum="5"> -->
			<select name="gara5" id="gara5_select" ></select>
		</td>
		<td>
			<!--<input type="text" size="2" maxlength="100" name="size5" rownum="5"> -->
			<select name="size5" id="size5_select" ></select>
		</td>
		<td><input type="text" size="10" maxlength="100" name="tanka5" id="tanka5" rownum="5"readonly>
		</td>
		<td><input type="text" size="10" maxlength="100" name="tani5" id="tani5" rownum="5"readonly>
		</td>
		<td><select id="suryo5" name="suryo5" yen="0" class="itemcount" rownum="5">
			<option value=1>1</option>
			<option value=2>2</option>
			<option value=3>3</option>
			<option value=4>4</option>
			<option value=5>5</option>
			<option value=6>6</option>
			<option value=7>7</option>
			<option value=8>8</option>
			<option value=9>9</option>
			<option value=10>10</option></select></td>
		<td><div><input type="text" size="10" maxlength="100" name="kingaku5" id="kingaku5" rownum="5"readonly></div></td>
		<td><input type="text" size="10" maxlength="100" name="jippi5" id="jippi5" rownum="5"readonly></td>
		<td colspan="2"><input type="text" size="10" maxlength="100" name="gokei5" id="gokei5" rownum="5"readonly></td>
		<td colspan="2"><input type="text" size="10" maxlength="100" name="hosoku5" id="hosoku5" rownum="5"readonly></td>
		<td><input type="text" size="5" maxlength="100" name="hattyu5" id="hattyu5" rownum="5"></td>
		<td><input type="text" size="5" maxlength="100" name="hassou5" id="hassou5" rownum="5"></td>
	</tr>

	<%--商品6つ目--%>

	<tr>
		<td><input type="text" size="2" maxlength="100" name="gou6" id="gou6" class="itemcode" rownum="6">
		</td>
		<td><input type="text" size="2" maxlength="100" name="page6" id="page6"  class="itemcode" rownum="6">
		</td>
		<td><input type="text" size="2" maxlength="100" name="No6" id="No6" class="itemcode"  rownum="6">
		</td>
		<td>
			<input type="text" size="15" maxlength="100" name="syohinname6"  id="syohinname6" rownum="6"readonly>
		</td>
		<td><input type="text" size="5" maxlength="100" name="kata6" rownum="6"readonly>
		</td>
		<td>
			<!-- <input type="text" size="2" maxlength="100" name="iro6" rownum="6" class="iro_single"> -->
			<select name="iro6" id="iro6_select" ></select>

		</td>
		<td>
			<!--<input type="text" size="2" maxlength="100" name="gara6" rownum="6"> -->
			<select name="gara6" id="gara6_select" ></select>
		</td>
		<td>
			<!--<input type="text" size="2" maxlength="100" name="size6" rownum="6"> -->
			<select name="size6" id="size6_select" ></select>
		</td>
		<td><input type="text" size="10" maxlength="100" name="tanka6" id="tanka6" rownum="6"readonly>
		</td>
		<td><input type="text" size="10" maxlength="100" name="tani6" id="tani6" rownum="6"readonly>
		</td>
		<td><select id="suryo6" name="suryo6" yen="0" class="itemcount" rownum="6">
			<option value=1>1</option>
			<option value=2>2</option>
			<option value=3>3</option>
			<option value=4>4</option>
			<option value=5>5</option>
			<option value=6>6</option>
			<option value=7>7</option>
			<option value=8>8</option>
			<option value=9>9</option>
			<option value=10>10</option></select></td>
		<td><div><input type="text" size="10" maxlength="100" name="kingaku6" id="kingaku6" rownum="6"readonly></div></td>
		<td><input type="text" size="10" maxlength="100" name="jippi6" id="jippi6" rownum="6"readonly></td>
		<td colspan="2"><input type="text" size="10" maxlength="100" name="gokei6" id="gokei6" rownum="6"readonly></td>
		<td colspan="2"><input type="text" size="10" maxlength="100" name="hosoku6" id="hosoku6" rownum="6"readonly></td>
		<td><input type="text" size="5" maxlength="100" name="hattyu6" id="hattyu6" rownum="6"></td>
		<td><input type="text" size="5" maxlength="100" name="hassou6" id="hassou6" rownum="6"></td>
	</tr>


	<tr>
		<td></td>
		<td colspan="8"></td>
		<td>合計</td>
		<td><div id="suryokei"name="suryokei"readonly></div></td>
		<td><div id="shokei"name="shokei"readonly></div></td>
		<td></td>
		<td colspan="2"><div id="zeikomikei"name="zeikomikei"readonly></div></td>
		<td colspan="4"></td>
	</tr>
	<tr>
		<td colspan="2">お支払い方法</td>
		<td>分割<select id="bunkatu" name="bunkatu">
			<option value=1>1</option>
			<option value=2>2</option>
			<option value=3>3</option>
			<option value=4>4</option>
			<option value=5>5</option>
			<option value=6>6</option>
			<option value=7>7</option>
			<option value=8>8</option>
			<option value=9>9</option>
			<option value=10>10</option>
			<option value=11>11</option>
			<option value=12>12</option>
			<option value=13>13</option>
			<option value=14>14</option>
			<option value=15>15</option>
			<option value=16>16</option>
			<option value=17>17</option>
			<option value=18>18</option>
			<option value=19>19</option>
			<option value=20>20</option>
			<option value=21>21</option>
			<option value=22>22</option>
			<option value=23>23</option>
			<option value=24>24</option></select></td>
		<td colspan="4">振込先銀行　あかね銀行　　　　西成支店</td>
		<td>発注</td>
		<td colspan="2"><input type="text" size="2" name="h_year" >年<input type="text" size="2" name="h_month" >月<input type="text" size="2" name="h_day" >日</td>
		<td>担当者</td>
		<td colspan="2"><input type="text" size="7" name="h_tanto" ></td>
		<td colspan="4">ボーナス控除手数料</td>
		<td colspan="2"><input type="text" size="7" name="b_tesuryo" ></td>
	</tr>
	<tr>
		<td colspan="2"><label>銀行振込
		<input type="radio" name="radio3" value="銀行振込" checked></label></td>
		<td colspan="2">振込元銀行</td>
		<td colspan="3"><input type="text" size="5" name="ginkoname" ></td>
		<td rowspan="2">請求</td>
		<td colspan="4">区分<select id="seikyukubun" name="seikyukubun">
			<option value="控除台帳記入">K.控除台帳記入</option>
			<option value="振込請求書作成">F.振込請求書作成</option></select></td>
		<td colspan="3">請求合計金額</td>
		<td colspan="4"><input type="text" size="7" name="sumgaku" ></td>
	</tr>
	<tr>
		<td colspan="2"><label>給与控除
		<input type="radio" name="radio3" value="給与控除"></label></td>
		<td colspan="2">控除年月</td>
		<td><input type="text" size="2" name="h_year" >年<input type="text" size="2" name="h_month" >月</td>
		<td colspan="2">分より控除する</td>
		<td colspan="2"><input type="text" size="2" name="h_year" >年<input type="text" size="2" name="h_month" >月</td>
		<td>担当者</td>
		<td><input type="text" size="3" name="s_tanto" ></td>
		<td colspan="3">支払期限</td>
		<td colspan="5"><input type="text" size="2" name="h_year" >年<input type="text" size="2" name="h_month" >月<input type="text" size="2" name="h_day" >日</td>
	</tr>
	<tr>
		<td colspan="2">ボーナス控除</td>
		<td colspan="2">控除年月</td>
		<td><input type="text" size="2" name="h_year" >年<input type="text" size="2" name="h_month" >月</td>
		<td colspan="2">分より控除する</td>
		<td>入金</td>
		<td colspan="2"><input type="text" size="2" name="h_year" >年<input type="text" size="2" name="h_month" >月</td>
		<td>担当者</td>
		<td><input type="text" size="3" name="n_tanto" ></td>
		<td colspan="3">未入金督促日付</td>
		<td colspan="5"><input type="text" size="2" name="h_year" >年<input type="text" size="2" name="h_month" >月<input type="text" size="2" name="h_day" >日</td>
	</tr>
</table>

<Div Align="right"><input type="submit" name="kakutei" value="決定">←------</Div>

</form>
<button  type="submit" onclick="window.print()" style="width:140px;height:70px">印刷</button>←-------------------------------------------

<br>
</body>
</html>