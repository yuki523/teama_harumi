<%@page import="dto.sendDTO"%>
<%@page import="dto.kaiinDTO"%>
<%@page import="web.WebJucyuDAO"%>
<%@page import="dto.shohinDTO"%>
<%@page import="dto.payDTO"%>
<%@page import="dto.juchu"%>
<%@page import="dto.WEBjuchuDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dto.shohinDTO"%>
<%
HttpSession ss = request.getSession();
kaiinDTO member = (kaiinDTO)ss.getAttribute("member");

///ログインしているなら
if (member == null){
//セッションが切れているなら
response.sendRedirect("cartMiss.html");
//RequestDispatcher rd = request.getRequestDispatcher("cartMiss.html");
//rd.forward(request, response);
}


int juchuno2 = (Integer)session.getAttribute("juchuno");
ArrayList<WEBjuchuDTO> web = WebJucyuDAO.infoGet(juchuno2);

String receptime = null;
int juchuno3 = 0;
String kyosanname = null;
String shozoku = null;
String hurigana = null;
String kaiinno = null;
String kaiinname = null;
String kaiintel = null;
String kaiintelkubun = null;
String jusho = null;
String tell = null;
String sendname = null;
String sendkubun = null;
int paykaisu = 1;
String paybank = null;
String shohinname = null;
String rusu =null;

int sumgaku = 0;
int sumtax = 0;
int paysum = 0;

String payway =null;

for(WEBjuchuDTO a : web){
receptime = a.getReceptime();
juchuno3 = a.getJuchuno();
kyosanname = a.getKyosanname();
shozoku = a.getBusho();
hurigana = a.getKaiinkana();
kaiinno = a.getKaiinno();
kaiinname = a.getKaiinname();
kaiintel = a.getKaiinntelno();
kaiintelkubun = a.getKaiinntelkubun();
jusho = a.getSendjusho();
tell = a.getSendtelno();
sendname = a.getSendname();
sendkubun = a.getSendkubun();
paykaisu = a.getPaykaisu();
paybank = a.getPaybank();
shohinname = a.getShohinname();

sumgaku = a.getSumgaku();
sumtax = a.getSumtax();
paysum = a.getPaysum();

payway = a.getPayway();
rusu = a.getRusu();

}

%>

<%@ page language="java" contentType="text/html; charset=UTF8"
    pageEncoding="UTF8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang= "ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<link rel="stylesheet"  type="text/css" href="tyumonuketuke.css"/>
<title>注文受付控え</title>
</head>
<body>

<hr size="25" color="#333333" width="max">
<br>
<h2>　　　　<a href="toppage.jsp">HARUMI</a></h2>
<br>
<br>
<hr size="1" color="#333333" width="900">
<br>
<br>

<h3>ご注文控えになります。印刷してください</h3>

<form action="" method="get">
</table>
<tr>


<%--ご依頼主情報 --%>

<table style="border: 1px solid #CCC;border-collapse: collapse;width: 100%;">
  <tr>
    <td style="padding:10px;border: 1px solid #CCC;background-color: #F4F8FF;font-weight: bold;padding: 5px;">ご注文お控え</td>
    <td style="padding:10px;border: 1px solid #CCC;background-color: #F4F8FF;font-weight: bold;padding: 5px;">注文受付日</td>
    <td style="padding:10px;border: 1px solid #CCC;background-color: #F4F8FF;font-weight: bold;padding: 5px;"><%=receptime %></td>
    <td style="padding:10px;border: 1px solid #CCC;background-color: #F4F8FF;font-weight: bold;padding: 5px;">ご注文受付番号</td>
    <td style="padding:10px;border: 1px solid #CCC;background-color: #F4F8FF;font-weight: bold;padding: 5px;"><%=juchuno3 %></td>
  </tr>
</table>
<br><br>

<table style="border: 1px solid #CCC;border-collapse: collapse;width: 100%;">

  <tr>
    <td style="padding:20px;border: 1px solid #CCC;background-color: #F9FCFE;font-weight: bold;">会員様企業名</td>
    <td style="padding:20px;border: 1px solid #CCC;colspan:2;"><%=kyosanname %></td>
    <td style="padding:10px;background-color: #F9FCFE;border: 1px solid #CCC;">所属</td>
    <td style="padding:10px;border: 1px solid #CCC;"><%=shozoku %></td>
    <td style="padding:10px;background-color: #F9FCFE;border: 1px solid #CCC;">社員番号</td>
    <td style="padding:10px;border: 1px solid #CCC;"><%=kaiinno %></td>

  </tr>
  <tr>
    <td style="padding:10px;border: 1px solid #CCC;background-color: #F9FCFE;font-weight: bold;">会員様氏名</td>
    <td style="padding:10px;border: 1px solid #CCC;"><%=kaiinname %></td>
    <td style="padding:20px;background-color: #F9FCFE;border: 1px solid #CCC;">会員様フリガナ</td>
    <td style="padding:20px;border: 1px solid #CCC;"><%=hurigana %></td>
    <td style="padding:10px;background-color: #F9FCFE;border: 1px solid #CCC;">ご連絡先TEL</td>
    <td style="padding:10px;border: 1px solid #CCC;"><%=kaiintel %></td>
    <td style="padding:10px;background-color: #F9FCFE;border: 1px solid #CCC;">TEL区分</td>
    <td style="padding:6px;border: 1px solid #CCC;"><%=kaiintelkubun %></td>
  </tr>
</table>
<br><br>

<!--
*********************************
届先
*********************************
-->
お届け住所
<table style="border: 1px solid #CCC;border-collapse: collapse;width: 100%;">
  <tr>
    <td style="padding:10px;border: 1px solid #CCC;background-color: #F9FCFE;font-weight: bold;">住所</td>
    <td style="padding:10px;border: 1px solid #CCC;"><%=jusho %></td>
    <td style="padding:20px;background-color: #F9FCFE;border: 1px solid #CCC;">届先区分</td>
    <td style="padding:20px;border: 1px solid #CCC;"><%=sendkubun %></td>
   </tr>
   <tr>
    <td style="padding:10px;background-color: #F9FCFE;border: 1px solid #CCC;">留守の時</td>
    <td style="padding:10px;border: 1px solid #CCC;"><%=kaiintel %></td>
    <td style="padding:10px;background-color: #F9FCFE;border: 1px solid #CCC;">TEL</td>
    <td style="padding:6px;border: 1px solid #CCC;"><%=tell %></td>
  </tr>
</table>
<br>注文商品
<table border =border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#F9FCFE"colspan="1">号</td>
<td bgcolor="#F9FCFE"colspan="1">頁</td>
<td bgcolor="#F9FCFE"colspan="1">No</td>
<td bgcolor="#F9FCFE"colspan="3">商品名</td>
<td bgcolor="#F9FCFE"colspan="2">型式</td>
<td bgcolor="#F9FCFE"colspan="1">色</td>
<td bgcolor="#F9FCFE"colspan="1">柄</td>
<td bgcolor="#F9FCFE"colspan="1">サイズ</td>
<td bgcolor="#F9FCFE"colspan="1">単価</td>
<td bgcolor="#F9FCFE"colspan="1">単位</td>
<td bgcolor="#F9FCFE"colspan="1">数量</td>
<td bgcolor="#F9FCFE"colspan="2">金額</td>
<td bgcolor="#F9FCFE"colspan="2">合計金額</td>
</tr>
<%
for(WEBjuchuDTO b : web){
%>
<%--商品1つ目--%>
<tbody>
<tr>
<td colspan="1"><%=b.getGou() %></td>
<td colspan="1"><%=b.getPage() %></td>
<td colspan="1"><%=b.getShohinno() %></td>
<td colspan="3"><%=b.getShohinname() %></td>
<td colspan="2"><%=b.getModel() %></td>
<td colspan="1"><%=b.getIro() %></td>
<td colspan="1"><%=b.getGara() %></td>
<td colspan="1"><%=b.getSize() %></td>
<td colspan="1"><%=b.getNotaxtotal() %></td>
<td colspan="1"><%=b.getUnit() %></td>
<td colspan="1">１</td>
<td colspan="2"><%=b.getNotaxtotal() %></td>
<td colspan="2"><%=b.getTotal() %></td>

</tr>
</tbody>
<%
}
%>


<!--
*************************************************
支払
*************************************************
-->


<tr>
<td></td>
<td></td>
<td bgcolor="#F9FCFE">合計</td>
<td bgcolor="#F9FCFE">税抜</td>
<td colspan="2"><%=sumgaku %></td>
<td bgcolor="#F9FCFE">税込</td>
<td colspan="3"><%=sumtax %></td>
<td bgcolor="#F9FCFE">支払合計額</td>
<td colspan="4"><%=paysum %></td>
</tr>

</table>
<br><br><br>
<table style="border: 1px solid #CCC;border-collapse: collapse;width: 100%;">


<tr>
<td bgcolor="#F9FCFE">お支払い方法</td>
<td colspan="3"><%=payway %></td>
<td bgcolor="#F9FCFE">回数：<%=paykaisu %></td>

</tr>
<tr>
<%
if(payway.equals("銀行振り込み")){
%>
<td colspan="4">振込先銀行　あかね銀行　　　　西成支店</td>
<td bgcolor="#F9FCFE" colspan="3">振込元銀行</td>
<td colspan="3"><%=paybank %></td>
<%
}else{
%>
<%
}
%>
</tr>
</table>
<p>
</p>


</form>

<Div Align="right"><button onclick="window.print()">印刷する</button></Div>
<br><br>
<%

session.removeAttribute("member");
session.removeAttribute("selectShohin");

%>
<Div align=center>
<a href="toppage.jsp">トップへ戻る</a>
</Div>
<br>
<br>
<br>
<div style="position: relative;">
<img src="foot.png" width="100%" height="130">
</div>
</body>
</html>