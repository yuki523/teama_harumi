/**
 *
 */
$(function() {

	//商品コードの入力テキストボックスからカーソルが外れたとき
	$('.itemcode').blur(function() {

		//alert("focus released");
		var rownum = $(this).attr("rownum");
		//alert($("#item_gou_"+rownum).val());

		var sizetmp = "";

		if($("#gou"+rownum).val()!="" && $("#page"+rownum).val()!="" && $("#No"+rownum).val()!=""){
			$.ajax({
				  type: 'GET',
				  url: 'http://localhost:8080/haruminn/ItemSearchByCode?g='+$("#gou"+rownum).val()+"&p="+$("#page"+rownum).val()+"&n="+$("#No"+rownum).val(),
				  dataType: 'jsonp',
				  jsonp: 'callback',
				  success: function(json){

					  $('#iro'+rownum+'_select > option').remove();
					  $('#gara'+rownum+'_select > option').remove();
					  $('#size'+rownum+'_select > option').remove();
					  var len = json.length;
					  for(var i=0;i<len; i++){

						  //商品名
						  $("#syohinname"+rownum).val(json[i].shohinname);
						  //単価
						  $("#tanka"+rownum).val(json[i].tanka);

						  $("#suryo"+rownum).attr("yen", json[i].tanka);

						  $("#tani"+rownum).val(json[i].unit);

						  if(json[i].iro!=""){
							  $('#iro'+rownum+'_select').append($('<option>').html(json[i].iro).val(json[i].iro));
						  }
						  if(json[i].gara!=""){
							  $('#gara'+rownum+'_select').append($('<option>').html(json[i].gara).val(json[i].gara));
						  }

						  if(json[i].size!="" && sizetmp!=json[i].size){
							  $('#size'+rownum+'_select').append($('<option>').html(json[i].size).val(json[i].size));
							  sizetmp = json[i].size;
						  }
					  }
					  $("#suryo"+rownum).change();
				  }
			});
		}
	});

	//ドロップダウンが変更されたとき
	$('.itemcount').change(function() {
		var rownum = $(this).attr("rownum");
		var goukeicnt=0;
		var goukeigaku=0;
		var gyougoukei=0;
		var gyougoukeizeikomi=0;

		var i=1;
		$('.itemcount').each(function(){

			gyougoukei=0;

			//すべてのclass=itemcountを持つドロップダウンの値を加算
			goukeicnt = goukeicnt + Number($(this).val());

			//すべてのclass=itemcountを持つドロップダウンの「yen」要素の値を加算
			goukeigaku = goukeigaku + ( Number($(this).attr("yen")) * Number($(this).val()));
			gyougoukei = ( Number($(this).attr("yen")) * Number($(this).val()));

			if(rownum==i){
				//行の合計
				$('#kingaku'+rownum).val(gyougoukei);
				//行の合計金額
				$('#gokei'+rownum).val(Math.round(gyougoukei*1.08));

			}
			gyougoukeizeikomi = gyougoukeizeikomi+ Math.round(gyougoukei*1.08);
			i++;
		});

		//数量の合計
		$('#suryokei').text(goukeicnt);
		//列の合計
		$('#shokei').text(goukeigaku);
		//列の税込み合計
		$('#zeikomikei').text(gyougoukeizeikomi);
	});
});