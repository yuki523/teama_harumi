
<%@page import="web.WebJucyuDAO"%>
<%@page import="dto.shohinDTO"%>
<%@page import="dto.kaiinDTO"%>
<%@page import="dto.juchu"%>
<%@page import="java.util.ArrayList"%>

<%@ page language="java" contentType="text/html; charset=UTF8"
    pageEncoding="UTF8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% request.setCharacterEncoding("UTF8");%>
<%
HttpSession ss = request.getSession();
kaiinDTO member = (kaiinDTO)ss.getAttribute("member");
ArrayList<shohinDTO> shohin = (ArrayList<shohinDTO>)ss.getAttribute("selectShohin");

///ログインしているなら
if (member == null){
	//セッションが切れているなら
	response.sendRedirect("cartMiss.html");
	//RequestDispatcher rd = request.getRequestDispatcher("cartMiss.html");
	//rd.forward(request, response);
} else {



	if(shohin.isEmpty()){

		response.sendRedirect("CartNG.html");

	}else{


	int goukei=0;
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<title>支払選択</title>

</head>
<body>


<hr size="25" color="#333333" width="max">
<br>
<h2>　　　　<a href="toppage.jsp">HARUMI</a></h2>

<Div align=right>
<% if (member==null) { %>
<a href="LoginJudge.jsp">ログイン</a>
<% } else { %>
<a href="http://localhost:8080/haruminn/LogOut">ログアウト</a>
<a href="http://localhost:8080/haruminn/cart.jsp">カート</a>
<% }  %>
</Div>
<br>
<hr size="1" color="#333333" width="900">

<Div Align="center">
<h1>お届け先入力、お支払選択</h1>
</Div>


<form action="http://localhost:8080/haruminn/WebShiharai" method="post" name="Form1"onSubmit="return (checkForm() && CheckBlank2() && changeDisplay())" >

<script type="text/javascript">
<!--
function changeDisplay() {
	//alert("changed");
	//銀行振り込み
    if ( document.Form1["shiharai"][0].checked ) {
        document . Form1["ginkouname"] . disabled = false;
        document . getElementById('input-number') . style . display = "inline";
        document . getElementById('input-number2') . style . display = "none";


        //給与控除2回～24回
    }else if(document.Form1["shiharai"][2].checked){

    	document . Form1["kaisu"] . disabled = false;
    	 document . getElementById('input-number') . style . display = "none";
        document . getElementById('input-number2') . style . display = "inline";

    } else {
        document . Form1["ginkouname"] . disabled = true;
        document . Form1["kaisu"] . disabled = true;
        document . getElementById('input-number') . style . display = "none";
        document . getElementById('input-number2') . style . display = "none";
    }
}

function checkForm(){
	//alert("Form");
	flag = 0;
	blank =0;
    for ( i = 0 ; i < document.Form1.shiharai.length ; i++){
        if (document.Form1.shiharai[i].checked){
           	flag = 1;
         }
    }
        if (!flag){
                alert('ラジオボタンのいずれかをご選択ください');
                return false;
        }

		return true;
}

function CheckBlank(){
	//alert("blank");
	if ( document.Form1["shiharai"][0].checked ) {
		if(document.Form1["ginkouname"].value==""){
			alert('銀行名をご入力ください');
			return false;
		}
	}
}


function CheckBlank2(){
	//alert("blank");
	if ( document.Form1["shiharai"][0].checked ) {
			if(document.Form1["ginkouname"].value==""){
				alert('銀行名をご入力ください');
				return false;
			}
		}
	if(document.Form1["kaiinname"].value=="" || document.Form1["kaiinyubin"].value=="" || document.Form1["kaiinjusho"].value=="" || document.Form1["kaiitelno"].value==""   ){
		<%System.out.println("0000");%>
		alert('お届け先をご入力ください');
			return false;


	}
	}


// -->
</script>
<Div Align="center">
<br>
<h3>購入予定商品一覧表</h3>
<br>
 <table border="1">
 <thead>
      <tr>
        <th>商品番号</th>
        <th>商品名</th>
        <th>サイズ</th>
        <th>色</th>
        <th>柄</th>
        <th>単価</th>
        <th>数量</th>
      </tr>
    </thead>
 <%
for(shohinDTO a : shohin){
 %>
    <tbody>
      <tr>
        <td><%= a.getGou() %><%= a.getPage() %><%= a.getShohinno() %></td>
        <td><%= a.getShohinname() %></td>
        <td><%= a.getSize() %></td>
        <td><%= a.getIro() %></td>
        <td><%= a.getGara() %></td>
        <td><%= a.getTanka() %></td>
        <% goukei = goukei + a.getTanka(); %>
        <td>1</td>
      </tr>
    </tbody>
<%
}
%>
  </table>
  </Div>
<br>
<br>
 <br><br>
<Div Align="center">
   <Font Size="7">
   <table border="1">
    <thead>
      <tr>
        <th>合計金額</th>
        <th><%=(int)Math.floor(goukei*1.08) %>円</th>
        <%
        session.setAttribute("goukei", goukei);
        %>
      </tr>
    </thead>
    </tbody>
   </table>
   </Font>
   </Div>
<br><br>
<br>
<hr size="1" color="#333333" width="900">
<br>
<br>
<Div Align="center">
<h3>届先入力（※変更がある場合は入力してください）</h3>
</Div>
<br>
<Div Align="center">
  <table border="1">
  <tr>
    <td>届先氏名</td>
    <td>郵便番号</td>
    <td>住所</td>
	<td>届先区分</td>
	<td>TEL(半角英数でご入力ください)</td>
    <td>留守の時</td>
  </tr>
  <tr>
    <td><input type="text" name="kaiinname" maxlength="20"value="<%=member.getKaiinname()%>"></td>
    <td><input type="text" name="kaiinyubin" maxlength="20"value="<%=member.getKaiinnyubin()%>"></td>
    <td><input type="text" name="kaiinjusho" size="50"value="<%=member.getKaiinnjusho()%>"></td>
    <td>
	<SELECT name="kubun">
	<OPTION value="自宅">自宅</OPTION>
	<OPTION value="その他">その他</OPTION>
	</SELECT>
	</td>
    <td><input type="text" name="kaiintelno" maxlength="20"value="<%=member.getKaiinntelno()%>"></td>

    <td><input type="text" name="rusunotoki" maxlength="20"value=""></td>
  </tr>
</table>
</Div>




<br><br>
<h3>支払条件</h3>
 <table border="1">
  <tr>
    <td>支払方法</td>

  </tr></table>
<input type="radio" name="shiharai" value="銀行振り込み" id="radio-0" onClick="changeDisplay()"><label for="radio-0"> 銀行振り込み</label>&nbsp;
<p id="input-number" style="display:none;">お振込元銀行名：<input type="text" name="ginkouname" size="20" value="">銀行</p>
<br><br>
<input type="radio" name="shiharai" value="給与控除1回" id="radio-1" onClick="changeDisplay()"><label for="radio-1"> 給与控除1回</label>&nbsp;
<br><br>
<input type="radio" name="shiharai" value="給与控除分割" id="radio-2"  onClick="changeDisplay()"><label for="radio-2">給与控除分割</label>&nbsp;
<p id="input-number2" style="display:none;">
<select name="kaisu">
<option value="2">2回</option>
<option value="3">3回</option>
<option value="4">4回</option>
<option value="5">5回</option>
<option value="6">6回</option>
<option value="7">7回</option>
<option value="8">8回</option>
<option value="9">9回</option>
<option value="10">10回</option>
<option value="11">11回</option>
<option value="12">12回</option>
<option value="13">13回</option>
<option value="14">14回</option>
<option value="15">15回</option>
<option value="16">16回</option>
<option value="17">17回</option>
<option value="18">18回</option>
<option value="19">19回</option>
<option value="20">20回</option>
<option value="21">21回</option>
<option value="22">22回</option>
<option value="23">23回</option>
<option value="24">24回</option>
</select>
</p>
<br><br><br>
<br><br><br>

 <INPUT type="submit" value="上記の内容で注文する">


</form>
<br><br><br>

<INPUT type="button" Value="前のページに戻る" onClick="history.go(-1);">

<%
	}
} %>
<br>
<br>
<br>
<div style="position: relative;">
<img src="foot.png" width="100%" height="130">
</div>

</body>

</html>

