

<%@ page language="java" contentType="text/html; charset=UTF8"
    pageEncoding="UTF8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<title>商品一覧表示</title>
</head>

<%@page import="java.util.ArrayList" %>
<%@page import="dto.shohinDTO" %>
<%@page import="gyomu.ShohinitiranDAO" %>
<%@page import="gyomu.Shohinitiranservlet" %>
<%@page import="dto.keiyakuDTO" %>
<%@page import="gyomu.KeiyakuDAO" %>
<%@page import="gyomu.Keiyakuservlet" %>
<%@page import="java.util.Arrays" %>

<body>

<SCRIPT Language="JavaScript">
<!--
function check_blank(frm){
if(frm.elements["newshohinname"].value=="" || frm.elements["newgou"].value=="" || frm.elements["newpage"].value=="" || frm.elements["newshohinno"].value=="" || frm.elements["newtanka"].value==""|| frm.elements["newunit"].value==""  || frm.elements["newrelease"].value=="" || frm.elements["newkigen"].value==""  || frm.elements["newboxsize"].value==""|| frm.elements["newsetumei"].value==""  || frm.elements["newcateno"].value=="" || frm.elements["newgazo"].value==""|| frm.elements["newsold"].value=="" ){
	alert('全て記入してください');
	return false;
}
}
 -->
</SCRIPT>

	<h1>商品menu</h1>
	<br>
	<A HREF="http://localhost:8080/haruminn/Top.jsp">トップへ</a><br><br>
	<br>
	番号半角英数で入力してください
	<form action="Shohinitiranservlet" method="post" onsubmit="return check_blank(this)">
	掲載号：<label><input type="text" name="gou"></label>　
	ページ：<label><input type="text" name="page"></label>　
	番号：<label><input type="text" name="shohinno"></label>　
	<input type="submit" name="submittype" value="番号検索">
	</form>
<br>
<br>
<%

//ArrayList<shohinDTO> sdto2 = ShohinitiranDAO.getKensaku(request.getParameter("gou"),
//		request.getParameter("page"),
//		request.getParameter("shohinno"));

	ArrayList<shohinDTO> sdto2=(ArrayList<shohinDTO>) session.getAttribute("shohininfo");
	if(sdto2==null){
		sdto2=new ArrayList<shohinDTO>();
	}
%>
<br><br>

<%
String mess = "ようこそ！";
if(request.getAttribute("message")!=null){
	mess = (String)request.getAttribute("message");
}
%>
<%=mess %><br><br>


<%//============================================================================================================= %>
<%//============================================================================================================= %>
<%//============================================================================================================= %>

		<h1>商品検索結果</h1>

 <tr>
  <td>
  <br>

  <%for(shohinDTO a : sdto2){%>
	<% System.out.print(a.getGou()); %>
    <%if(a.getGou() == null || a.getPage() == null || a.getShohinno() == null){%>
	  存在しない商品番号です。
  <%} %>



  ●商品情報　　　　　　　　　　　　　　　　　　　　　　
  <br>

		<form action="Shohinitiranservlet" method="post">
	 <table border=1 cellspacing=1 cellpadding=3>
       <tr><td>掲載号</td><td><input type="text" name="gou" value="<%=a.getGou()%>"></td>
       							<input type="hidden" name="gou2" value="<%=a.getGou()%>">
    					  <td>掲載頁</td><td><input type="text" name="page"value="<%=a.getPage()%>"></td>
    					  					<input type="hidden" name="page2" value="<%=a.getPage()%>">
 						  <td>商品番号</td><td><input type="text" name="shohinno" value="<%=a.getShohinno()%>"></td>
 						  					   <input type="hidden" name="shohinno2" value="<%=a.getShohinno()%>">
    </tr>
    <tr><td>商品名</td><td><input type="text" name="shohinname"value ="<%=a.getShohinname()%>"></td>
    					<input type="hidden" name="shohinname2" value="<%=a.getShohinname()%>">
    	<td>単価</td><td>￥<input type="text" name="tanka" value ="<%=a.getTanka()%>"></td>
    	<td>カテゴリーNO.</td><td><input type="text" name="cateno" value="<%=a.getCateno() %>"></td>
    </tr>
    <tr><td>色</td><td><input type="text" name="iro" value="<%=a.getIro() %>"></td>
    				 <input type="hidden" name="iro2" value="<%=a.getIro()%>">
    	<td>柄</td><td><input type="text" name="gara" value="<%=a.getGara() %>"></td>
    					 <input type="hidden" name="gara2" value="<%=a.getGara()%>">
    	<td>サイズ</td><td><input type="text" name="size" value="<%=a.getSize() %>"></td>
    						 <input type="hidden" name="size2" value="<%=a.getSize()%>">
    </tr>
    <tr><td>型式</td><td><input type="text" name="model" value="<%=a.getModel() %>"></td>
    	<td>単位</td><td><input type="text" name="unit" value="<%=a.getUnit() %>"></td>
    	<td>売れた個数</td><td><input type="text" name="sold" value="<%=a.getSold() %>"></td>
    	</tr>
        <tr><td>商品説明</td><td colspan=5><input type="text" name="setumei" value="<%=a.getSetumei() %>"></td>
    	</tr>
    <tr><td>契約販売店</td><td><input type="text" name="keiyakuno" value="<%=a.getKeiyakuno() %>"></td>
    	<td>商品提供日</td><td><input type="text" name="release" value="<%=a.getRelease() %>"></td>
    	<td>有効期限</td><td><input type="text" name="kigen" value="<%=a.getKigen() %>"></td>
    </tr>

    <tr><td>送料有無</td><td><input type="text" name="soryoumu" value="<%=a.getSoryoumu() %>"readonly></td>
    	<td>ダンボールサイズ</td><td><input type="text" name="boxsize" value="<%=a.getBoxsize() %>"></td>
    	<td>画像URL</td><td><input type="text" name="gazo" value="<%=a.getGazo() %>"></td>
    </tr>
    	 </table>
	 </td>
 </tr>
<input type="submit"name="submittype"value="変更">
<input type="submit"name="submittype"value="削除">
</form>
<br><br>
<%} %>
<br><br><br><br>
	<form action="Shohinitiranservlet" method="post" onsubmit="return check_blank(this)">
<h1>新規商品登録</h1>

 <tr>
  <td>
  <br>
  ●商品情報　　　　　　　　　　　　　　　　　　　　　　
  <br>
   <table border=1 cellspacing=1 cellpadding=3>
       <tr><td>掲載号</td><td><input type="text" name="newgou"></td>
    					  <td>掲載頁</td><td><input type="text" name="newpage"></td>
 						  <td>商品番号</td><td><input type="text" name="newshohinno"></td>
    </tr>
    <tr><td>商品名</td><td><input type="text" name="newshohinname"></td>
    	<td>単価</td><td>￥<input type="text" name="newtanka"  onKeyup="this.value=this.value.replace(/[^0-9]+/i,'')" value="0"></td>



    	<td>カテゴリーNO.</td><td><SELECT name="newcateno">
		<OPTION value=1>1時計</OPTION>
		<OPTION value=2>2宝石</OPTION>
		<OPTION value=1>3ウェア</OPTION>
		<OPTION value=2>4その他</OPTION>
		</SELECT></td>
    </tr>
    <tr><td>色</td><td><input type="text" name="newiro"></td>
    	<td>柄</td><td><input type="text" name="newgara"></td>
    	<td>サイズ</td><td><input type="text" name="newsize"></td>
    </tr>
    <tr><td>型式</td><td><input type="text" name="newmodel"></td>
    	<td>単位</td><td><input type="text" name=newunit></td>
    	<td>売れた個数</td><td><input type="text" name="newsold" onKeyup="this.value=this.value.replace(/[^0-9]+/i,'')" value="0"></td>
    	</tr>
        <tr><td>商品説明</td><td colspan=5><input type="text" name="newsetumei"></td>
    	</tr>
    <tr><td>契約販売店</td><td><input type="text" name="newkeiyakuno"></td>
    	<td>商品提供日</td><td><input type="text" name="newrelease" value="　年　月　日"></td>
    	<td>有効期限</td><td><input type="text" name="newkigen"value="　年　月　日"></td>
    </tr>

    <tr><td>送料有無</td><td><input type="text" name="newsoryoumu" readonly></td>
    	<td>ダンボールサイズ</td><td><input type="text" name="newboxsize"></td>
    	<td>画像URL</td><td><input type="text" name="newgazo"></td>
    </tr>
   </table>
  </td>
 </tr>
   <input type="submit" name="submittype" value="追加">

</form>







</body>
</html>